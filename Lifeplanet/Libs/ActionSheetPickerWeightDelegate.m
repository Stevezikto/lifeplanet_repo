//
//  ActionSheetPickerCustomPickerDelegate.m
//  ActionSheetPicker
//
//  Created by  on 13/03/2012.
//  Copyright (c) 2012 Club 15CC. All rights reserved.
//

#import "ActionSheetPickerWeightDelegate.h"
#import <MKUnits/MKUnits.h>

@implementation ActionSheetPickerWeightDelegate

- (id)init
{
    if (self = [super init]) {    
        _unitFirst = [self setUnitValue:@"kg"];
        _unitSecond = [self setUnitValue:@"lb"];
        _unitThird = [self setUnitValue:@"decimal"];
        
        self.selectedKey = @"kg";
        self.selectedValue = _unitFirst[0];
        self.selectedDecimal = _unitThird[0];
        
        _unitKeys = [NSMutableArray new];
        [_unitKeys addObject:@"kg"];
        [_unitKeys addObject:@"lb"];

        _unitKeysAndValues = [[NSMutableDictionary alloc] init];
        [_unitKeysAndValues setValue:_unitFirst forKey:@"kg"];
        [_unitKeysAndValues setValue:_unitSecond forKey:@"lb"];
        
        // 기본값 : 24
        
        self.selectedValue = _unitFirst[24];
        
//        NSLog(@"third : %@", _unitThird);
//        NSLog(@"unitKeys : %@", _unitKeys);
//        NSLog(@"_unitKeysAndValues : %@", _unitKeysAndValues);
//        NSLog(@"first : %@ / second : %@", _unitFirst, _unitSecond);
    }
    return self;
}

/////////////////////////////////////////////////////////////////////////
#pragma mark - ActionSheetCustomPickerDelegate Optional's 
/////////////////////////////////////////////////////////////////////////
- (void)configurePickerView:(UIPickerView *)pickerView
{
    // Override default and hide selection indicator
    pickerView.showsSelectionIndicator = NO;
}

- (void)actionSheetPickerDidSucceed:(AbstractActionSheetPicker *)actionSheetPicker origin:(id)origin
{
//    BOOL isPound = NO;
//    
//    NSRange subRange;
//    subRange = [_lbHeight.text rangeOfString :@"cm"];
//    
//    if (subRange.location == NSNotFound){
//        _isFeet = YES;
//    }
//    else{
//        _isFeet = NO;
//    }
//    
    // symbol 설정
    BOOL isPound = NO;
    
    NSRange subRange;
    subRange = [self.selectedKey rangeOfString :@"kg"];
    
    if (subRange.location == NSNotFound){
        isPound = YES;
    }
    else{
        isPound = NO;
    }

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"weightPicker" object:nil userInfo:@{@"value": [NSString stringWithFormat:@"%@%@%@",self.selectedValue,self.selectedDecimal,self.selectedKey], @"pound": @(isPound)}];
}

/////////////////////////////////////////////////////////////////////////
#pragma mark - UIPickerViewDataSource Implementation
/////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    NSLog(@"component : %ld", component);
    // Returns
    switch (component) {
        case 0: return  [[self getValuesByKey:self.selectedKey] count];
        case 1: return [_unitThird count];
        case 2: return [_unitKeys count];
        default:break;
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////
#pragma mark UIPickerViewDelegate Implementation
/////////////////////////////////////////////////////////////////////////

// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    switch (component) {
        case 0: return 100.0f;
        case 1: return 40.0f;
        case 2: return 100.0f;
        default:break;
    }
    
    return 0;
}
/*- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
 {
 return
 }
 */
// these methods return either a plain UIString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0: return [self getValuesByKey:self.selectedKey][(NSInteger)row];
        case 1: return _unitThird[(NSInteger)row];
        case 2: return _unitKeys[(NSUInteger) row];
        default:break;
    }
    return nil;
}

/////////////////////////////////////////////////////////////////////////

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"Row %li selected in component %li", (long)row, (long)component);
    
    id transformedValue;
    
    NSInteger value, result;
    NSInteger selectedRow;
    NSString *symbol;
    
    switch (component) {
        case 0:
            self.selectedValue = [self getValuesByKey:self.selectedKey][(NSUInteger) row];
            value = [self.selectedValue integerValue];
            return;
        case 1:
            self.selectedDecimal = _unitThird[row];
            return;
        case 2:

            value = [self.selectedValue integerValue];
            
            if([self.selectedKey isEqual:@"lb"]){
                symbol = @"lb";
                transformedValue = [[[@(value) mass_pound] convertTo:[MKMassUnit kilogram]] amountWithPrecision:0];
            }
            else{
                symbol = @"kg";
                transformedValue = [[[@(value) mass_kilogram] convertTo:[MKMassUnit pound]] amountWithPrecision:0];
            }

            // 변환된 값
            result = [self returnConvertedValue:[NSString stringWithFormat:@"%@", transformedValue] typeOfMetricUsed:symbol];
            
            self.selectedKey = _unitKeys[(NSInteger) row];
            [pickerView reloadComponent:0];
            [pickerView reloadAllComponents];
//            self.selectedValue = [self getValuesByKey:self.selectedKey][(NSUInteger) [pickerView selectedRowInComponent:0]];
            self.selectedValue = [self getValuesByKey:self.selectedKey][result];

            // 변한 된 값으로 row 이동
            [pickerView selectRow:result inComponent:0 animated:NO];
            
            return;
            
        default:break;
    }
 
}

-(NSMutableArray *)getValuesByKey:(NSString *)unitKey
{
    NSMutableArray *citiesIncontinent = _unitKeysAndValues[unitKey];
    return citiesIncontinent;
};

- (NSMutableArray *)setUnitValue:(NSString *)key{
    
    NSMutableArray *values = [NSMutableArray new];
    
    if([key isEqual:@"kg"]){
        for(int i=30; i<720; i++){
            
            NSString *value = [NSString stringWithFormat:@"%d", i];
            [values addObject:value];
        }
    }
    else if([key isEqual:@"lb"]){
        for(int i=66; i<1587; i++){
            NSString *value = [NSString stringWithFormat:@"%d", i];
            [values addObject:value];
        }
    }
    
    else if([key isEqual:@"decimal"]){
        for(int i=0; i<10; i++){
            NSString *value = [NSString stringWithFormat:@".%d", i];
            [values addObject:value];
        }
    }

    return values;
}

- (NSInteger)returnConvertedValue:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType{
    
    NSLog(@"theMeasure: %@ , metricType :%@", theMeasure, metricType);
    

    NSInteger result = 0;
    
    if ([metricType isEqualToString:@"lb"]){
        for(int i = 0; i < _unitFirst.count; i++){
            
            if([theMeasure isEqualToString:_unitFirst[i]]){
                NSLog(@"i : %d , measure : %@", i , theMeasure);
                result = (NSInteger)i;
            }
        }
    }
    else{
        for(int i = 0; i < _unitSecond.count; i++){
            
            if([theMeasure isEqualToString:_unitSecond[i]]){
                NSLog(@"i : %d , measure : %@", i , theMeasure);
                result = (NSInteger)i;
            }
        }
    }
    
    return result;
}


@end
