//
//  MHScrollView.m
//  Arki
//
//  Created by ken on 2015. 3. 23..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "MHScrollView.h"
//#import "UIScrollView+currentPage.h"

@interface MHScrollView() <UIScrollViewDelegate> {
    CGRect screenFrame;
}

@property(nonatomic)NSInteger currentPage;
@end

@implementation MHScrollView

- (id)init {
    NSLog(@"");
    return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"");
    return [self initWithFrameAndDirection:frame direction:0 circularScroll:NO];
}

- (id)initWithFrameAndDirection:(CGRect)frame
                      direction:(int)direction
                 circularScroll:(BOOL) circularScrolling {
    NSLog(@"");
    self = [super initWithFrame:frame];
    if (self) {
//        screenFrame = frame;
        [self initializeControl];
    }
    return self;
}

- (void) initializeControl {
    
    screenFrame = self.frame;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.pagingEnabled = YES;
//    self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.delegate = self;
    self.contentSize = CGSizeMake(screenFrame.size.width, self.contentSize.height);
//    currentPage = NSNotFound;
//    _mScrollView.numberOfPages =  horizontalPosition / _mScrollView.frame.size.width;
//    _mScrollView.lastXOffset = horizontalPosition - _mScrollView.frame.size.width;
    
//    NSLog(@"contentsize : %f", self.contentSize.width);
//    [self scrollRectToVisible:CGRectMake(320,0,320,416) animated:NO];
//    NSLog(@"mh:w : %f",self.frame.size.width);
    [self scrollRectToVisible:CGRectMake(screenFrame.size.width , 0 , screenFrame.size.width ,self.frame.size.height) animated:NO];
}


- (NSInteger)currentPageIndex{
    
    return self.currentPage;//[self currentPage:self];
}

- (NSInteger)totalPages{
    
    return self.numberOfTotalPageCount;
}

- (CGPoint)offsetFromPage:(NSInteger)page{
    return CGPointMake(screenFrame.size.width * page, self.contentOffset.y);
}

- (void)moveToPage:(int)page{
//    NSLog(@"page : %d / offset : %f", page, page*self.frame.size.width);
    [self setContentOffset:CGPointMake(page * screenFrame.size.width, 0) animated:YES];
}

- (void) pageChanged:(NSInteger) index {
    if (index == _currentPage) return;
    _currentPage = index;
    
    if ([self.controlDelegate respondsToSelector:@selector(MHScrollView:currentPageChanged:)])
        [self.controlDelegate MHScrollView:self currentPageChanged:self.currentPage];
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSLog(@"mh.w: %f / x : %f , self:%f",scrollView.frame.size.width, scrollView.contentOffset.x, screenFrame.size.width);
    if(scrollView.contentOffset.x > scrollView.frame.size.width) {
        // We are moving forward. Load the current doc data on the first page.
//        [self loadPageWithId:currIndex onPage:0];
//        // Add one to the currentIndex or reset to 0 if we have reached the end.
//        currIndex = (currIndex >= [documentTitles count]-1) ? 0 : currIndex + 1;
//        [self loadPageWithId:currIndex onPage:1];
//        // Load content on the last page. This is either from the next item in the array
//        // or the first if we have reached the end.
//        nextIndex = (currIndex >= [documentTitles count]-1) ? 0 : currIndex + 1;
//        [self loadPageWithId:nextIndex onPage:2];
    }
    if(scrollView.contentOffset.x < scrollView.frame.size.width) {
        // We are moving backward. Load the current doc data on the last page.
//        [self loadPageWithId:currIndex onPage:2];
//        // Subtract one from the currentIndex or go to the end if we have reached the beginning.
//        currIndex = (currIndex == 0) ? [documentTitles count]-1 : currIndex - 1;
//        [self loadPageWithId:currIndex onPage:1];
//        // Load content on the first page. This is either from the prev item in the array
//        // or the last if we have reached the beginning.
//        prevIndex = (currIndex == 0) ? [documentTitles count]-1 : currIndex - 1;
//        [self loadPageWithId:prevIndex onPage:0];     
    }
    
    if (nil != _controlDelegate && [_controlDelegate respondsToSelector:@selector(MHScrollViewDidEndDecelerating:atPageIndex:)])
        [_controlDelegate MHScrollViewDidEndDecelerating:self atPageIndex:self.currentPageIndex];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{

    if (nil != _controlDelegate && [_controlDelegate respondsToSelector:@selector(MHScrollViewDidEndScrollingAnimation:)])
        [_controlDelegate MHScrollViewDidEndScrollingAnimation:self];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    [self pageChanged:[self currentPageIndex]];
}

@end
