//
//  MHScrollView.h
//  Arki
//
//  Created by ken on 2015. 3. 23..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MHScrollView;

@protocol MHScrollViewDelegate <NSObject>
@optional
- (void)MHScrollViewDidEndDecelerating:(MHScrollView *)pagingView atPageIndex:(NSInteger)pageIndex;
- (void)MHScrollViewDidEndScrollingAnimation:(MHScrollView *)scrollView;
- (void)MHScrollView:(MHScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex;
@end

@interface MHScrollView : UIScrollView

@property (nonatomic)int numberOfPages;

@property (nonatomic)int numberOfTotalPageCount;

@property (nonatomic)int numberOfDataCountPer;

@property (nonatomic)float lastXOffset;

@property (nonatomic, assign)   id<MHScrollViewDelegate>    controlDelegate;

- (void) initializeControl;

- (NSInteger)currentPageIndex;

- (CGPoint)offsetFromPage:(NSInteger)page;

- (void)moveToPage:(int)page;

@end

