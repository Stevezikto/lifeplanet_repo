//
//  ActionSheetPickerCustomPickerDelegate.h
//  ActionSheetPicker
//
//  Created by  on 13/03/2012.
//  Copyright (c) 2012 Club 15CC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActionSheetPicker.h"

@interface ActionSheetPickerWeightDelegate : NSObject <ActionSheetCustomPickerDelegate>
{
    NSArray *notesToDisplayForKey;
    NSArray *scaleNames;
    NSArray *scaleNames2;
    
    NSMutableArray *pounds, *kgs;
}

@property (nonatomic, strong) NSString *selectedKey;
@property (nonatomic, strong) NSString *selectedValue;
@property (nonatomic, strong) NSString *selectedScale;
@property (nonatomic, strong) NSString *selectedDecimal;
@property (nonatomic, strong) NSMutableDictionary *unitKeysAndValues;
@property (nonatomic, strong) NSMutableArray *unitKeys;
@property (nonatomic, strong) NSArray *unitFirst;
@property (nonatomic, strong) NSArray *unitSecond;
@property (nonatomic, strong) NSArray *unitThird;
@property (nonatomic) NSInteger selectedUnit;
@property (nonatomic) NSInteger actionSheetType;
@end
