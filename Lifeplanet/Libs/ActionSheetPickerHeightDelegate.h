//
//  ActionSheetPickerWeightDelegate.h
//  Arki
//
//  Created by ken on 2015. 4. 2..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActionSheetPicker.h"

@interface ActionSheetPickerHeightDelegate : NSObject <ActionSheetCustomPickerDelegate>

@property (nonatomic, strong) NSString *selectedKey;
@property (nonatomic, strong) NSString *selectedValue;
@property (nonatomic, strong) NSString *selectedScale;

@property (nonatomic, strong) NSMutableDictionary *unitKeysAndValues;
@property (nonatomic, strong) NSMutableArray *unitKeys;
@property (nonatomic, strong) NSArray *unitFirst;
@property (nonatomic, strong) NSArray *unitSecond;
@property (nonatomic) NSInteger selectedUnit;
@property (nonatomic) NSInteger actionSheetType;

@end
