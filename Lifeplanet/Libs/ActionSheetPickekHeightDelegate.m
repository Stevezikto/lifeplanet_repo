//
//  ActionSheetPickerWeightDelegate.m
//  Arki
//
//  Created by ken on 2015. 4. 2..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "ActionSheetPickerHeightDelegate.h"

@implementation ActionSheetPickerHeightDelegate

- (id)init
{
    if (self = [super init]) {
      
        _unitFirst = [self setUnitValue:@"cm"];
        _unitSecond = [self setUnitValue:@"ft"];
        
        self.selectedKey = @"cm";
        self.selectedValue = _unitFirst[0];
        
        _unitKeys = [NSMutableArray new];
        [_unitKeys addObject:@"cm"];
        [_unitKeys addObject:@"ft"];
        
        _unitKeysAndValues = [[NSMutableDictionary alloc] init];
        [_unitKeysAndValues setValue:_unitFirst forKey:@"cm"];
        [_unitKeysAndValues setValue:_unitSecond forKey:@"ft"];
        
        // 기본값 : height : 54 , weight : 24
        
        self.selectedValue = _unitFirst[54];
       
    }
    return self;
}

/////////////////////////////////////////////////////////////////////////
#pragma mark - ActionSheetCustomPickerDelegate Optional's
/////////////////////////////////////////////////////////////////////////
- (void)configurePickerView:(UIPickerView *)pickerView
{
    // Override default and hide selection indicator
    pickerView.showsSelectionIndicator = NO;
}

- (void)actionSheetPickerDidSucceed:(AbstractActionSheetPicker *)actionSheetPicker origin:(id)origin
{
    NSLog(@"");
    
    NSString *send;
    if([self.selectedKey isEqual:@"cm"])
        send = self.selectedKey;
    else
        send = @"";
    
    // symbol 설정
    BOOL isFeet = NO;
    
    NSRange subRange;
    subRange = [self.selectedKey rangeOfString :@"cm"];
    
    if (subRange.location == NSNotFound){
        isFeet = YES;
    }
    else{
        isFeet = NO;
    }
    
    NSLog(@"value : %@ , send : %@", self.selectedValue, send);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"heightPicker" object:nil userInfo:@{@"value": [NSString stringWithFormat:@"%@%@",self.selectedValue, send], @"feet": @(isFeet)}];
}

/////////////////////////////////////////////////////////////////////////
#pragma mark - UIPickerViewDataSource Implementation
/////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // Returns
    switch (component) {
        case 0: return [[self getValuesByKey:self.selectedKey] count];
        case 1: return [_unitKeys count];
        default:break;
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////
#pragma mark UIPickerViewDelegate Implementation
/////////////////////////////////////////////////////////////////////////

// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    switch (component) {
        case 0: return 100.0;
        case 1: return 100.0;
        default:break;
    }
    
    return 0;
}
/*- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
 {
 return
 }
 */
// these methods return either a plain UIString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0: return [self getValuesByKey:self.selectedKey][row];
        case 1: return _unitKeys[(NSUInteger) row];
        default:break;
    }
    return nil;
}

/////////////////////////////////////////////////////////////////////////

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"Row %li selected in component %li", (long)row, (long)component);
    
    NSInteger selectedRow;
    NSString *symbol = nil;
    NSString *result;
    
    switch (component) {
      
        case 0:
            self.selectedValue = [self getValuesByKey:self.selectedKey][(NSUInteger) row];
            return;
            
        case 1: // 선택시 단위 변경
            
            if([self.selectedKey isEqual:@"cm"]){
                NSLog(@"value : %@ , aaa : %@",self.selectedValue, [self returnRightMetric:self.selectedValue typeOfMetricUsed:@"cm"]);
                result = [self returnRightMetric:self.selectedValue typeOfMetricUsed:@"cm"];
                selectedRow = [self returnConvertedValue:result typeOfMetricUsed:@"cm"];
            }
            else{
                NSLog(@"value : %@ , aaa : %@",self.selectedValue, [self returnRightMetric:self.selectedValue typeOfMetricUsed:@"inches"]);
                result = [self returnRightMetric:self.selectedValue typeOfMetricUsed:@"inches"];
                selectedRow = [self returnConvertedValue:result typeOfMetricUsed:@"inches"];
            }
            
            NSLog(@"selectedRow : %ld", selectedRow);
            
            
            self.selectedKey = _unitKeys[(NSInteger) row];
            [pickerView reloadComponent:0];

//            self.selectedValue = [self getValuesByKey:self.selectedKey][(NSUInteger) [pickerView selectedRowInComponent:0]];
            self.selectedValue = [self getValuesByKey:self.selectedKey][selectedRow];
            
            [pickerView selectRow:selectedRow inComponent:0 animated:NO];
            
            NSLog(@"row : %ld , value : %@", [pickerView selectedRowInComponent:0] ,self.selectedValue);
            
            return;
        default:break;
    }
    
}

-(NSMutableArray *)getValuesByKey:(NSString *)unitKey
{
    NSMutableArray *citiesIncontinent = _unitKeysAndValues[unitKey];

    return citiesIncontinent;
};

- (NSMutableArray *)setUnitValue:(NSString *)key{
    
    NSMutableArray *values = [NSMutableArray new];
    
    if([key isEqual:@"cm"]){
        for(int i = 109; i < 272; i++){
            
            NSString *value = [NSString stringWithFormat:@"%d", i];
            [values addObject:value];
        }
    }
    else if([key isEqual:@"ft"]){
        int feet = 3;
        int inch = 7;
        
        for(int i = 0; i < 60; i++){
            
            if(inch > 11){
                inch = 0;
                feet ++;
            }
//”
            NSString *value = [NSString stringWithFormat:@"%d’%d”", feet, inch];
            [values addObject:value];
            inch ++;
        }
    }
    
    return values;
}

- (NSString *)returnRightMetric:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType
{
    NSString *result = nil;
    
    if ([metricType isEqualToString:@"inches"]) {
        if ([theMeasure isEqualToString:@""]) {
            return @"0";
        }
        NSArray* theConvertion = [theMeasure componentsSeparatedByCharactersInSet:
                                  [NSCharacterSet characterSetWithCharactersInString:@"’”"]];
        NSInteger value1 = [theConvertion[0] intValue];
        NSInteger value2 = [theConvertion[1] intValue];
        
        float number = ((value1 * 12) + value2) * 2.54;
//        result = [NSString stringWithFormat:@"%.0f cm", round(number * 100.0) / 100.0];
        result = [NSString stringWithFormat:@"%.0f", round(number * 100.0) / 100.0];
        
        
    } else if ([metricType isEqualToString:@"cm"]) {
        float value = [theMeasure floatValue];
        float number = value / 2.54;
        
        if (roundf( number) >= 12.0) {
            if ((int)round( number) % 12==0) {
                result = [NSString stringWithFormat:@"%i’%i”", (int)roundf(number / 12.0), (int)round( number) % 12];
            }else{
                result = [NSString stringWithFormat:@"%i’%i”", (int)floorf(number / 12.0), (int)round( number) % 12];
            }
        } else {
            result = [NSString stringWithFormat:@"0’%i”", (int)round(number)];
        }
    }
    NSLog(@"result: %@", result);
    return result;
}

- (NSInteger)returnConvertedValue:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType{
    
    NSLog(@"theMeasure: %@ , metricType :%@", theMeasure, metricType);
    
    NSInteger result = 0;
    
    if ([metricType isEqualToString:@"inches"]){
        for(int i = 0; i < _unitFirst.count; i++){
            
            if([theMeasure isEqualToString:_unitFirst[i]]){
                NSLog(@"i : %d , measure : %@", i , theMeasure);
                result = (NSInteger)i;
            }
        }
    }
    else{
        for(int i = 0; i < _unitSecond.count; i++){
            
            if([theMeasure isEqualToString:_unitSecond[i]]){
                NSLog(@"i : %d , measure : %@", i , theMeasure);
                result = (NSInteger)i;
            }
        }
    }
    
    return result;
}


@end
