//
//  SWBarChart.m
//  Arki
//
//  Created by ken on 2015. 3. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "SWBarChart.h"
#define BOTTOM_LINE_HEIGHT 2
#define HORIZONTAL_LINE_HEIGHT 0.5
#define Y_COORDINATE_LABEL_WIDTH 30
#define DIRECTION  (_columnsIndexStartFromLeft? - 1 : 1)

@interface SWBarChart()

@property (strong, nonatomic) NSMutableDictionary *eColumns;
@property (strong, nonatomic) NSMutableDictionary *eLabels;
@property (strong, nonatomic) SWBar *fingerIsInThisEColumn;
@property CGRect mFrame;

@property (nonatomic) float fullValueOfTheGraph;

@end


@implementation SWBarChart

@synthesize columnsIndexStartFromLeft = _columnsIndexStartFromLeft;
@synthesize showHighAndLowColumnWithColor = _showHighAndLowColumnWithColor;
@synthesize fingerIsInThisEColumn = _fingerIsInThisEColumn;
@synthesize minColumnColor = _minColumnColor;
@synthesize maxColumnColor = _maxColumnColor;
@synthesize normalColumnColor = _normalColumnColor;
@synthesize eColumns = _eColumns;
@synthesize eLabels = _eLabels;
@synthesize leftMostIndex = _leftMostIndex;
@synthesize rightMostIndex = _rightMostIndex;
@synthesize showHorizontalLabelsWithInteger = _showHorizontalLabelsWithInteger;
@synthesize fullValueOfTheGraph = _fullValueOfTheGraph;
@synthesize dataSource = _dataSource;
@synthesize delegate = _delegate;

#pragma -mark- Setter and Getter
- (void)setDelegate:(id<SWBarChartDelegate>)delegate
{
    if (_delegate != delegate)
    {
        _delegate = delegate;
        
        if (![_delegate respondsToSelector:@selector(eColumnChart: didSelectColumn:)])
        {
            NSLog(@"@selector(eColumnChart: didSelectColumn:) Not Implemented!");
            return;
        }
        
        if (![_delegate respondsToSelector:@selector(eColumnChart:fingerDidEnterColumn:)])
        {
            NSLog(@"@selector(eColumnChart:fingerDidEnterColumn:) Not Implemented!");
            return;
        }
        
        if (![_delegate respondsToSelector:@selector(eColumnChart:fingerDidLeaveColumn:)])
        {
            NSLog(@"@selector(eColumnChart:fingerDidLeaveColumn:) Not Implemented!");
            return;
        }
        
        if (![_delegate respondsToSelector:@selector(fingerDidLeaveEColumnChart:)])
        {
            NSLog(@"@selector(fingerDidLeaveEColumnChart:) Not Implemented!");
            return;
        }
    }
}

- (void)setDataSource:(id<SWBarChartDataSource>)dataSource
{
    if (_dataSource != dataSource)
    {
        _dataSource = dataSource;
        
        if (![_dataSource respondsToSelector:@selector(numberOfColumnsInSWBarChart:)])
        {
            NSLog(@"@selector(numberOfColumnsInEColumnChart:) Not Implemented!");
            return;
        }
        
        if (![_dataSource respondsToSelector:@selector(numberOfColumnsPresentedEveryTime:)])
        {
            NSLog(@"@selector(numberOfColumnsPresentedEveryTime:) Not Implemented!");
            return;
        }
        
        if (![_dataSource respondsToSelector:@selector(highestValueSWBarChart:)])
        {
            NSLog(@"@selector(highestValueEColumnChart:) Not Implemented!");
            return;
        }
        
        if (![_dataSource respondsToSelector:@selector(SWBarChart:valueForIndex:)])
        {
            NSLog(@"@selector(eColumnChart:valueForIndex:) Not Implemented!");
            return;
        }
        
        {
            NSInteger totalColumnsRequired = 0;
            totalColumnsRequired = [_dataSource numberOfColumnsPresentedEveryTime:self]; // 한번에 보여지는 수
            NSInteger totalColumns = 0;
            totalColumns = [_dataSource numberOfColumnsInSWBarChart:self]; // 토탈 수
            /** Currently only support columns layout from right to left, WILL ADD OPTIONS LATER*/
            if (_columnsIndexStartFromLeft)
            {
                _leftMostIndex = 0;
                _rightMostIndex = _rightMostIndex + totalColumnsRequired - 1;
            }
//            else
//            {
//                _rightMostIndex = 0;
//                _leftMostIndex = _rightMostIndex + totalColumnsRequired - 1;
//            }

            /** Start construct horizontal lines*/
            /** Start construct value labels for horizontal lines*/
            if (_showHorizontalLabelsWithInteger)
            {
                NSInteger valueGap = [_dataSource highestValueSWBarChart:self].value / 10 + 1;
                NSInteger horizontalLabelsCount = [_dataSource highestValueSWBarChart:self].value / valueGap + 1;
                _fullValueOfTheGraph = valueGap * horizontalLabelsCount;
            }
            else
            {
                /** In order to leave some space for the heightest column */
                _fullValueOfTheGraph = [_dataSource highestValueSWBarChart:self].value * 1.1;
                _fullValueOfTheGraph = 75.0;
//                NSLog(@"highest : %f", [_dataSource highestValueSWBarChart:self].value * 1.1);
               
            }
//            if(_showBadWalkingLabel){
//                NSInteger valueGap = [_dataSource highestValueSWBarChart:self].value / 10 + 1;
//                NSInteger horizontalLabelsCount = [_dataSource highestValueSWBarChart:self].value / valueGap + 1;
//                _fullValueOfTheGraph = valueGap * horizontalLabelsCount;
//            }
//            else{
//                /** In order to leave some space for the heightest column */
//                _fullValueOfTheGraph = [_dataSource highestValueSWBarChart:self].value * 1.1;
//            }
        }
        
        [self reloadData];
    }
}
- (void)setMaxColumnColor:(UIColor *)maxColumnColor
{
    _maxColumnColor = maxColumnColor;
    [self reloadData];
}

- (void)setMinColumnColor:(UIColor *)minColumnColor
{
    _minColumnColor = minColumnColor;
    [self reloadData];
}

- (void)setShowHighAndLowColumnWithColor:(BOOL)showHighAndLowColumnWithColor
{
    _showHighAndLowColumnWithColor = showHighAndLowColumnWithColor;
    [self reloadData];
}

- (void)setColumnsIndexStartFromLeft:(BOOL)columnsIndexStartFromLeft
{
    if (_dataSource)
    {
        NSLog(@"setColumnsIndexStartFromLeft Should Be Called Before Setting Datasource!");
        return;
    }
    _columnsIndexStartFromLeft = columnsIndexStartFromLeft;
}


#pragma -mark- Custom Methed
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _mFrame = frame;
        /** Should i release these two objects before self have been destroyed*/
        _eLabels = [NSMutableDictionary dictionary];
        _eColumns = [NSMutableDictionary dictionary];
        
        NSLog(@"frame; % f",frame.size.width);
        [self initData];
    }
    return self;
}



- (void)initData
{
    /** Initialize colors for max and min column*/
    _minColumnColor = DEFAULT_BLUE;
    _maxColumnColor = DEFAULT_GREEN;
    _normalColumnColor = DEFAULT_YELLOW;
    _showHighAndLowColumnWithColor = YES;
    
}

- (void)reloadData
{
    if (nil == _dataSource)
    {
        NSLog(@"Important!! DataSource Not Set!");
        return;
    }

    NSInteger totalColumnsRequired = 0;
    totalColumnsRequired = [_dataSource numberOfColumnsPresentedEveryTime:self];
    
//    float heightOfTheColumnShouldBe = self.frame.size.height / (float)(totalColumnsRequired + (totalColumnsRequired + 1) * 2);
    float heightOfTheColumnShouldBe = (53.0);
    float heightSpaceBetweenCloumn = 59.0; // 화면 비율 크기로 바꿔야함
    
    for (int i = 0; i < totalColumnsRequired; i++)
    {
        NSInteger currentIndex = _leftMostIndex - i * DIRECTION;
        SWBarData *eColumnDataModel = [_dataSource SWBarChart:self valueForIndex:currentIndex];
        if (eColumnDataModel == nil)
            eColumnDataModel = [[SWBarData alloc] init];
        
        /** Construct Columns*/
        SWBar *eColumn = [_eColumns objectForKey: [NSNumber numberWithInteger:currentIndex ]];
        if (nil == eColumn)
        {
            eColumn = [[SWBar alloc] initWithFrame:CGRectMake(0,
                                                              (i * (heightOfTheColumnShouldBe + heightSpaceBetweenCloumn)),
                                                              _mFrame.size.width,
                                                              heightOfTheColumnShouldBe)];
            
            NSLog(@"eColumn.w : %f", eColumn.frame.size.width);
            
            eColumn.backgroundColor = [UIColor clearColor];
            eColumn.walkingTime = eColumnDataModel.value;
            eColumn.grade = eColumnDataModel.value / _fullValueOfTheGraph; // eColumnDataModel.value는 each model 값, fullValue는 최대값, 최대값에서 해당 model의 값을 나눈 비율로 표시하는 듯
            eColumn.eColumnDataModel = eColumnDataModel;
            
            NSLog(@"grade : %f / eColumnDataModel.value: :%f / _fullValueOfTheGraph :%f ",eColumn.grade, eColumnDataModel.value, _fullValueOfTheGraph);
            
            float progressLineWidth = (eColumn.grade) * _mFrame.size.width;
            
            NSLog(@"progressLineWidth : %f", progressLineWidth);
            
            [eColumn.pedoTypeView setFrame:CGRectMake(progressLineWidth,
                                                      eColumn.pedoTypeView.frame.origin.y,
                                                      eColumn.pedoTypeView.frame.size.width,
                                                      eColumn.pedoTypeView.frame.size.height)];
            
            NSLog(@"eColumn.pedoTypeView.frame.size.width : %f",eColumn.pedoTypeView.frame.size.width);
            
//            NSLog(@"grade : %f / full : %f", eColumn.grade, _fullValueOfTheGraph);
            NSString *userPedoType = [self returnWalkingType:eColumnDataModel.unit.integerValue];
            float originalPedoLabelY = eColumn.lbPedoType.frame.origin.y;
            
            CGSize labelNewSize = [self getSizeForText:userPedoType maxWidth:MAXFLOAT font:@"Helvetica-Bold" fontSize:eColumn.lbPedoType.font.pointSize];
            
            // 20 분 이상일 때
            if(eColumnDataModel.value >= 20){
                
                eColumn.barColor = _normalColumnColor;
                eColumn.lbPedoType.textColor = DEFAULT_BLUE;
                
                NSLog(@"time : %f , type : %f", eColumn.lbTime.frame.size.width, eColumn.lbPedoType.frame.origin.x);
               
//                eColumn.lbTime.backgroundColor = [UIColor yellowColor];
//                eColumn.lbPedoType.backgroundColor = [UIColor grayColor];
                
                eColumn.lbPedoType.frame = CGRectMake(progressLineWidth - eColumn.lbPedoType.frame.size.width - 12.0,
                                                      originalPedoLabelY,
                                                      labelNewSize.width, labelNewSize.height);
                
                if(CGRectIntersectsRect(eColumn.lbTime.frame, eColumn.lbPedoType.frame)){
                    
                    eColumn.lbPedoType.frame = CGRectMake(eColumn.lbTime.frame.size.width,
                                                          originalPedoLabelY,
                                                          labelNewSize.width, labelNewSize.height);
                }
                
//                eColumn.lbPedoType.frame = CGRectMake(progressLineWidth - eColumn.lbPedoType.frame.size.width - 12.0,
//                                                      originalPedoLabelY,
//                                                      labelNewSize.width, labelNewSize.height);
            }
            else{
                eColumn.barColor = [UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.6];
                eColumn.lbPedoType.textColor = [UIColor whiteColor];
                eColumn.lbPedoType.frame = CGRectMake(eColumn.pedoTypeView.frame.origin.x + eColumn.pedoTypeView.frame.size.width,
                                                      originalPedoLabelY,
                                                      labelNewSize.width, labelNewSize.height);
                
            }
            eColumn.lbPedoType.text = userPedoType;
            eColumn.lbTime.text = [NSString stringWithFormat:@"%@", eColumnDataModel.date];
            eColumn.pedoTypeView.image = [UIImage imageNamed:[self returnWalkingImage:eColumnDataModel.unit.integerValue]];

            [self addSubview:eColumn];
            
            [_eColumns setObject:eColumn forKey:[NSNumber numberWithInteger:currentIndex ]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"columnEnd" object:nil];
        }
    }
}

- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = MAXFLOAT;
    constraintSize.width = width;
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}

- (NSString *)returnWalkingType:(NSInteger)userWalkingType{
    
    NSString *type;
    
    switch (userWalkingType) {
        case WalkTypePower:
            type = @"Power";
            break;
        case WalkTypeGood:
            type = @"Good";
            break;
        case WalkTypeNormal:
            type = @"Normal";
            break;
        case WalkTypeBad:
            type = @"Bad";
            break;
        case WalkTypeSmartphone:
            type = @"Smart phone";
            break;
        case WalkTypePocket:
            type = @"In pocket";
            break;
        default:
            type = @"Normal";
            break;

    }
    return type;
}

- (NSString *)returnWalkingImage:(NSInteger)userWalkingType{
    
    NSString *imageName;
    
    switch (userWalkingType) {
        case WalkTypePower:
            imageName = @"sound_walking_power";
            break;
        case WalkTypeGood:
            imageName = @"sound_walking_good";
            break;
        case WalkTypeNormal:
            imageName = @"sound_walking_normal";
            break;
        case WalkTypeBad:
            imageName = @"sound_walking_bad";
            break;
        case WalkTypeSmartphone:
            imageName = @"sound_walking_smart_phone";
            break;
        case WalkTypePocket:
            imageName = @"sound_walking_inpocket";
            break;
        default:
            imageName = @"sound_walking_normal";
            break;
            
    }
    return imageName;
}

#pragma -mark- EColumnDelegate


#pragma -mark- detect Gesture

@end
