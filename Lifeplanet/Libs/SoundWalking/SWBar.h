//
//  SWBar.h
//  Arki
//
//  Created by ken on 2015. 3. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWBarData.h"

@class SWBar;

@protocol EColumnDelegate <NSObject>

- (void)eColumnTaped:(SWBar *)eColumn;


@end

@interface SWBar : UIView

@property (nonatomic) float grade;

@property (nonatomic,strong) CAShapeLayer * chartLine;

@property (nonatomic, strong) UIColor * barColor;

@property (nonatomic, strong) SWBarData *eColumnDataModel;

@property (weak, nonatomic) id <EColumnDelegate> delegate;

@property (strong, nonatomic) UILabel *lbTime;

@property (strong, nonatomic) UILabel *lbPedoType;

@property (strong, nonatomic) UIImageView *pedoTypeView;

@property (nonatomic) float walkingTime;

-(void)rollBack;



@end
