//
//  SWBarChart.h
//  Arki
//
//  Created by ken on 2015. 3. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWBar.h"
#import "SWBarData.h"

@class SWBarChart;



@protocol SWBarChartDataSource <NSObject>

/** How many Columns are there in total.*/
- (NSInteger) numberOfColumnsInSWBarChart:(SWBarChart *) eColumnChart;

/** How many Columns should be presented on the screen each time*/
- (NSInteger) numberOfColumnsPresentedEveryTime:(SWBarChart *) eColumnChart;

/** The hightest vaule among the whole chart*/
- (SWBarData *)     highestValueSWBarChart:(SWBarChart *) eColumnChart;

/** Value for each column*/
- (SWBarData *)     SWBarChart:(SWBarChart *) SWBarChart
                          valueForIndex:(NSInteger)index;

@optional
/** Allow you to customize the color of every coloum as you wish.*/
- (UIColor *) colorForEColumn:(SWBar *)eColumn;

/** New protocals coming soon, will allow you to customize column*/



@end


@protocol SWBarChartDelegate <NSObject>

/** When finger single taped the column*/
- (void)        eColumnChart:(SWBarChart *) eColumnChart
             didSelectColumn:(SWBar *) eColumn;

/** When finger enter specific column, this is dif from tap*/
- (void)        eColumnChart:(SWBarChart *) eColumnChart
        fingerDidEnterColumn:(SWBar *) eColumn;

/** When finger leaves certain column, will
 tell you which column you are leaving*/
- (void)        eColumnChart:(SWBarChart *) eColumnChart
        fingerDidLeaveColumn:(SWBar *) eColumn;

/** When finger leaves wherever in the chart,
 will trigger both if finger is leaving from a column */
- (void) fingerDidLeaveEColumnChart:(SWBarChart *)eColumnChart;

@end


@interface SWBarChart : UIView

@property (nonatomic, readonly) NSInteger leftMostIndex;
@property (nonatomic, readonly) NSInteger rightMostIndex;

@property (nonatomic, strong) UIColor *minColumnColor;
@property (nonatomic, strong) UIColor *maxColumnColor;
@property (nonatomic, strong) UIColor *normalColumnColor;

@property (nonatomic) BOOL showHighAndLowColumnWithColor;
@property (nonatomic) BOOL showBadWalkingLabel;

/** If this switch in on, all horizontal labels will show in Integer. */
@property (nonatomic) BOOL showHorizontalLabelsWithInteger;

/** IMPORTANT:
 This should be setted before datasoucre has been set.*/
@property (nonatomic) BOOL columnsIndexStartFromLeft;

- (void)initData;

/** Call to redraw the whole chart*/
- (void)reloadData;

@property (weak, nonatomic) id <SWBarChartDataSource> dataSource;
@property (weak, nonatomic) id <SWBarChartDelegate> delegate;

@end