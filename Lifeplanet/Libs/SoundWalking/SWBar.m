//
//  SWBar.m
//  Arki
//
//  Created by ken on 2015. 3. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "SWBar.h"
#import <QuartzCore/QuartzCore.h>

@implementation SWBar
@synthesize barColor = _barColor;
@synthesize eColumnDataModel = _eColumnDataModel;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        // Initialization code
//        _chartLine              = [CAShapeLayer layer];
//        _chartLine.lineCap      = kCALineCapButt;
//        _chartLine.fillColor    = [[UIColor whiteColor] CGColor];
//        _chartLine.lineWidth    = self.frame.size.height / 1.5;
//        _chartLine.strokeEnd    = 0.0;
//        self.clipsToBounds      = true;
//        [self.layer addSublayer:_chartLine];
//        
//        _walkingTime = 0;
//        
//        _lbTime = [[UILabel alloc] initWithFrame:CGRectMake(10.0, self.frame.size.height/4, 60.0, self.frame.size.height/2)];
//        [_lbTime setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0]];
////        _lbTime.minimumScaleFactor = 0.5f;
//        _lbTime.adjustsFontSizeToFitWidth = YES;
//        [_lbTime setMinimumScaleFactor:0.6f];
//        _lbTime.textColor = DEFAULT_BLUE;
//        [self addSubview:_lbTime];
//        
//        _lbPedoType = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 40.0, _lbTime.frame.origin.y + 6, 50.0, self.frame.size.height/2)];
//        [_lbPedoType setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
//        _lbPedoType.textColor = DEFAULT_BLUE;
//        [self addSubview:_lbPedoType];
//        
//        _pedoTypeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sound_walking_good"]];
//        [self addSubview:_pedoTypeView];
//        
//        _lbPedoType.adjustsFontSizeToFitWidth=YES;
//        _lbPedoType.minimumScaleFactor=0.5;

    }
    return self;
}



-(void)setGrade:(float)grade
{
//    NSLog(@"self.h : %f / height : %f", self.frame.origin.y, self.frame.size.height);
    
    _grade = grade;
    UIBezierPath *progressline = [UIBezierPath bezierPath];
    
    [progressline moveToPoint:CGPointMake(0, self.frame.size.height/2.0)];
    [progressline addLineToPoint:CGPointMake((grade) * self.frame.size.width, self.frame.size.height/2.0)];
    
    [progressline setLineWidth:1.0];
    [progressline setLineCapStyle:kCGLineCapSquare];
    _chartLine.path = progressline.CGPath;

    _chartLine.strokeColor = [_barColor CGColor];

    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.5;
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [_chartLine addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
    
    _chartLine.strokeEnd = 1.0;
    
//    float progressLineWidth = (grade) * self.frame.size.width;
//    
//    [_lbPedoType setFrame:CGRectMake(progressLineWidth - _lbPedoType.frame.size.width - 12.0, _lbPedoType.frame.origin.y, _lbPedoType.frame.size.width, _lbPedoType.frame.size.height)];
//    
//    [_pedoTypeView setFrame:CGRectMake(progressLineWidth, _pedoTypeView.frame.origin.y, _pedoTypeView.frame.size.width, _pedoTypeView.frame.size.height)];
//    
//    NSLog(@"walking : %f", self.walkingTime);
//    
//    if(self.walkingTime >= 20){
//        _lbPedoType.text = @"Good";
//    }
//    else{
//        self.alpha = 0.6;
//        _lbPedoType.text = @"smart phone";
//        [_lbPedoType setTextColor:[UIColor whiteColor]];
//        [_lbPedoType setFrame:CGRectMake(_pedoTypeView.frame.origin.x + _pedoTypeView.frame.size.width,
//                                         _pedoTypeView.frame.origin.y,
//                                         _pedoTypeView.frame.size.width + 60.0,
//                                         _pedoTypeView.frame.size.height)];
//        
//    }
    
}

- (void)setBarColor:(UIColor *)barColor
{
    _chartLine.strokeColor = [barColor CGColor];
}

- (UIColor *)barColor
{
    return [UIColor colorWithCGColor:_chartLine.strokeColor];
}

//- (void)setLabelTitle:(NSString *)title{
//    
//    _lbTime.text = title;
//}

-(void)rollBack{
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _chartLine.strokeColor = [UIColor clearColor].CGColor;
    } completion:nil];
    
    
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //Draw BG
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);
    CGContextFillRect(context, rect);
    
}
@end
