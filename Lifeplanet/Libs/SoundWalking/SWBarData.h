//
//  SWBarData.h
//  Arki
//
//  Created by ken on 2015. 3. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWBarData : NSObject

@property (strong, nonatomic) NSString *label;
@property (nonatomic) float value;
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *date;
//- (id)initWithLabel:(NSString *)label
//              value:(float)vaule
//              index:(NSInteger)index
//               unit:(NSString *)unit;
- (id)initWithLabel:(NSString *)label
              value:(float)vaule
              index:(NSInteger)index
               unit:(NSString *)unit
               date:(NSString *)date;

@end
