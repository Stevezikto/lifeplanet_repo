//
//  SWBarData.m
//  Arki
//
//  Created by ken on 2015. 3. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "SWBarData.h"

@implementation SWBarData

@synthesize label = _label;
@synthesize value = _value;
@synthesize index = _index;
@synthesize unit  = _unit;

- (id)init
{
    self = [super init];
    if (self)
    {
        _label = @"Empty";
        _value = 0;
    }
    return self;
}

- (id)initWithLabel:(NSString *)label
              value:(float)vaule
              index:(NSInteger)index
               unit:(NSString *)unit
               date:(NSString *)date
{
    self = [self init];
    if (self)
    {
        if (nil == label)
        {
            _label = @"";
        }
        else
        {
            _label = label;
        }
        
        if (nil == unit)
        {
            _unit = @"";
        }
        else
        {
            _unit = unit;
        }
        _value = vaule;
        _index = index;
        _date = date;
    }
    return self;
}


@end
