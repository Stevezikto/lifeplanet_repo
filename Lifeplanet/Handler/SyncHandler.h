//
//  SyncHandler.h
//  Arki
//
//  Created by ken on 2015. 6. 23..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncHandler : NSObject{
    
}

@property (strong, nonatomic)NSData *syncedData;
@property (strong, nonatomic)NSDate *lastSyncedDate;
@property (nonatomic)NSInteger pedoCountForToday, resyncCount;

+ (SyncHandler *)sharedInstance;;

@end
