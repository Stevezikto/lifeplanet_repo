//
//  UserHandler.m
//  Arki
//
//  Created by ken on 2015. 4. 14..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UserHandler.h"
#import "UserData.h"
#import <Realm/Realm/Realm.h>

#define BIRTH "05-08-1983"

@implementation UserHandler

+ (void)createNewUser:(PFUser *)parameters complete:(void (^)(BOOL coomplete))complete{

    NSLog(@"params : %@", parameters);
    
    [self hasUserImageFile:parameters complete:^(UIImage *image) {
        NSLog(@"image from parse : %@", image);
    }];

    RLMRealm *realm = [RLMRealm defaultRealm];
  
    [realm beginWriteTransaction];
    
    UserData *user          = [UserData new];
    
    NSString *uuid          = parameters.objectId;
    NSString *deviceUUID    = parameters[@"deviceUUID"];
    NSString *name          = parameters[@"name"];
    int gender              = [parameters[@"gender"] intValue];
    NSString *birth         = parameters[@"age"];

    NSDictionary *physicalInfo = [self appendSymbol:parameters];
    NSLog(@"physicalInfo : %@", physicalInfo);
    
    // null cm 들어옴
    NSString *height        = physicalInfo[@"height"];
    NSString *weight        = physicalInfo[@"weight"];
    
    NSString *day           = parameters[@"arkiday"];
    int ziktoDay            = 1;
    
    if(!deviceUUID)
        user.deviceUUID = @"";
    else
        user.deviceUUID = deviceUUID;
    
    if(!uuid)
        user.uuid = @"";
    else
        user.uuid = uuid;
    
    if(!name)
        user.name = @"";
    else
        user.name = name;
    
    if(!gender)
        user.gender = 1;
    else
        user.gender = gender;
    
    if(!birth)
        user.age = @"05-08-1983";
    else
        user.age = birth;
    
    if(!height)
        user.height = 0;
    else
        user.height = [height intValue];
    
    if(!weight)
        user.weight = 0;
    else
        user.weight = [weight intValue];
    
    if(!day)
        user.day = NSLocalizedString(@"sunday", @"Sunday");
    else
        user.day = day;
    
    user.ziktoDay = ziktoDay;
    
    
    user.imageUrl = [GeneralUtil loadFileWithName:@"image.png"];
    user.coachingLevel = 2;// default

    [realm addObject:user];
    [realm commitWriteTransaction];

    complete(YES);
    
    NSLog(@"Create User : %@", user);
}

+ (void)updateUser:(PFUser *)pfUser{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"uuid = %@",pfUser.objectId];
    
    [UserHandler fetchUserData:pred complete:^(BOOL complete, UserData *user) {
        if(user){
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            
            UserData *new                   = [UserData new];
            new.uuid                        = user.uuid;
            new.deviceUUID                  = user.deviceUUID;
            new.name                        = user.name;
            int gender                      = [pfUser[@"gender"] intValue];
            new.gender                      = gender;
            new.imageUrl                    = [GeneralUtil loadFileWithName:@"image.png"];
            new.age = user.age;
            
            if(!new.age)
                new.age                     = @"1983-02-19";

            if(pfUser[@"isFeet"] == NO)
                new.isFeet                  = NO;
            if(pfUser[@"isPound"] == NO)
                new.isPound                 = NO;
            
            if(!pfUser[@"height"])
                user.height                 = 0;
            if(!pfUser[@"weight"])
                user.weight                 = 0;


            new.day = pfUser[@"arkiday"];
            new.coachingLevel = user.coachingLevel;
            new.stepCount = user.stepCount;
            new.sleepTarget = user.sleepTarget;
            new.soundWalking = 2;
            
            [UserData createOrUpdateInRealm:realm withValue:new];
            
            [realm commitWriteTransaction];
        }
    }];
}

+ (UserData *)fetchData:(void (^)(BOOL complete))complete{
    
    RLMResults *result = [UserData allObjects];
    UserData *user = result[0];
    
//    complete(YES);
    
    return user;
}

+ (void)fetchUserData:(NSPredicate *)pred complete:(void (^)(BOOL complete, UserData* user))complete{
    
    RLMResults *result = [UserData objectsWithPredicate:pred];
    NSLog(@"result :%@ count : %ld", result, result.count);
    
    if(result.count){
        
        UserData *user = result[0];
     
        complete(YES, user);
    }
}

+ (int)fetchUserCoachingLevel{
    
    RLMResults *result = [UserData allObjects];
    UserData *user = result[0];
    NSLog(@"user:%@", user);
    return user.coachingLevel + 2;
}

+ (void)hasUserImageFile:(PFUser *)pfUser complete:(void (^)(UIImage *image))complete{
    
    NSLog(@"parse uuid : %@", pfUser);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"userId = %@",pfUser];

    PFQuery *query = [PFQuery queryWithClassName:@"Upload" predicate:predicate];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!object) {
            NSLog(@"The getFirstObject request failed.");
        } else {
            // The find succeeded.
            NSLog(@"Successfully retrieved the object.");
         
            [query getObjectInBackgroundWithId:object.objectId block:^(PFObject *gameScore, NSError *error) {
            
                PFFile *userImageFile = gameScore[@"imageFile"];
                NSLog(@"userimagefile : %@", userImageFile);
                
                [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                    if (!error) {
                        
                        UIImage *image = [UIImage imageWithData:imageData];
            
                        complete(image);
                        
                        // Local에 가져온 이미지 저장
                        [GeneralUtil saveFile:image fileName:@"image.png" complete:^(BOOL complete) {
                            if(complete)
                                NSLog(@"이미지 로컬에 저장");
                        }];
                    }
                }];
            }];
        }        
    }];
}

+ (void)saveUserData:(NSDictionary *)user complete:(void (^)(BOOL complete))complete{
  
    NSAssert(user != nil, @"user data must not be nil");
    
    PFUser *currentUser = [PFUser currentUser];
    NSLog(@"user:%@ / pfuser : %@", user, currentUser);
    
    dispatch_async( dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 ), ^(void)
                   {
                       // Image 로컬에 저장
                       NSData *imageData = user[@"image"];
                       NSString *imageName = [NSString stringWithFormat:@"%@.png",currentUser.objectId];
                                              
                       UIImage *image = [UIImage imageWithData:imageData];

                       [GeneralUtil saveFile:image fileName:@"image.png" complete:^(BOOL complete) {
                           
                           if(complete){
                               
                               NSLog(@"image save success");
                           }
                       }];
                       
                       // Image save to parse
                       
                       PFFile *imageFile = [PFFile fileWithName:imageName data:imageData];
                                              
                       [self saveImageToServer:currentUser image:imageFile complete:^(BOOL success) {
                           
                           if(complete){
                               
                               NSLog(@"이미지 파스에 저장");
                           }
                       }];
                       
                       // Parse Update
                       
                       [self updateParseUser:user complete:^(BOOL complete) {
                           
                           if(complete){
                               NSLog(@"파스 정보 업데이트");
                           }
                       }];
                       
                       // Local DB Update
                       
                       [self updateLocalUser:user parseUser:currentUser complete:^(BOOL complete) {
                           
                           if(complete){
                               NSLog(@"DB 사용자 업데이트");
                           }
                       }];
                       
                       
                       [SVProgressHUD dismiss];
                       
                       complete(YES);
                       
//                       // Parse
//                       [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                           
//                           if(error){
//                               complete(NO);
//                           }
//                           if(succeeded){
//                               
//                               NSLog(@"pfuser : %@", currentUser);
//                               
//                               
//                           }
//                       } progressBlock:^(int percentDone) {
////                           NSLog(@"percent : %d", percentDone);
//                           
//                           if(percentDone == 100){
////                               complete(YES);
//                           }
//                       }];
                   });
}

+ (void)saveImageToServer:(PFUser *)user image:(id)imageFile complete:(void (^)(BOOL success))complete{

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"userId = %@",user];
  
    PFQuery *query = [PFQuery queryWithClassName:@"Upload" predicate:predicate];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
      
        if (!object) {
            NSLog(@"The getFirstObject request failed.");
            
            PFObject *userPhoto = [PFObject objectWithClassName:@"Upload"];
            userPhoto[@"imageFile"] = imageFile;
            userPhoto[@"userId"]    = user;
            
            [userPhoto saveInBackground];
            
            NSLog(@"프로필 사진 파스 저장");
            
        } else {
            // The find succeeded.
            NSLog(@"Successfully retrieved the object.");
            
            object[@"imageFile"] = imageFile;
            object[@"userId"]    = user;
            
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    // The object has been saved.
                } else {
                    // There was a problem, check error.description
                }
            }];
        }
    }];
    
}

+ (void)updateParseUser:(id)user complete:(void (^)(BOOL complete))complete{
    
    NSLog(@"파스 업데이트 시작 : user : %@", user);
    
    NSString *name,*gender,*age,*arkiDay;
    
    int coachingLevel,stepCount,soundWalking,sleepTarget,ziktoDay,height,weight;
    
    BOOL isPound;
    BOOL isFeet;
    
    isFeet                       = NO;
    isPound                      = NO;
    
    name                         = user[@"name"];
    gender                       = user[@"gender"];
    age                          = user[@"age"];
    
    height                       = [user[@"height"] intValue];
    weight                       = [user[@"weight"] intValue];
    
    arkiDay                      = user[@"day"];
    ziktoDay                     = [user[@"ziktoDay"] intValue]; // 1 ~ 7 , 1 :sun
    coachingLevel                = [user[@"coaching"] intValue];
    stepCount                    = [user[@"step"] intValue];
    soundWalking                 = [user[@"soundWalking"] intValue];
    sleepTarget                  = [user[@"sleep"] intValue];
    isPound                      = [user[@"pounds"] boolValue];
    isFeet                       = [user[@"feet"] boolValue];
    
    // Gender 세팅
    __block int isGender         = 1;
    
    if([gender isEqual:NSLocalizedString(@"female", @"Female")])
        isGender                 = 2;
    
    PFUser *currentUser = [PFUser currentUser];
    
    currentUser[@"name"]         = name;
    currentUser[@"gender"]       = @(isGender);
    currentUser[@"age"]          = age;
    
    currentUser[@"height"]       = @(height);
    currentUser[@"weight"]       = @(weight);
    
    currentUser[@"ziktoDay"]     = @(ziktoDay);
    currentUser[@"isPound"]      = @(isPound);
    currentUser[@"isFeet"]       = @(isFeet);
    
    [[PFUser currentUser] saveInBackground];
}

+ (void)updateLocalUser:(id)user parseUser:(PFUser *)currentUser complete:(void (^)(BOOL complete))success{
    
    NSString *name,*gender,*age,*arkiDay;
    
    int coachingLevel,stepCount,soundWalking,sleepTarget,ziktoDay,height,weight;
    
    BOOL isPound;
    BOOL isFeet;
    
    isFeet                       = NO;
    isPound                      = NO;
    
    name                         = user[@"name"];
    gender                       = user[@"gender"];
    age                          = user[@"age"];
    
    height                       = [user[@"height"] intValue];
    weight                       = [user[@"weight"] intValue];
    
    arkiDay                      = user[@"day"];
    ziktoDay                     = [user[@"ziktoDay"] intValue]; // 1 ~ 7 , 1 :sun
    coachingLevel                = [user[@"coaching"] intValue];
    stepCount                    = [user[@"step"] intValue];
    soundWalking                 = [user[@"soundWalking"] intValue];
    sleepTarget                  = [user[@"sleep"] intValue];
    isPound                      = [user[@"pounds"] boolValue];
    isFeet                       = [user[@"feet"] boolValue];
    
    // Gender 세팅
    __block int isGender         = 1;
    
    if([gender isEqual:NSLocalizedString(@"female", @"Female")])
        isGender                 = 2;
    
    
    // DB 갱신
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"uuid = %@",currentUser.objectId];
    
    [UserHandler fetchUserData:pred complete:^(BOOL complete, UserData *user) {
       
        if(complete){
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            
            UserData *new        = [UserData new];
            new.uuid             = user.uuid;
            new.deviceUUID       = user.deviceUUID;
            new.name             = name;
            new.gender           = isGender;
            new.imageUrl         = [GeneralUtil loadFileWithName:@"image.png"];;
            new.age              = age;
            
            new.height           = height;
            new.weight           = weight;
            
            new.day              = arkiDay;
            new.ziktoDay         = ziktoDay;
            new.coachingLevel    = coachingLevel;
            new.stepCount        = stepCount;
            new.sleepTarget      = sleepTarget;
            new.soundWalking     = soundWalking;
            new.isPound          = isPound;
            new.isFeet           = isFeet;
            
            [UserData createOrUpdateInRealm:realm withValue:new];
            
            [realm commitWriteTransaction];
            
            success(YES);
            
            NSLog(@"new : %@", new);
        }
    }];
}

+ (NSDictionary *)appendSymbol:(PFUser *)user {
    
    BOOL isFeet      = [user[@"isFeet"] boolValue];
    BOOL isPound     = [user[@"isPound"] boolValue];
    
    NSString *height = [user[@"height"] stringValue];
    NSString *weight = [user[@"weight"] stringValue];

    // Default
    if(!height)
        height = @"163";
    if(!weight)
        weight = @"63";
    
    if(!isFeet)
        height = [NSString stringWithFormat:@"%@cm", height];
    else{
        height = [self returnRightMetric:height typeOfMetricUsed:@"cm"];
//        height = [NSString stringWithFormat:@"%@’", height];
    }
    if(!isPound)
        weight = [NSString stringWithFormat:@"%@kg", weight];
    else
        weight = [NSString stringWithFormat:@"%@lb", weight];
    
    return @{@"isFeet"  :@(isFeet),
             @"isPound" :@(isPound),
             @"height"  :height,
             @"weight"  :weight};
}

+ (void)storeDeviceUUID:(NSString *)uuid{
    
    PFUser *user = [PFUser currentUser];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"uuid = %@",user.objectId];
    [UserHandler fetchUserData:pred complete:^(BOOL complete, UserData *user) {
        
        if(user){
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            
            UserData *new = [UserData new];
            new.uuid = user.uuid;
            new.deviceUUID = uuid;
            new.name = user.name;
            new.gender = user.gender;
            new.imageUrl = user.imageUrl;
            new.age = user.age;
            new.height = user.height;
            new.weight = user.weight;
            new.day = user.day;
            new.ziktoDay = user.ziktoDay;
            new.coachingLevel = user.coachingLevel;
            new.stepCount = user.stepCount;
            new.sleepTarget = user.sleepTarget;
            new.soundWalking = user.soundWalking;
            
            [UserData createOrUpdateInRealm:realm withValue:new];
            
            [realm commitWriteTransaction];
            
        }
        
        NSLog(@"user Result : %@", [UserData allObjects]);
    }];
}

+ (void)getDeviceUUID:(PFUser *)user callback:(void(^)(id uuid))callback{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"uuid = %@",user.objectId];
    [UserHandler fetchUserData:pred complete:^(BOOL complete, UserData *aUser) {
        if(aUser){
            
            callback(aUser.deviceUUID);
        }
    }];
}

+ (void)getCurrentUser:(NSPredicate *)pred complete:(void (^)(UserData *mUser))success{
    
    [UserHandler fetchUserData:pred complete:^(BOOL complete, UserData *user) {
        
        if(user)
            success(user);
        else
            success(nil);
    }];
}

+ (void)removeAllUserData{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *result = [UserData allObjects];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:result];
    [realm commitWriteTransaction];
    NSLog(@"result.count : %ld", result.count);
}

+ (NSString *)returnRightMetric:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType
{
    NSString *result = nil;
    
    if ([metricType isEqualToString:@"inches"]) {
        if ([theMeasure isEqualToString:@""]) {
            return @"0";
        }
        NSArray* theConvertion = [theMeasure componentsSeparatedByCharactersInSet:
                                  [NSCharacterSet characterSetWithCharactersInString:@"’”"]];
        NSInteger value1 = [theConvertion[0] intValue];
        NSInteger value2 = [theConvertion[1] intValue];
        
        float number = ((value1 * 12) + value2) * 2.54;
        
        result = [NSString stringWithFormat:@"%.0f", round(number * 100.0) / 100.0];
        
        
    } else if ([metricType isEqualToString:@"cm"]) {
        float value = [theMeasure floatValue];
        float number = value / 2.54;
        
        if (roundf( number) >= 12.0) {
            if ((int)round( number) % 12==0) {
                result = [NSString stringWithFormat:@"%i’%i”", (int)roundf(number / 12.0), (int)round( number) % 12];
            }else{
                result = [NSString stringWithFormat:@"%i’%i”", (int)floorf(number / 12.0), (int)round( number) % 12];
            }
        } else {
            result = [NSString stringWithFormat:@"0’%i”", (int)round(number)];
        }
    }
    NSLog(@"result: %@", result);
    return result;
}

@end
