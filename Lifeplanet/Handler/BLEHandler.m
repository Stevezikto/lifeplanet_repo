//
//  BLEHandler.m
//  Arki
//
//  Created by ken on 2015. 4. 17..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "BLEHandler.h"

#import <NSDate+Calendar/NSDate+Calendar.h>

#import "Mask.h"
#import "BandData.h"
#import "UserHandler.h"
#import "ArkiBandData.h"
#import "ArkiMask.h"
#import "BandHandler.h"
#import <Parse/Parse.h>
#import "LoginAgent.h"

NSString *DEVICE_ARKI = @"Zikto-walk";
#define FAKE_UUID @"2163F9CB-74F0-B767-6F8F-47601D0CB360"

#define SEVERCE_UUID @"0000f0f0-1212-efde-1523-785feabcd123"
#define CHARACTERISTIC_WRITE_UUID @""
#define CHARACTERISTIC_NOTIFY_UUID @""

#define BYTE_RANGE 6
#define FIRST_TWO_BYTE 2
#define SECOND_FOUR_BYTE 4
#define SYNC_DELAY 5.0
#define OnePacket 18
#define OTA_BLOCK_SIZE          16

static NSUInteger totalByteLength = 0;

@implementation BLEHandler

#define AMOUNT_PER 8

+ (BLEHandler *)sharedInstance
{
    static dispatch_once_t pred;
    static BLEHandler *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[BLEHandler alloc] init];
    });
    
    return view;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        NSLog(@"");
        [LGCentralManager sharedInstance];
        _totalData = [NSMutableData new];
//        _totalData = [[NSMutableData alloc] initWithCapacity:20000];
        _resendTimer = [NSTimer new];
        
//        _deviceUUID = [NSString new];
        self.deviceUUID = nil;
        // 디바이스 uuid 세팅
//        [UserHandler getDeviceUUID:[PFUser currentUser] callback:^(id uuid) {
//            _deviceUUID = uuid;
//        }];

    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
 1.마스킹 할 위치에 해당하는 부분의 값을 이진수 1로 바꾸고 나머지는 0으로 만든다
 2.해당 위치의 실제 데이터값(original)과 마스킹 값을 마스킹한다 (&)곱하기연산
 3.마스킹 한 결과값을 shifting 한다 (실제 값을 받기 위해 민다)
 
 int value = CFSwapInt32BigToHost(*(int*)([data4 bytes]));
 int value2 = CFSwapInt32LittleToHost(*(int*)([data4 bytes]));
 unsigned int len = (uint32_t)[data length];
 uint32_t little = (uint32_t)NSSwapHostIntToLittle(len);
 */

- (void)scanBLEWithCompleteWithType:(NSInteger)BLEType complete:(void(^)(id data))complete{

    // 인스턴스 릴리즈 되지 않게 처리
    
//    if(BLEType != BLEProtocol700103){
//        [UserHandler getDeviceUUID:[PFUser currentUser] callback:^(id uuid) {
//            _deviceUUID = uuid;
//        }];
//    }    
    
    _selectedBLEType = 100000;

    [self startTimer:BLEType];
    
    // Find 아닐 경우 로딩 show
    if(BLEType != BLEProtocol700103){
        
        [UserHandler getDeviceUUID:[PFUser currentUser] callback:^(id uuid) {
            NSLog(@"device uudi : %@", uuid);

            if(uuid)
                _deviceUUID = uuid;
            else
                _deviceUUID = nil;
                
        }];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    }
    else{
        _deviceUUID = nil;
    }
    
//    NSString *uuid = FAKE_UUID;
    NSLog(@"uuid : %@ / bletype : %ld", _deviceUUID, BLEType);
    
#if TARGET_IPHONE_SIMULATOR
    if(BLETypeDisconnect == BLEType){
    }
    else if(BLETypeStart == BLEType){
    }
    else if(BLETypeGaitStart == BLEType){
        _resendTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(disconnect) userInfo:nil repeats:NO];
    }
    else if(BLETypeDailySyncStart == BLEType){
        _resendTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(disconnect) userInfo:nil repeats:NO];
    }
#endif
    
    [[LGCentralManager sharedInstance] scanForPeripheralsByInterval:4
                                                         completion:^(NSArray *peripherals)
     {
         if (peripherals.count){

             NSLog(@"device uuid : %@, length: %ld", _deviceUUID, _deviceUUID.length);
      
             if(_deviceUUID.length){
                 NSAssert(_deviceUUID != nil, @"device uuid must not be nil");
                 
                 for(LGPeripheral *p in peripherals){
                     if([p.UUIDString isEqual:_deviceUUID]){
                         NSLog(@"UUID 일치");
                         
                         [self stopTimer];
                         _tempPeripheral = p;
                         
//                         if(BLEType != BLEProtocolOTA){
                        
                             [self connectWithPeripheral:p ConnectionType:BLEType complete:^(id data) {
                                 NSLog(@"컨넥션 종료");
                                 complete(data);
                             }];
//                         }
//                         else{
//                             NSLog(@"OTA 시작");
//                             mi_CurFrame = 0;
//                             mi_TotalFrame = 0;
//                             mi_CurByte = 0;
//                             mi_PrePercent = 0;
//                             
//                             mb_Thread = false;
//                             [self performOTA:^(NSData *data) {
////                                 [self connectOTAWithPeripheral:p data:data complete:^(id data) {
////                                     
////                                 }];
//                                 
//                             }];

//                         }
                     }
                 }
             }
             else{
                 NSLog(@"No UUID");
                
                 LGPeripheral *arki;
                
                 for(LGPeripheral *p in peripherals){
                 
                     NSLog(@"name : %@ / rssi  :%ld",p.name, p.RSSI);
                     
                     if([p.name isEqual:DEVICE_ARKI]){
                     
                         NSLog(@"같은 이름의 디바이스 발견");
                         
                         arki = p;
                         _tempPeripheral = arki;
                         
                         break;
                     }
                 }
                 
                 // 발견시 타이머 종료
                 [self stopTimer];
 
                 // UUID 없을시 프로토콜 체크후 파인드 미 아니면 리턴
                 if(BLEType != BLEProtocol700103){
                     
                     [self disconnectByWrongApproach];
                     
                     return ;
                 }
                 
                 [self connectWithPeripheral:arki ConnectionType:BLEProtocol700103 complete:^(id data) {
                     NSLog(@"컨넥션 종료");
                 }];
             }
         }
     }];
}

#pragma mark - Connction
- (void)connectWithPeripheral:(LGPeripheral *)peripheral ConnectionType:(NSInteger)type complete:(void(^)(id data))complete{
    NSLog(@"peri:%@ / uuid : %@",peripheral.name, peripheral.UUIDString);
    
    [self startTimer:type];
    
    [peripheral connectWithCompletion:^(NSError *error) {
        if(error){
            [self stopBLEScan:nil];
            return ;
        }
        [peripheral discoverServicesWithCompletion:^(NSArray *services, NSError *error) {
            if(error){
                [self stopBLEScan:nil];
                return ;
            }
            for (LGService *service in services) {
                if ([service.UUIDString isEqualToString:@"0000f0f0-1212-efde-1523-785feabcd123"]) {
                    
                    [service discoverCharacteristicsWithCompletion:^(NSArray *characteristics, NSError *error) {
                        
                        // crash issue
                        LGCharacteristic *notifyCharact = characteristics[1];
                        LGCharacteristic *writeCharact = characteristics[0];
                        
                        switch (type) {
                      
                            case BLEProtocol700103: // Fine ME
                            {
                                _selectedBLEType = BLEProtocol700103;
                                
                                [self startFindMeWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(BOOL data) {
                                    
                                    if(data){
                                        NSLog(@"Find Me Callback > Time Sync");
                                        // Time Sync
                                        [self startTimeSyncWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                            if(data){
                                                // 싱크 시간 저장
                                                [[NSUserDefaults standardUserDefaults] setObject:_now forKey:@"syncDate"];
                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                NSLog(@"now : %@", _now);
                                                
                                                // UUID 전달 및 저장
                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"findEnd" object:peripheral.UUIDString];
                                                // Erase All
                                                [self startEraseAllWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(BOOL data) {
                                                    if(data){
                                                        NSLog(@"Erase All Callback > 등록 종료");
                                                        [peripheral disconnectWithCompletion:^(NSError *error) {
                                                            NSLog(@"disconnect Success");
                                                        }];
                                                    }
                                                }];
                                            }
                                        }];
                                    }
                                }];
                            }
                                break;
                            
                            case BLEProtocol700503: // register
                            {
                                _selectedBLEType = BLEProtocol700503;
                                
                                [self startRegisterWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                    NSLog(@"Register callback With score : %@", data); // 배열
                                    complete(data);
                                 
                                    // Disconnect
                                    [peripheral disconnectWithCompletion:^(NSError *error) {
                                        NSLog(@"disconnect Success");
                                    }];
                                }];
                            }
                                break;
                            
                            case BLEProtocol6003: // sync
                            {
                                _selectedBLEType = BLEProtocol6003;
                                
                                NSMutableArray *datas = [NSMutableArray new];
                                
                                [self startSyncWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                    if(data){
                                        NSLog(@"sync Success");
                                        [datas addObject:data]; // sync Total Data
                                        
                                        [SVProgressHUD dismiss]; // progoress loding dismiss
                                        
                                        [self startTimeSyncWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                            if(data){
                                                [datas addObject:data]; // sync Date
                                                
                                                [self startGetPedoWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                                    if(data){
                                                        [datas addObject:data]; // get pedo
                                                        
                                                        [self startGetSoundWalkingScoreWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                                            if(data){
                                                                [datas addObject:data]; // get soundwalking score
                                                                
                                                                [self startGetBodyBalanceScoreWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                                                    if(data){
                                                                        [datas addObject:data]; // get bodybalance score
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"bleSyncEnd" object:datas userInfo:@{@"data":_totalData}];
                                                                        
                                                                        [SVProgressHUD dismiss];
                                                                      
                                                                        // sync reset
                                                                        [self startResetWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(BOOL data) {
                                                                            if(data){
                                                                                [peripheral disconnectWithCompletion:^(NSError *error) {
                                                                                    NSLog(@"disconnect Success");
                                                                                }];
                                                                            }
                                                                            }];                                                                        
                                                                    }
                                                                }];
                                                            }
                                                        }];
                                                    }
                                                }];
                                            }
                                        }];
                                    }
                                }];
                            }
                                break;
                            case BLEProtocol700603:
                            {
                                _selectedBLEType = BLEProtocol700603;
                                
//                                NSMutableArray *datas = [NSMutableArray new];
                                
                                [self startGetPedoWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact complete:^(id data) {
                                    if(data){
//                                        [datas addObject:data];
                                        
                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"getPedo" object:data];
//                                        complete(datas);
                                    }
                                }];
                            }
                                break;
                            case BLEProtocolOTA:
                                NSLog(@"BLEProtocolOTA start");
                                
                                _selectedBLEType = BLEProtocolOTA;
                                
                                mi_CurFrame = 0;
                                mi_TotalFrame = 0;
                                mi_CurByte = 0;
                                mi_PrePercent = 0;
                                
                                mb_Thread = false;
                                
                                [self performOTA:^(NSData *data) {
                                    [self startOTAlWithNotifyCharacteristic:notifyCharact writeCharacteristic:writeCharact data:data complete:^(BOOL data) {
                                        
                                    }];
                                }];
                        }
                        // 첫번째 바이트로 타입 구분(60,61,70,71)
                    }];
                }
            }
        }];
    }];
}

#pragma mark - Find Me

- (void)startFindMeWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(BOOL data))complete{
   
    [self stopTimer];

    __block NSData *findMe700103 = [self createBLEProtocolData:BLEProtocol700103];
    __block __weak id notification;

    [notify setNotifyValue:YES completion:^(NSError *error) {

        if(!error){
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                [write writeValue:findMe700103 completion:^(NSError *error) {
                    NSLog(@"Write 시작 :%@",findMe700103);
                    [self startTimer:_selectedBLEType];
                }];
            });
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"registerPopup" object:nil];
        
        if([findMe700103 isEqualToData:data]){
            NSLog(@"Find Me Back");
            
            notification = [[NSNotificationCenter defaultCenter] addObserverForName:@"bleFindOk" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
                complete(YES);
            }];
        }
    }];
}

#pragma mark - Time Sync

- (void)startTimeSyncWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(id data))complete{
    
    [self stopTimer];
    
    __block NSData *TimeSync = [self createBLEProtocolData:BLEProtocol7002Time];
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
    
        if(!error){
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [write writeValue:TimeSync completion:^(NSError *error) {
                    NSLog(@"Write 시작 :%@",TimeSync);
                    [self startTimer:_selectedBLEType];
                }];
            });
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
//        NSData *secondByte = [data subdataWithRange:NSMakeRange(1, 1)];
        NSData *getTime = [self createBLEProtocolData:BLEProtocol700203];
        
        if([getTime isEqual:data]){
            NSLog(@"Time Sync Back");
            complete(_now);
        }
    }];
}

#pragma mark - Erase All

- (void)startEraseAllWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(BOOL data))complete{
    
    [self stopTimer];
    
    __block NSData *erase700703 = [self createBLEProtocolData:BLEProtocol700703];
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
    
        if(!error){
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [write writeValue:erase700703 completion:^(NSError *error) {
                    NSLog(@"Write 시작 :%@",erase700703);
                    [self startTimer:_selectedBLEType];
                }];
            });
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
//        NSData *secondByte = [data subdataWithRange:NSMakeRange(1, 1)];
        
        if([erase700703 isEqual:data]){
            NSLog(@"Erase All Back");
            complete(YES);
        }
    }];
}

#pragma mark - Reset

- (void)startResetWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(BOOL data))complete{
    
    [self stopTimer];
    
    __block NSData *erase700403 = [self createBLEProtocolData:BLEProtocol700403];
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
        
        if(!error){
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [write writeValue:erase700403 completion:^(NSError *error) {
                    NSLog(@"Write 시작 :%@",erase700403);
                    [self startTimer:_selectedBLEType];
                }];
            });
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        //        NSData *secondByte = [data subdataWithRange:NSMakeRange(1, 1)];
        
        if([erase700403 isEqual:data]){
            NSLog(@"Reset Success");
            complete(YES);
        }
    }];
}


#pragma mark - Register

- (void)startRegisterWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(id data))complete{
    
    [self stopTimer];
    
    
    NSData *getGait = [self createBLEProtocolData:BLEProtocolCommand]; // 70
    NSData *getGaitWaiting = [self createBLEProtocolData:BLEProtocolCommandTransfer]; // 71
    __block NSData *gait700503 = [self createBLEProtocolData:BLEProtocol700503];
    NSData *gait710503 = [self createBLEProtocolData:BLEProtocol710503];
    NSData *gait71050003 = [self createBLEProtocolData:BLEProtocol71050003];
    
//    __block BOOL isGot700503 = NO;
    _isGot700503 = NO;
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
        
        if(!error){
            double delayInSeconds = 1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
                [write writeValue:gait700503 completion:^(NSError *error) {
                    NSLog(@"Write 시작 :%@",gait700503);
                    [self startTimer:_selectedBLEType];
                }];
            });
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        // 프로토콜 체크
        NSData *result = [self decodeProtocol:data];
        NSLog(@"result : %@", result);
        
        NSData *firstByte = [result subdataWithRange:NSMakeRange(0, 1)];
        
        NSLog(@"firstByte : %@ , got : %d", firstByte, _isGot700503);
        
        if([getGait isEqualToData:firstByte]){
            NSLog(@"Get 70 gait start");

            // 700503을 제대로 받았는지 체크
            _isGot700503 = YES;
            NSLog(@"got 700503 :%d",_isGot700503);
            
            [write writeValue:gait710503 completion:^(NSError *error) {
                [self startTimer:_selectedBLEType];
            }];
        } // 70 받고 나서 진입
        else if([getGaitWaiting isEqualToData:firstByte]){

            NSLog(@"Get 71 : got 700503 : %d", _isGot700503);
            
            if(_isGot700503){
            
                if([gait71050003 isEqualToData:result]){
                    NSLog(@"측정중 .. ");
                    double delayInSeconds = 1;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        
                        [write writeValue:gait710503 completion:^(NSError *error) {
                            NSLog(@"Write 시작 :%@",gait710503);
                            [self startTimer:_selectedBLEType];
                        }];
                    });
                }
                else{
                    NSLog(@"Get Register Score");
                    
                    _isGot700503 = NO;
                    
                    [SVProgressHUD dismiss];
                    
                    ArkiMask *mask = [ArkiMask sharedInstance];
                    
                    NSData *score = [result subdataWithRange:NSMakeRange(2, 3)]; // 우선 손은 제외 손 포함하면 4
                    NSArray *scores = [mask parseBodyBalance:score length:3];
                    
                    __block NSArray *results;
                    
                    // 손 방향 체크
                    
                    NSData *handByte = [result subdataWithRange:NSMakeRange(5, 1)];
                    
                    [mask maskOneByte:handByte compelte:^(UInt32 data) {
                        
                        results = @[scores, @(data)];
                        
                        complete(results);
                    }];
                    
                    //                complete(scores);
                }
            }
            else{
                [GeneralUtil showAlert:@"Error" message:[NSString stringWithFormat:@"%@", result] cancel:@"Ok"];
                [self disconnectWithNoCallback];
            }
        }
        
    }];
}

#pragma mark - Sync

- (void)startSyncWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(id data))complete{
    
    [self stopTimer];
    
    _totalData = [NSMutableData new]; // 누적 초기화
    _syncDataLength = 0; // 프로그레스
    
    ArkiMask *mask = [ArkiMask sharedInstance];
    
    __block int endIndex = 0, temp = 0, totalLength = 0;
    __block NSData *sync6003 = [self createBLEProtocolData:BLEProtocol6003];

    NSData *sync6103 = [self createBLEProtocolData:BLEProtocol6103];
    
    NSData *syncCommand = [self createBLEProtocolData:BLEProtocolSyncDataStart]; // 60
//    NSData *syncEnd = [self createBLEProtocolData:BLEProtocolSyncDataTransfer]; // 61
//    NSData *syncError = [self createBLEProtocolData:BLEProtocolError]; // 03
    __block NSMutableData *appendedData = [NSMutableData new];
    
    [SVProgressHUD dismiss]; // 노멀 로딩 dismiss 하고 프로그레시브 로딩 변경

    [notify setNotifyValue:YES completion:^(NSError *error) {
        
        if(!error){
            [write writeValue:sync6003 completion:^(NSError *error) {
                [self startTimer:_selectedBLEType];
            }];
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        NSData *firstByte = [data subdataWithRange:NSMakeRange(0, 1)];
        
        // 60
        if([syncCommand isEqualToData:firstByte]){
            NSLog(@"get sync length");
            
            // 프로토콜 체크
            NSData *result = [self decodeProtocol:data];
            NSLog(@"length result : %@", result);
            
            NSData *length = [result subdataWithRange:NSMakeRange(1, 4)];
            
            [mask maskLengthData:length complete:^(UInt32 data) {
                NSLog(@"length : %u", data);
                
                if(data < 1){
                    NSLog(@"sync data 없음 ");
//                    "no_data" = "No data to sync."
//                     [[NSNotificationCenter defaultCenter] postNotificationName:@"syncFail" object:NSLocalizedString(@"no_data", @"No data to be synced.")];
                    complete(_totalData);
                    return;
                }
                else{
//                    NSLog(@"length 있음");
                    temp = 1.0;
                    _syncDataLength = (float)data;
                    totalLength = data;
    
                    [write writeValue:sync6103 completion:^(NSError *error) {
                        if(!error){
                            [self startTimer:_selectedBLEType];
                        }
                    }];
                }
            }];
        } // 61
//        else if([syncEnd isEqualToData:firstByte]){
        else{
            NSLog(@"get sync data");

            if([sync6103 isEqualToData:data]){
                // 종료 카운트 시작 : 총 3번이면 종료
                endIndex ++;
                
                if(endIndex >= 3){

                    NSLog(@"Sync 종료 , 데이터 : %@", _totalData);
                    
                    totalByteLength = 0; // 초기화 (누적 방지)
                    _syncDataLength = 0; // 로딩 프로그레스 방지

                    complete(_totalData);
                    return ;
                }
                [write writeValue:sync6103 completion:^(NSError *error) {
                    NSLog(@"6103 : %@", sync6103);
                    [self startTimer:_selectedBLEType];
                }];
            }
            else{
//                NSLog(@"누적누적");
                endIndex = 0;
                
                // 누적하기 전 앞 61 뒤 1byte 제거

                int position0x03 = -1;
                
                uint8_t* receive = (uint8_t *)[data bytes];
                
                for( int i = 0 ; i<data.length;i++)
                {
                    if(receive[i] == 0x03)
                    {
                        position0x03 = i;
                        NSLog(@"position : %d", position0x03);
                    }
                }
                
                if( position0x03 == -1)
                {
                    NSData *tempData = [data subdataWithRange:NSMakeRange(0, data.length)];
                    [appendedData appendData:tempData];
                    
                    NSLog(@"appendData1 : %@", appendedData);
                }
                else
                {
                    
                    NSData *tempData = [data subdataWithRange:NSMakeRange(0, position0x03)];
                    [appendedData appendData:tempData];
                    NSLog(@"tempData : %@", tempData);
                    
                    //decode
                    if(appendedData.length !=0)
                    {
                        NSData *result = [self decodeProtocol:appendedData];                        
                        NSLog(@"result2 : %@, length : %ld", result, result.length);
                        
                        //decode된 데이터의 길이 19
                        if(result.length == 19){
                            
                            //decode된 데이터를 totalData append
                            // 앞 61 제거
                            [_totalData appendData:[result subdataWithRange:NSMakeRange(1, result.length-1)]];
                            
                            //appenddata 초기화
                            appendedData = [NSMutableData new];
                        }
                        else{
                            appendedData = [NSMutableData new];
                        }
                    }
                    
                }
                
                if(_syncDataLength > 0)
                    _syncDataLength --;
                
//                [SVProgressHUD showProgress:temp - (_syncDataLength/totalLength)
//                                     status:[NSString stringWithFormat:@"%ld/%ld",(NSInteger)_syncDataLength, (NSInteger)totalLength]
//                                   maskType:SVProgressHUDMaskTypeGradient];
                [SVProgressHUD showProgress:temp - (_syncDataLength/totalLength)
                                     status:NSLocalizedString(@"syncing", @"Syncing..")
                                   maskType:SVProgressHUDMaskTypeGradient];
                
                [write writeValue:sync6103 completion:^(NSError *error) {
                    NSLog(@"6103 : %@", sync6103);
//                    [self startTimer:_selectedBLEType];
                    _resendTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(resend:) userInfo:@{@"charact" : write} repeats:NO];
                }];
            }
        }
//        else if([syncError isEqualToData:firstByte]){
//            [write writeValue:sync6103 completion:^(NSError *error) {
//                if(!error){
//                    [self startTimer:_selectedBLEType];
//                }
//            }];
//        }
    }];
}

#pragma mark - Get Pedo

- (void)startGetPedoWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(id data))complete{
    
    [self stopTimer];
    
    ArkiMask *mask = [ArkiMask sharedInstance];
    
    NSData *pedo700603 = [self createBLEProtocolData:BLEProtocol700603];
    NSData *pedoCommand = [self createBLEProtocolData:BLEProtocolCommand]; // 70

    [SVProgressHUD showWithStatus:NSLocalizedString(@"save_db", @"Saving into database..")];
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
        
        if(!error){
            [write writeValue:pedo700603 completion:^(NSError *error) {
                NSLog(@"send : %@", pedo700603);
                [self startTimer:_selectedBLEType];
            }];
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        // 프로토콜 체크
        NSData *result = [self decodeProtocol:data];
        NSLog(@"result : %@", result);
        
        NSData *firstByte = [result subdataWithRange:NSMakeRange(0, 1)];
       
        if([pedoCommand isEqualToData:firstByte]){
            
            NSData *pedoValue = [result subdataWithRange:NSMakeRange(2, 4)];
            [mask maskPedoData:pedoValue complete:^(UInt32 data) {
                NSLog(@"pedo :%u", data);
                complete(@(data));
            }];
//            [mask maskLengthData:pedoValue complete:^(UInt32 data) {
//                
////                complete([NSString stringWithFormat:@"%d", data]);
//                complete(@(data));
//            }];
        }
    }];
}

#pragma mark - Get SoundWalkingScore

- (void)startGetSoundWalkingScoreWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(id data))complete{
    
    [self stopTimer];
    
    NSData *walking700803 = [self createBLEProtocolData:BLEProtocol700803];
    NSData *walkingCommand = [self createBLEProtocolData:BLEProtocolCommand]; // 70
    
    NSMutableArray *scores = [NSMutableArray new];
    
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
        if(!error){
            [write writeValue:walking700803 completion:^(NSError *error) {
                NSLog(@"send : %@", walking700803);
                [self startTimer:_selectedBLEType];
            }];
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        // 프로토콜 체크
        NSData *result = [self decodeProtocol:data];
        NSLog(@"result : %@", result);
        
        NSData *firstByte = [result subdataWithRange:NSMakeRange(0, 1)];
        
        if([walkingCommand isEqualToData:firstByte]){
            
            NSData *score = [result subdataWithRange:NSMakeRange(2, 15)];
            __block int getScore = 0;
            
            // 0이 들어오면 종료
            for(int i=0; i< score.length; i++){
                
                NSData *oneByte = [score subdataWithRange:NSMakeRange(i, 1)];
              
                ArkiMask *mask = [ArkiMask sharedInstance];
                
                [mask maskOneByte:oneByte compelte:^(UInt32 data) {
                    
                    NSLog(@"Score : %u", data);
                    getScore = data;
                    
                }];
                
                if(getScore < 1)
                    break;
                
                NSLog(@"SoundWalkingScore 누적 : %d", getScore);

                NSArray *mScore = @[@(getScore)];
                [scores addObject:mScore];
                
//                [scores addObject:[NSString stringWithFormat:@"%u",getScore]];
            }
            complete(scores);
        }
    }];
}

#pragma mark - Get BodyBalanceScore

- (void)startGetBodyBalanceScoreWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write complete:(void (^)(id data))complete{
    
    [self stopTimer];
    
    ArkiMask *mask = [ArkiMask sharedInstance];
    NSMutableArray *scores = [NSMutableArray new];
    
    NSData *body700903 = [self createBLEProtocolData:BLEProtocol700903];
    NSData *bodyCommand = [self createBLEProtocolData:BLEProtocolCommand]; // 70
    NSData *body710903 = [self createBLEProtocolData:BLEProtocol710903];
    NSData *bodyTransfer = [self createBLEProtocolData:BLEProtocolCommandTransfer]; // 71
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
        
        if(!error){
            [write writeValue:body700903 completion:^(NSError *error) {
                NSLog(@"send : %@", body700903);
                [self startTimer:_selectedBLEType];
            }];
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        [self stopTimer];
        
        // 프로토콜 체크
        NSData *result = [self decodeProtocol:data];
        NSLog(@"result : %@", result);

        __block int lengthDay;
        NSData *firstByte = [result subdataWithRange:NSMakeRange(0, 1)];

        if([bodyCommand isEqualToData:firstByte]){
            NSLog(@"70 받음");
            // day length
            // Length 받기
            NSData *length = [result subdataWithRange:NSMakeRange(2, 1)];
            
//            NSLog(@"length : %@ / %d", length, (int)[length bytes]);
//            [mask maskOneByte:length compelte:^(UInt32 data) {
//                NSLog(@"day length : %u", data); // 3일치 펌웨어 이슈
//                 lengthDay = data - 100;
//                NSLog(@"lengthDay : %d", lengthDay);
//                
//                [write writeValue:body710903 completion:^(NSError *error) {
//                    NSLog(@"send : %@", body710903);
//                    [self startTimer:_selectedBLEType];
//                }];
//            }];
            
           _lengthBodyScore = [mask parseOneByte:length compelte:^(UInt32 data) {
                    NSLog(@"day length : %u", data); // 3일치 펌웨어 이슈
//                    lengthDay = data - 100;
               }];
            _lengthBodyScore -= 100;
            
            NSLog(@"_lengthBodyScore : %ld",(long)_lengthBodyScore);
            
            NSLog(@"lengthDay : %d", lengthDay);
            
            [write writeValue:body710903 completion:^(NSError *error) {
                NSLog(@"send : %@", body710903);
                [self startTimer:_selectedBLEType];
            }];
        }
        else if([bodyTransfer isEqualToData:firstByte]){
            if([body710903 isEqualToData:data]){
                // 종료
                complete(scores);
            }
            else{
                NSLog(@"71 받음 response : %@ , length:%ld 일치",data, _lengthBodyScore);
             
                NSData *score = [result subdataWithRange:NSMakeRange(2, 3)];
                
                [scores addObject:[mask parseBodyBalance:score length:3]];
                
                [write writeValue:body710903 completion:^(NSError *error) {
                    NSLog(@"send : %@", body710903);
                    [self startTimer:_selectedBLEType];
                }];
            }
        }
    }];
}


#pragma mark - OTA

- (void)startOTAlWithNotifyCharacteristic:(LGCharacteristic *)notify writeCharacteristic:(LGCharacteristic *)write data:(NSData *)data complete:(void (^)(BOOL data))complete{
    
    [self stopTimer];
    
    _otaData = data;
    
    __block NSData *ota5103 = [self createBLEProtocolData:BLEProtocol5103];
    __block NSData *otaStart = [self createBLEProtocolData:BLEProtocolOTA]; // 50
    
    [notify setNotifyValue:YES completion:^(NSError *error) {
        
        if(!error){
            if(error){
                NSLog(@"OTA error");
                [self stopBLEScan:nil];
                return ;
            }
            
            NSLog(@"mb_OTA_Write : %d", mb_OTA_Write);
            
            [write writeValue:_otaData completion:^(NSError *error) {
                NSLog(@"Write success");
                if(mb_OTAMode)
                {
                    mb_OTA_Write = true;
                }
                else{
                    mb_OTA_Write = false;
                }
            }];
        }
    } onUpdate:^(NSData *data, NSError *error) {
        NSLog(@"Response Data : %@",data);
        
        uint8_t *bytePtr = (uint8_t *)[data bytes];
        
        if(bytePtr[0] == 0x50){
            NSLog(@"Response OTA NEWIMG");
            
            [self Response_OTA_NewImg];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"ota" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        
        [self stopTimer];
        
        
        [write writeValue:_otaData completion:^(NSError *error) {
            NSLog(@"쓰기...");
            
            // Progress
            [SVProgressHUD showProgress:(float)mi_PrePercent / 100.0
                                 status:[NSString stringWithFormat:@"%ld%%",(NSInteger)mi_PrePercent]
                               maskType:SVProgressHUDMaskTypeGradient];
            
            if(mb_OTAMode)
            {
                mb_OTA_Write = true;
            }
            else{
                mb_OTA_Write = false;
            }
            if(![ota5103 isEqualToData:_otaData]){
                [self startTimer:BLETypeOTAStart];
            }
            else{
                NSLog(@"endData 들어옴");
            }
        }];
    }];
}


- (void)connectOTAWithPeripheral:(LGPeripheral *)peripheral data:(NSData *)sendData complete:(void(^)(id data))complete{
    NSLog(@"peri:%@ / uuid : %@",peripheral.name, peripheral.UUIDString);
    
    _otaData = sendData;
    
//    [self startTimer:BLETypeOTAStart];
    
    [peripheral connectWithCompletion:^(NSError *error) {
        if(error){
            [self stopBLEScan:nil];
            return ;
        }
        [peripheral discoverServicesWithCompletion:^(NSArray *services, NSError *error) {
            if(error){
                [self stopBLEScan:nil];
                return ;
            }
            for (LGService *service in services) {
                if ([service.UUIDString isEqualToString:@"0000f0f0-1212-efde-1523-785feabcd123"]) {
                    
                    [service discoverCharacteristicsWithCompletion:^(NSArray *characteristics, NSError *error) {
                        if(error){
                            [self stopBLEScan:nil];
                            return ;
                        }
                        NSLog(@"ota test1");
                        
                        LGCharacteristic *notifyCharact = characteristics[1];        // F2 0000F0F2-1212-EFDE-1523-785FEABCD123
                        __weak LGCharacteristic *writeCharact = characteristics[0];  // F1 0000F0F1-1212-EFDE-1523-785FEABCD123
                      
                        __block NSData *end = [self createByte:BLETypeOTAStart];
                        
                        [notifyCharact setNotifyValue:YES completion:^(NSError *error) {
                            if(error){
                                NSLog(@"OTA error");
                                [self stopBLEScan:nil];
                                return ;
                            }
                            
                            NSLog(@"mb_OTA_Write : %d", mb_OTA_Write);
                            
                            [writeCharact writeValue:_otaData completion:^(NSError *error) {
                                NSLog(@"Write success");
                                if(mb_OTAMode)
                                {
                                    mb_OTA_Write = true;
                                }
                                else{
                                    mb_OTA_Write = false;
                                }
                            }];

                        } onUpdate:^(NSData *data, NSError *error) {
                            NSLog(@"Response Data : %@",data);
                          
                            uint8_t *bytePtr = (uint8_t *)[data bytes];
                            
                            if(bytePtr[0] == 0x50){
                                NSLog(@"Response OTA NEWIMG");
                                
                                [self Response_OTA_NewImg];
                            }
                        }];
                        
                        [[NSNotificationCenter defaultCenter] addObserverForName:@"ota" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
                            
                            [self stopTimer];
                            
                            
                            [writeCharact writeValue:_otaData completion:^(NSError *error) {
                                NSLog(@"쓰기...");
                                
                                // Progress
                                [SVProgressHUD showProgress:(float)mi_PrePercent / 100.0
                                                     status:[NSString stringWithFormat:@"%ld%%",(NSInteger)mi_PrePercent]
                                                   maskType:SVProgressHUDMaskTypeGradient];
                                
                                if(mb_OTAMode)
                                {
                                    mb_OTA_Write = true;
                                }
                                else{
                                    mb_OTA_Write = false;
                                }
                                if(![end isEqualToData:_otaData]){
                                    [self startTimer:BLETypeOTAStart];
                                }
                                else{
                                    NSLog(@"endData 들어옴");
                                }
                            }];
                        }];
                    }];
                }
            }
        }];
    }];
}

#pragma mark - Method

- (NSData *)decodeProtocol:(NSData *)originalData{
    
    NSLog(@"original : %@", originalData);
    
    if(originalData.length <= 2)
    {
        return originalData;
    }
    
    NSData *convertedData;
    
    uint8_t receive_Decode[255];
    uint8_t* receive = (uint8_t *)[originalData bytes];

    int receive_DecodeIndex = 0;
    
    for(int i=0; i<originalData.length - 1; i++){
        if( receive[i] == 0xD0)
        {
            if(receive[i+1]==0xD0)
            {
                receive_Decode[receive_DecodeIndex]=0xD0;
            }
            else if(receive[i+1]==0xD3)
            {
                receive_Decode[receive_DecodeIndex]=0x03;
            }
            i++;
        }
        else
        {
            receive_Decode[receive_DecodeIndex] = receive[i];
        }
        receive_DecodeIndex++;
        
//        NSLog(@"decode : %u", receive_Decode[receive_DecodeIndex]);
    }
    //마지막 데이터 처리
//    receive_Decode[receive_DecodeIndex] = receive[originalData.length-1];
//    receive_DecodeIndex++;
//
    convertedData = [NSData dataWithBytes:receive_Decode length:receive_DecodeIndex];

    if( receive[originalData.length - 2] != 0xD0)
    {
        receive_Decode[receive_DecodeIndex] = receive[originalData.length - 1];
        receive_DecodeIndex++;
    }
    convertedData = [NSData dataWithBytes:receive_Decode length:receive_DecodeIndex];
    return convertedData;
}

- (NSData *)createBLEProtocolData:(NSInteger)type{
    int bytelength = 20;
    int emptyLength = 0;
    uint8_t bytes[bytelength];
    
    for(int i=emptyLength; i<20; i++)
    {
        bytes[i] = 0x00;
    }
    
    if(BLEProtocol700103 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x01;
        bytes[2] = 0x03;

//        emptyLength = 20;
    }
    else if(BLEProtocol700203 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x02;
        bytes[2] = 0x03;
//        emptyLength = 3;
    }
    else if(BLEProtocol700403 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x04;
        bytes[2] = 0x03;
    }
    else if(BLEProtocol700503 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x05;
        bytes[2] = 0x03;
        //        emptyLength = 3;
    }
    else if(BLEProtocol700603 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x06;
        bytes[2] = 0x03;
    }
    else if(BLEProtocol700703 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x07;
        bytes[2] = 0x03;
        
    }
    else if(BLEProtocol700803 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x08;
        bytes[2] = 0x03;
    }
    else if(BLEProtocol700903 == type){
        bytes[0] = 0x70;
        bytes[1] = 0x09;
        bytes[2] = 0x03;
    }
    else if(BLEProtocol6003 == type){
        bytes[0] = 0x60;
        bytes[1] = 0x03;
        
    }
    else if(BLEProtocol6103 == type){
        bytes[0] = 0x61;
        bytes[1] = 0x03;
        
    }
    else if(BLEProtocol5103 == type){
        bytes[0] = 0x51;
        bytes[1] = 0x03;
    }
    else if(BLEProtocol5003 == type){
        bytes[0] = 0x50;
        bytes[1] = 0x03;
    }
    else if(BLEProtocol710503 == type){
        bytes[0] = 0x71;
        bytes[1] = 0x05;
        bytes[2] = 0x03;
    }
    else if(BLEProtocol71050003 == type){
        bytes[0] = 0x71;
        bytes[1] = 0x05;
        bytes[2] = 0x00;
        bytes[3] = 0x03;
    }
    else if(BLEProtocol710903 == type){
        bytes[0] = 0x71;
        bytes[1] = 0x09;
        bytes[2] = 0x03;
    }
    else if(BLEProtocol7002Time == type){
        
        _now = [[NSDate date] toLocalTime];
        NSDate *today = [NSDate date];
        
        bytes[0] = 0x70;
        bytes[1] = 0x02;
        bytes[2] = (today.year - 2000) + 100;
        bytes[3] = (today.month) + 100;
        bytes[4] = (today.day) + 100;
        bytes[5] = (today.hour) + 100;
        bytes[6] = (today.minute) + 100;
        bytes[7] = (today.second) + 100;
        bytes[8] = 0x03;

        // Test
//        NSDate *startOfToday = [GeneralUtil startOfToday:0];
//        NSDate *yesterday = [startOfToday dateByAddingDays:-1];
//        _now = yesterday;
//
//        bytes[0] = 0x70;
//        bytes[1] = 0x02;
//        bytes[2] = (yesterday.year - 2000) + 100;
//        bytes[3] = (yesterday.month) + 100;
//        bytes[4] = (yesterday.day) + 100;
//        bytes[5] = (yesterday.hour) + 100;
//        bytes[6] = (yesterday.minute) + 100;
//        bytes[7] = (yesterday.second) + 100;
//        bytes[8] = 0x03;

    }
    else if(BLEProtocolTimeSync == type){
        bytes[0] = 0x02;
        bytelength = 1;
        
    }
    else if(BLEProtocolRegister == type){
        bytes[0] = 0x05;
        bytelength = 1;
    }
    else if(BLEProtocolOTA == type){
        bytes[0] = 0x50;
        bytelength = 1;
    }
    else if(BLEProtocolOTATransfer == type){
        bytes[0] = 0x51;
        bytelength = 1;
    }
    else if(BLEProtocolSyncDataStart == type){
        bytes[0] = 0x60;
        bytelength = 1;
    }
    else if(BLEProtocolSyncDataTransfer == type){
        bytes[0] = 0x61;
        emptyLength = 20;
        bytelength = 1;
    }
    else if(BLEProtocolCommand == type){
        bytes[0] = 0x70;
        bytelength = 1;
    }
    else if(BLEProtocolCommandTransfer == type){
        bytes[0] = 0x71;;
        bytelength = 1;
    }
    else if(BLEProtocolError == type){
        bytes[0] = 0x03;
        bytelength = 1;
    }
    NSData *data = [NSData dataWithBytes:bytes length:bytelength];
//    NSLog(@"만들어진 Data : %@", data);
    return data;
}

- (void)stopBLEScan:(NSTimer *)timer{
    
    [SVProgressHUD dismiss];
    
    [[LGCentralManager sharedInstance] stopScanForPeripherals];
    
    NSString *bleType = timer.userInfo[@"bletype"];
    
    NSLog(@"bleType : %@", bleType);
    
    if(_tempPeripheral){
        [_tempPeripheral disconnectWithCompletion:^(NSError *error) {
            NSLog(@"uuid : %@ disconnected", _tempPeripheral.UUIDString);
            
            _tempPeripheral = nil;
            [self stopTimer];
        }];
    }
    
    if([bleType isEqual:@"disconnectFind"]){
        NSLog(@"disconnectFind Me Post !!");
    }
    else if([bleType isEqual:@"disconnectGait"]){
        NSLog(@"disconnectGait Post !!");
        
    }
    else if([bleType isEqual:@"syncFail"]){
        NSLog(@"dissonncetDaily Sync !!");
    }
    else if([bleType isEqual:@"disconnectOTA"]){
        NSLog(@"disconnect OTA Sync !!");
    }
    else{
        NSLog(@"잘못된 연결");
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:bleType object:nil];
}



- (void)requestOTA:(NSNotification *)notification{
    
    NSData *data = notification.object;
    NSLog(@"data :%@",data);
}



- (NSData *)createByte:(NSInteger)type{
    
    int bytelength = 20;
    int emptyLength = 0;
    uint8_t bytes[bytelength];
    
    if(type == BLETypeStart){
      
        bytes[0] = 0x70;
        bytes[1] = 0x01;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeStartTime){
        
      
        NSDate *today = [NSDate date];
        _now = [[NSDate date] toLocalTime];
        NSLog(@"now2 : %@", _now);
        
        bytes[0] = 0x70;
        bytes[1] = 0x02;
        bytes[2] = (today.year - 2000) + 100;
        bytes[3] = (today.month) + 100;
        bytes[4] = (today.day) + 100;
        bytes[5] = (today.hour) + 100;
        bytes[6] = (today.minute) + 100;
        bytes[7] = (today.second) + 100;
        bytes[8] = 0x03;
//        NSLog(@"h2 : %ld / h3 :%ld", _now.hour, [NSDate date].hour);
        emptyLength = 9;
    }
    else if(type == BLETypeStartEnd){

        bytes[0] = 0x70;
        bytes[1] = 0x00;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    
    else if(type == BLETypeGaitStart){

        bytes[0] = 0x70;
        bytes[1] = 0x05;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeGaitStartResponse){
        // 안씀
        bytes[0] = 0x70;
        bytes[1] = 0x00;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeGaitRequest){
        
        bytes[0] = 0x71;
        bytes[1] = 0x05;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeGaitWaiting){
        
        bytes[0] = 0x71;
        bytes[1] = 0x05;
        bytes[2] = 0x00;
        bytes[3] = 0x03;
        
        emptyLength = 4;
    }
    else if(type == BLETypeDailySyncStart){
    
        bytes[0] = 0x60;
        bytes[1] = 0x03;
        
        emptyLength = 2;
    }
    else if(type == BLETypeDailySyncResponse){
    
        bytes[0] = 0x60;
        bytes[1] = 0x00;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeDailySyncEnd){
    
        bytes[0] = 0x61;
        bytes[1] = 0x03;
        
        emptyLength = 2;
    }
    else if(type == BLETypeErase){
        
        bytes[0] = 0x70;
        bytes[1] = 0x04;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeTimeResponse){
        bytes[0] = 0x70;
        bytes[1] = 0x02;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypePedo){
        bytes[0] = 0x70;
        bytes[1] = 0x06;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(type == BLETypeDailySyncStartFirst){
        bytes[0] = 0x60;
        emptyLength = 20;
    }
    else if(type == BLETypeSyncEndFirst){
        bytes[0] = 0x61;
        emptyLength = 20;
    }
    else if(type == BLETypeTimeResponse){
        bytes[0] = 0x70;
        emptyLength = 20;
    }
    else if(type == BLETypeOTAStart){

        bytes[0] = 0x51;
        bytes[1] = 0x03;
        emptyLength = 2;
    }
    else if(type == BLETypeSoundWalkingStart){
        
        bytes[0] = 0x70;
        bytes[1] = 0x08;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(BLETypeBodyBalanceStart == type){
        bytes[0] = 0x70;
        bytes[1] = 0x09;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(BLETypeBodyBalanceResponse == type){
        bytes[0] = 0x70;
        bytes[1] = 0x09;
        
        emptyLength = 20;
    }
    else if(BLETypeBodyBalanceTransfer == type){
        bytes[0] = 0x71;
        bytes[1] = 0x09;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(BLETypeBodyBalanceTransferResponse == type){
        bytes[0] = 0x71;
        bytes[1] = 0x09;
        
        emptyLength = 20;
    }
    else if(BLETypeEraseAll == type){
        bytes[0] = 0x70;
        bytes[1] = 0x07;
        bytes[2] = 0x03;
        
        emptyLength = 3;
    }
    else if(BLETypePedoCommand == type){
        bytes[0] = 0x70;
        bytes[1] = 0x06;
        emptyLength = 20;
    }
    
    // 빈 바이트 추가 (0을 만나면 뒤에 추가하게 변경해야함)
    for(int i=emptyLength; i<20; i++)
    {
        bytes[i] = 0x00;
    }
    
    NSData *data = [NSData dataWithBytes:bytes length:bytelength];
    
    return data;
}

- (void)startTimer:(NSInteger)bleType{
    NSLog(@"startTimer : %ld", bleType);
    NSString *userInfo = @"";
    switch (bleType) {
        case BLEProtocol700103:
            userInfo = @"disconnectFind";
            break;
        case BLEProtocol700503:
            userInfo = @"disconnectGait";
            break;
        case BLEProtocol6003:
            userInfo = @"syncFail";
            break;
        case BLEProtocolOTA:
            userInfo = @"disconnectOTA";
        default:
            break;
    }
//    NSLog(@"bletype : %ld / userinfo : %@", userInfo);
    [_resendTimer invalidate];
    _resendTimer = nil;
    
//    if(bleType == BLEProtocol6003)
//        _resendTimer = [NSTimer scheduledTimerWithTimeInterval:SYNC_DELAY target:self selector:@selector(resend:) userInfo:@{@"bletype": userInfo} repeats:NO];
//    else
        _resendTimer = [NSTimer scheduledTimerWithTimeInterval:SYNC_DELAY target:self selector:@selector(stopBLEScan:) userInfo:@{@"bletype": userInfo} repeats:NO];
}

- (void)stopTimer{
    NSLog(@"타이머 정지");
    [_resendTimer invalidate];
    _resendTimer = nil;
    
    _recallCount = 0;
}

- (void)resend:(NSTimer *)timer{
    // 3초가 지나도 응답이 없으면 다시 write
    NSLog(@"다시 보내야함 . info : %@, index : %d", timer.userInfo, _recallCount);
    
    _recallCount ++;
    
    if(_recallCount >= 2){
        [self stopBLEScan:nil];
        _recallCount = 0;
        return;
    }
    LGCharacteristic *writeCharact = timer.userInfo[@"charact"];
    NSData *sync6103 = [self createBLEProtocolData:BLEProtocol6103];
//    
    [writeCharact writeValue:sync6103 completion:^(NSError *error) {
        _resendTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(resend:) userInfo:@{@"charact": writeCharact, @"bletype":@"syncFail"} repeats:NO];
    }];
}

- (void)resendGait:(NSTimer *)timer{
    
    _recallCount ++;
    
    if(_recallCount >= 2){
        [self stopBLEScan:nil];
        _recallCount = 0;
        return;
    }
    LGCharacteristic *writeCharact = timer.userInfo[@"charact"];
    NSData *sync700503 = [self createBLEProtocolData:BLEProtocol700503];
    //
    [writeCharact writeValue:sync700503 completion:^(NSError *error) {
        _resendTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(resendGait:) userInfo:@{@"charact": writeCharact, @"bletype":@"disconnectGait"} repeats:NO];
    }];

}

#pragma mark - Disconnect

- (void)disconnectByWrongApproach{
 
    [SVProgressHUD dismiss];
    
    [GeneralUtil showAlert:@"Error" message:@"You must register your Zikto walk first." cancel:@"Ok"];
    
    if(_tempPeripheral){
        [_tempPeripheral disconnectWithCompletion:^(NSError *error) {
            NSLog(@"uuid : %@ disconnected", _tempPeripheral.UUIDString);
            
            _tempPeripheral = nil;
            [self stopTimer];
        }];
    }
}

- (void)disconnect{
    NSLog(@"");

    [self stopBLEScan:nil];
}

- (void)disconnectWithNoCallback{
    
    NSLog(@"");
    
    [SVProgressHUD dismiss];
    [[LGCentralManager sharedInstance] stopScanForPeripherals];
    
    if(_tempPeripheral){
        [_tempPeripheral disconnectWithCompletion:^(NSError *error) {
    
            NSLog(@"uuid : %@ disconnected", _tempPeripheral.UUIDString);
            NSLog(@"disconnect Success");
            _tempPeripheral = nil;
            [self stopTimer];
        }];
    }
}

- (void)disconnectScan:(NSInteger)type{
    
    [SVProgressHUD dismiss];
    
    [self stopTimer];
    
    if(_tempPeripheral){
        [_tempPeripheral disconnectWithCompletion:^(NSError *error) {
            NSLog(@"uuid : %@ disconnected", _tempPeripheral.UUIDString);
            _tempPeripheral = nil;
        }];
    }
    
    NSString *postMessage;
    
    switch (type) {
        case BLETypeDailySyncStart:
            postMessage = @"syncFail";
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:postMessage object:nil];
}


#pragma mark - OTA Method

- (uint8_t*)ConvertSendData:(uint8_t*)tempData andLength:(int)length
{
    // convertSendData 초기화해야함
    //    static uint8_t ffff[length];
    int iIndex = 0;
    //    int iCount = length;
    
    for(int i=0; i < length; i++)
    {
        if(tempData[i] == 0x03)
        {
            convertSendData[iIndex++] = 0xD0;
            convertSendData[iIndex++] = 0xD3;
        }
        else if(tempData[i] == 0xD0)
        {
            convertSendData[iIndex++] = 0xD0;
            convertSendData[iIndex++] = 0xD0;
        }
        else
        {
            convertSendData[iIndex++] = tempData[i];
        }
    }
    convertSendData[iIndex++] = 0x03;
    
    mi_byteIndex = iIndex;
    NSLog(@"mi_byteIndex : %d", mi_byteIndex);
    
    if(iIndex == 20)
    {
        NSLog(@"정확히20");
    }
    
    if(iIndex < 20)
    {
        // 임시적
        //        mb_OTA_LimitWrite = false;
        for(int j=iIndex; j < 20; j++)
        {
            convertSendData[iIndex++] = 0x00;
        }
    }
    else if( iIndex >= 20 )
    {
        mb_OTA_LimitWrite = true;
        for(int j=iIndex; j < 40; j++)
        {
            convertSendData[iIndex++] = 0x00;
        }
    }
    
    return convertSendData;
}

- (void)performOTA:(void (^)(NSData *data))complete{
    
    NSLog(@"");
    mi_CurFrame = 0;
    mi_TotalFrame = 0;
    mi_CurByte = 0;
    mi_PrePercent = 0;
    
    mb_Thread = false;
    __block NSData *binaryData;
    
    [LoginAgent sendAnsynchronousRequestForMethodGet:OTA parameters:nil success:^(id responseObject) {
        binaryData = (NSData *)responseObject;
        [self StartOTA:binaryData complete:^(NSData *data) {
            complete(data);
        }];
    } failure:^(NSError *error) {
        
    }];
    
//    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"Arki_mcu_OTA_TEST_LINK" ofType:@"BIN"];
//    NSData *myData = [NSData dataWithContentsOfFile:filepath];
//    
//    [self StartOTA:myData complete:^(NSData *data) {
//        complete(data);
//    }];
}

- (void)StartOTA:(NSData*)filedata complete:(void (^)(NSData *data))complete
{
    if([filedata length] != 0 && filedata)
    {
        NSLog(@"fileData");
        uint8_t *ota_data = (uint8_t *)[filedata bytes];
        NSLog(@"%u.%u.%u", ota_data[5], ota_data[6], ota_data[7]);
        mb_OTAMode = true;
        
        //        [self StartOTA:bytePtr];
        for(int i=0; i < 3; i++)
            m_FW_NewVersion[i] = ota_data[i+5];
        NSLog(@"%x, %x, %x, %x", ota_data[8], ota_data[9], ota_data[10], ota_data[11] );
        mi_NewImgSize = ((ota_data[8] & 0xFF) << 24) | ((ota_data[9] & 0xFF) << 16) | ((ota_data[10] & 0xFF) << 8) | ((ota_data[11] & 0xFF));
        mi_NewImgType = ((ota_data[12] & 0xFF) << 8) | (ota_data[13] & 0xFF);
        mi_NewImg_AppType = ((ota_data[14] & 0xFF) << 8) | (ota_data[15] & 0xFF);
        
        mFileBuffer = ota_data;
        subData = filedata;
        
        NSLog(@"이미지사이즈 : %u, 이미지타입 : %u, 이미지앱타입 : %u", mi_NewImgSize, mi_NewImgType, mi_NewImg_AppType);
        
        mi_CurFrame = 0;
        mi_Sequence = 1;
        _OTA_Thread = NULL;
        
        //        [self Request_OTA_CurImg];
        [self Request_OTA_CurImg:^(NSData *data) {
            complete(data);
        }];
    }
}

- (void)Request_OTA_CurImg:(void (^)(NSData *data))complete
{
    int byteLength = 20;
    
    uint8_t bOta[byteLength];
    bOta[0] = 0x50;
    bOta[1] = 0x03;
    
    for(int i=2; i < byteLength; i++)
    {
        bOta[i] = 0x00;
    }
    
    NSData *sendData = [NSData dataWithBytes:bOta length:byteLength];
    
    complete(sendData);
  
    //    [self.vsBleAdapter writeValueNoResponse:self.vsCommonVar.VS_CMD_Characteristic
    //                                          p:self.vsBleAdapter.activePeripheral
    //                                       data:sendData];
    
}

- (void) Response_OTA_NewImg{
    
    mb_Thread = true;
    
    if(_OTA_Thread == NULL)
    {
        NSLog(@"OTA_Thread start");
        mb_OTA_Success = false;
        if( (mi_NewImgSize % OTA_BLOCK_SIZE) == 0)
        {
            mi_TotalFrame = mi_NewImgSize / OTA_BLOCK_SIZE;
        }
        else
        {
            mi_TotalFrame = (mi_NewImgSize / OTA_BLOCK_SIZE) +1;
        }
        
        NSLog(@"TotalFrame : %d", mi_TotalFrame);
        
        _OTA_Thread = [[NSThread alloc] initWithTarget:self selector:@selector(OTA_DataStart) object:nil];
        [_OTA_Thread start];
    }
    else
        [_OTA_Thread start];
}

- (void)OTA_DataStart
{
    NSLog(@"mb_Thread : %d / mb_OTA_Success : %d / mb_OTA_Write : %d / mb_OTA_LimitWrite : %d",mb_Thread, mb_OTA_Success, mb_OTA_Write, mb_OTA_LimitWrite);
    while (mb_Thread) {
        if(mb_OTA_Success)
        {
            mb_Thread = false;
            mb_OTAMode = false;
            
            [self Request_OTA_End];
            break;
        }
        else
        {
            if(mb_OTA_Write)
            {
                mb_OTA_Write = false;
                if(mb_OTA_LimitWrite)
                {
                    [self Request_OTA_LimitData];
                }
                else
                {
                    [self OTADataWrite];
                }
            }
            else
            {
                if(mi_Sequence == 1)
                {
                    mb_OTA_Write = false;
                    [self OTADataWrite];
                }
            }
        }
    }
    NSLog(@"mb_Thread : %d / mb_OTA_Success : %d / mb_OTA_Write : %d / mb_OTA_LimitWrite : %d",mb_Thread, mb_OTA_Success, mb_OTA_Write, mb_OTA_LimitWrite);
}

- (void)Request_OTA_DATA:(NSData*)data
{
    NSLog(@"");
    long limitLength = [data length] + 2;
    NSData *tempData;
    uint8_t *asdff = [self ConvertSendData:(uint8_t*)[data bytes] andLength:(int)[data length]];
    
    if(mb_OTA_LimitWrite)
    {
        limitLength = 40;
    }
    else
    {
        limitLength = 20;
    }
    
    uint8_t uOtadata[limitLength];
    uOtadata[0] = 0x51;                     // ota NewImage
    for(int i=0; i< mi_byteIndex; i++)
        uOtadata[i+1] = asdff[i];
    
    for( int i = mi_byteIndex; i < limitLength-1; i++)
    {
        uOtadata[i+1] = 0x00;
    }
    
    //    uOtadata[mi_byteIndex+1] = 0x03;
    
    if(limitLength == 20)
    {
     
        //        [self.vsBleAdapter writeValueNoResponse:self.vsCommonVar.VS_CMD_Characteristic
        //                                              p:self.vsBleAdapter.activePeripheral
        //                                           data:[NSData dataWithBytes:uOtadata length:20]];
        
        _otaData = [NSData dataWithBytes:uOtadata length:20];
     
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ota" object:nil];
    }
    else if(limitLength == 40)
    {
        //        NSLog(@"%@", [NSData dataWithBytes:uOtadata length:limitLength] );
        
        for(int i=0; i < 20; i++)
        {
            tempData01[i] = uOtadata[i];
            tempData02[i] = uOtadata[i+20];
            
            //            NSLog(@"데이터2 : %@", [NSData dataWithBytes:tempData02 length:20]);
        }
        
        if(tempData02[0] == 0x03)
        {
            NSLog(@"OTA 중지해야함");
        }
        
        //        [self.vsBleAdapter writeValueNoResponse:self.vsCommonVar.VS_CMD_Characteristic
        //                                              p:self.vsBleAdapter.activePeripheral
        //                                           data:[NSData dataWithBytes:tempData01 length:20]];
        //        NSLog(@"데이터1 : %@", [NSData dataWithBytes:tempData01 length:20]);
        tempData = [NSData dataWithBytes:tempData01 length:20];
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"reqeustOTA" object:tempData];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"reqeustOTA" object:nil userInfo:@{@"data": tempData, @"ota":@(mb_OTAMode)}];
        _otaData = tempData;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ota" object:nil];
    }
    
    
    
}

-(void) Request_OTA_LimitData
{
    NSLog(@"");
    mb_OTA_LimitWrite = false;
    //    [self.vsBleAdapter writeValueNoResponse:self.vsCommonVar.VS_CMD_Characteristic
    //                                          p:self.vsBleAdapter.activePeripheral
    //                                       data:[NSData dataWithBytes:tempData02 length:20]];
    //    NSLog(@"데이터2 : %@", [NSData dataWithBytes:tempData02 length:20]);
    
    NSData *tempData = [NSData dataWithBytes:tempData02 length:20];
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"reqeustOTA" object:tempData];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"reqeustOTA" object:nil userInfo:@{@"data": tempData, @"ota":@(mb_OTAMode)}];
    _otaData = tempData;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ota" object:nil];
}


-(void) Request_OTA_End
{
    int byteLength = 20;
    
    uint8_t bOta[byteLength];
    bOta[0] = 0x51;
    bOta[1] = 0x03;
    
    for(int i=2; i < byteLength; i++)
    {
        bOta[i] = 0x00;
    }
    
    NSData *sendData = [NSData dataWithBytes:bOta length:byteLength];
    
    _otaData = sendData;

    [SVProgressHUD dismiss];
    
    [self stopTimer];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"ota" object:nil];
    
    NSLog(@"OTA_End데이터 : %@", sendData);
}

-(void) OTADataWrite
{
    if(mi_CurFrame < mi_TotalFrame)
    {
        mi_CurFrame++;
        mi_Sequence++;
        mi_CurByte = (mi_CurFrame - 1) * OTA_BLOCK_SIZE;
        
        NSLog(@"-----------------------------------------");
        NSLog(@"CurByte : %d", mi_CurByte);
        
        NSData *destination;// = [subData subdataWithRange:NSMakeRange(mi_CurByte, OTA_BLOCK_SIZE)];
        
        if( (mi_NewImgSize - mi_CurByte) < OTA_BLOCK_SIZE )
        {
            destination = [subData subdataWithRange:NSMakeRange(mi_CurByte, (mi_NewImgSize - mi_CurByte))];
        }
        else
            destination = [subData subdataWithRange:NSMakeRange(mi_CurByte, OTA_BLOCK_SIZE)];
        
        //        NSLog(@"destination : %@", destination);
        [self Request_OTA_DATA:destination];
        
        mi_PrePercent = (mi_CurFrame*100)/(mi_NewImgSize / OTA_BLOCK_SIZE);

        NSLog(@"남은퍼센트 : %d", mi_PrePercent);
        NSLog(@"-----------------------------------------");
    }
    else
    {
        if(mi_CurFrame == mi_TotalFrame)
        {
            mb_OTA_Success = true;
        }
    }
}

@end
