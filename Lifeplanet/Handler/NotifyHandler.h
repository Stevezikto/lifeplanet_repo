//
//  NotifyHandler.h
//  Arki
//
//  Created by ken on 2015. 6. 24..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotifyHandler : NSObject

+ (void)setZiktoDayNotification:(NSDate *)ziktoDay;

@end
