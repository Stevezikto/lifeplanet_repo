//
//  SyncHandler.m
//  Arki
//
//  Created by ken on 2015. 6. 23..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "SyncHandler.h"
#import <Realm/realm/Realm.h>
#import "BLEHandler.h"
#import "BandHandler.h"
#import "DayBandHandler.h"
#import "WalkingScoreHandler.h"
#import "ArkiMask.h"

#define MAX_RETRY_COUNT 3

@implementation SyncHandler

#pragma mark - Init

+ (SyncHandler *)sharedInstance
{
    static dispatch_once_t pred;
    static SyncHandler *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[SyncHandler alloc] init];
    });
    
    return view;
}

- (id)init{
    
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startSync) name:@"startBleSync" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bleSyncEnd:) name:@"bleSyncEnd" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrySyncOrFail:) name:@"syncFail" object:nil];
    }
    return self;
}

#pragma mark - Sync

- (void)startSync{
    
    BLEHandler *ble = [BLEHandler sharedInstance];
    [ble scanBLEWithCompleteWithType:BLEProtocol6003 complete:^(id data) {
        
        NSLog(@"%@ / pedo : %ld", data, _pedoCountForToday);
    
        if(data){
            
        }
    }];
}

- (void)retrySyncOrFail:(NSNotification *)note{
    
    NSLog(@"BLE 실패. Sync 초기화 상태 유지 count: %ld", _resyncCount);
    
    if(note.object){ // length == 0
        [SVProgressHUD dismiss];
        
        [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:note.object cancel:NSLocalizedString(@"ok", @"Ok")];
    }
    else{ // retry over 3times
        
        _resyncCount ++;
        
        if(_resyncCount > MAX_RETRY_COUNT){
            
            [SVProgressHUD dismiss];
            
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_find_zikto", @"Can’t find Zikto walk.") cancel:NSLocalizedString(@"ok", @"Ok")];
            
            _resyncCount = 0;
            
        }
        else{
            [self startSync];
        }
    }
}

- (void)bleSyncEnd:(NSNotification *)note{
    
    NSLog(@"Note.object : %@", note.object);
    NSArray * getData = note.object;
    
    _syncedData = nil;
    _syncedData = getData[0]; // 누적된 값
    _lastSyncedDate = getData[1]; // 싱크 시간
    
    // parse에 raw 데이터 전송
    BandHandler *band = [BandHandler sharedInstance];
    [band sendRawData:_syncedData];
    
    NSLog(@"lastsynctime : %@", _lastSyncedDate);
    
    _pedoCountForToday = [getData[2] integerValue];
    
    NSLog(@"mPedo : %ld", _pedoCountForToday);
    
    [self saveSoundWalkingScore:getData[3]];
    
    [self saveBalanceScore:getData[4]];
    
    [self savePedoCount:_pedoCountForToday];
    
    [self parsingWithData:^(BOOL isFinished) {

        if(isFinished){
            
            [SVProgressHUD dismiss];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadView" object:nil];
        }
    }];
    
    // Date test
    int temptLength = 3;
    for (int i=0; i<temptLength; i++){
        NSInteger dateIndexFromToday = (i+1) - temptLength;
        NSDate *today = [GeneralUtil startOfToday:dateIndexFromToday];
        NSLog(@"i : %d , Date start index : %ld / date : %@",i,dateIndexFromToday, today);
    }
    
}

#pragma mark - Save

- (void)parsingWithData:(void (^) (BOOL isFinished))complete{
    
    // 마스킹 된 값 디비에 저장
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    NSDate *syncDate;
   
    syncDate =  [[NSUserDefaults standardUserDefaults] objectForKey:@"syncDate"]; // Sync 끝나고 받은 시간
   
    if(!syncDate){ // Sync 시간이 없으면 현재 시간으로 새로 적용
        syncDate = [[NSDate date] toLocalTime];
    }
    
    NSLog(@"syncDate : %@ , newSyncDate : %@", syncDate, _lastSyncedDate);
    
    __block NSInteger index = 0; // 1이었지만 0으로 변경
    BandHandler *band = [BandHandler sharedInstance] ;
    
    //    NSLog(@"bleLength : %ld",_bleData.length);
    
    
    
    int aaa = 0;
    
    for(int i=0; i<300; i++){
        NSDate *newDate = [syncDate dateByAddingMinute:aaa];
        NSDate *newDate2 = [newDate dateBySettingSecond:0];
        NSLog(@"newDate : %@ / 2 : %@", newDate, newDate2);
        aaa ++;
    }
    
    
    NSDate *now = [[NSDate date] toLocalTime];
    
    if(_syncedData.length >= 6){
      
        for(int k=0; k< _syncedData.length-6; k=k+6){ // 윈도우사이즈 6 : 부분을 잘라서 보는 한 단위
  
            NSData *sixBytes;
            
            sixBytes = [_syncedData subdataWithRange:NSMakeRange(k, 6)];
            
            ArkiMask *mask = [ArkiMask sharedInstance];
            
            [mask maskWithData:sixBytes complete:^(id data) {
                
//                if([[syncDate dateByAddingMinutes:index] isEarlierThanOrEqualDate:now]){
                if([[syncDate dateByAddingMinutes:index] isEarlierThanOrEqualDate:_lastSyncedDate]){
                
                    //                    NSLog(@"현재 시간보다 데이타 시간이 이전임. 누적 가능");
                    
                    int activityNum = [data[2] intValue]; // 액티비티 넘버
                    
                    NSDate *newDate = [syncDate dateByAddingMinute:index];
                    NSDate *newDateWithZeroSeconds = [newDate dateBySettingSecond:0];
                    ArkiBandData *bandData = [band parseData:data type:activityNum date:newDateWithZeroSeconds];
                    //                NSLog(@"band.Date : %@", bandData.date);
                    [realm addObject:bandData];
                    index ++;
                }
                else{
                    //                    NSLog(@"현재 시간 이후 시간이라 제거");
                    return;
                }
            }];
        }
    }
    
    [realm commitWriteTransaction];
    
    NSLog(@"_pedoCountForToday : %ld", _pedoCountForToday);
    
    // 페도 카운트 받아서 첫번째 데이터에 저장
    [band updateProperty:@{@"pedo": [NSString stringWithFormat:@"%ld",_pedoCountForToday]}];
    
    // 싱크한 시간 저장
    
    [[NSUserDefaults standardUserDefaults] setObject:_lastSyncedDate forKey:@"syncDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // 하루치 디비에 저장
    
    //    [DayBandHandler removeAllData];
    
    NSArray * dataForDay = [band getDataForDay:[NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@",[GeneralUtil startOfToday:0], [GeneralUtil endOfToday:0]]];
    
    NSMutableArray *aDay = [NSMutableArray new];
    
        for(NSArray * data in dataForDay){
            
            [aDay addObject: @[data[1], data[0]]];
            
            NSLog(@"aDay : %@", aDay);
        }
    
    [DayBandHandler updateData:aDay];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadView" object:nil];
    
    complete(YES);
}

- (void)saveSoundWalkingScore:(NSArray *)scores{
    
    WalkingScoreHandler *walking = [WalkingScoreHandler sharedInstance];
    
    //    // SoundScore 저장
    NSArray *soundScores = scores;
    NSLog(@"souncscore : %@", soundScores);
    
    for(int i=0; i<soundScores.count; i++){
        
        NSLog(@"i : %d", i);
        
        NSInteger dateIndexFromToday = (i+1) - soundScores.count;
        
        NSDate *today = [GeneralUtil startOfToday:dateIndexFromToday];
        
        NSArray *score = soundScores[i];
        NSNumber *scoreValue = score[0];
        
        NSLog(@"Date start index : %ld / date : %@",dateIndexFromToday, today);
        
        [walking addSoundWalkingDataToDB:[scoreValue intValue] withDate:today];
    }
}

- (void)saveBalanceScore:(NSArray *)scores{
    
    WalkingScoreHandler *walking = [WalkingScoreHandler sharedInstance];
    
    // BalanceScore 저장
    NSArray *balanceScores = scores;
    
    NSLog(@"balancescore : %@", balanceScores);
    
    for(int i=0; i<balanceScores.count; i++){
        
        NSLog(@"i : %d", i);
        
        NSInteger dateIndexFromToday = (i+1) - balanceScores.count;
        
        NSDate *today = [GeneralUtil startOfToday:dateIndexFromToday];
        
        NSArray *scores = balanceScores[i];
        
        [walking addBalanceDataToDB:scores withDate:today];
    }
}

- (void)savePedoCount:(NSInteger)score{
    
    WalkingScoreHandler *walking = [WalkingScoreHandler sharedInstance];
    
    // PedoCount 저장
   
    NSLog(@"pedoCount : %ld", score);
    
    NSDate *today = [GeneralUtil startOfToday:0];
    
    [walking addPedoDataToDB:(int)score date:today];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
