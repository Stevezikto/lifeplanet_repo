//
//  SignHandler.h
//  Arki
//
//  Created by ken on 2015. 2. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface SignHandler : NSObject

+ (PFUser *)currentUser;
+ (BOOL)signedUser;
+ (void)signUpWithFacebook:(void (^)(int successType))complete;
+ (void)signUpWithEmail:(NSDictionary *)parameters complete:(void (^)(bool success))complete;
+ (void)logIn:(NSDictionary *)parameters complete:(void (^)(bool success))complete;

@end
