//
//  SignHandler.m
//  Arki
//
//  Created by ken on 2015. 2. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "SignHandler.h"
#import "UserDataModel.h"
#import "UserData.h"
#import "UserHandler.h"
#import <Realm/Realm/Realm.h>

@implementation SignHandler

+ (PFUser *)currentUser{

    return [PFUser currentUser];
}

+ (BOOL)signedUser{
    
    return [UserDataModel checkSignedUser];
}

+ (void)signUpWithFacebook:(void (^)(int successType))complete{
   
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    [PFFacebookUtils logInWithPermissions:@[@"public_profile",@"user_birthday",@"email"] block:^(PFUser *user, NSError *error) {
        NSLog(@"user:%@",user);
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        }
        else if (user.isNew) {
            NSLog(@"User signed up and logged in through Facebook!");
            [self getUserInfo:user complete:^(bool finished) {
                if(finished)
                    complete(FaceBookLoginTypeSignUp);
            }];
        }
        else {
            NSLog(@"User logged in through Facebook!");
            complete(FaceBookLoginTypeSignIn);
//            [self getUserInfo:user complete:^(bool finished) {
//                NSLog(@"페이스북 유저 정보 받아옴 : %d", finished);
//                if(finished)
//                    complete(FaceBookLoginTypeLogin);
//            }];
        }
        
//        [SVProgressHUD dismiss];
    }];
}

// parse 에만 가입 되어있고 facebook과 링크 되어 있지 않은 사용자인지 체크
+ (void)linkFacebookUserToParse:(PFUser *)user complete:(void (^)(bool complete))complete{
    
    if (![PFFacebookUtils isLinkedWithUser:user]) {
        [PFFacebookUtils linkUser:user permissions:nil block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Woohoo, user logged in with Facebook!");
            }
            else{
                NSLog(@"failed linking");
            }
        }];
    }
    else{
        NSLog(@"already linked user");
    }
    complete(YES);
}

+ (void)getUserInfo:(PFUser *)user complete:(void (^)(bool finished))finished
{
    NSString *params = @"id, name, email, picture.type(large),gender";
    
    [FBRequestConnection startWithGraphPath:@"me" parameters: @{@"fields" : params} HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result,NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSLog(@"result : %@", result);
            NSLog(@"user : %@", user);
           
            user[@"name"] = result[@"name"];
           
//            NSString *gender = result[@"gender"];
//            user[@"gender"] = [gender capitalizedString];
            int gender = 1;
            if([result[@"gender"] isEqual:@"female"])
                gender = 2;
            
            user[@"gender"] = @(gender);
            user[@"arkiday"] = @"Sunday";
//            user[@"age"] = [NSString stringWithFormat:@"%ld",(long)[GeneralUtil calcurateUserBirth:result[@"birthday"]]];
//            user[@"age"] = result[@"birthday"];
            
            NSString *email = result[@"email"];
            if(![email isEqualToString:@""] && email != nil)
                user[@"email"] = email;            
          
            NSURL *imageUrl = [NSURL URLWithString: [[result[@"picture"] objectForKey:@"data"] objectForKey:@"url"]];
            NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
            NSString *imageName = [NSString stringWithFormat:@"%@.png",user.objectId];
            PFFile *imageFile = [PFFile fileWithName:imageName data:imageData];
          
            PFObject *imageUpload = [PFObject objectWithClassName:@"Upload"];
            imageUpload[@"imageFile"] = imageFile;
            imageUpload[@"userId"] = user;
            
            [imageUpload saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    // The object has been saved.
                    UIImage *image = [UIImage imageWithData:imageData];
                    [GeneralUtil saveFile:image fileName:@"image.png" complete:^(BOOL complete) {
                        NSLog(@"페이스북 이미지 로컬에 저장 완료 및 사용자 정보 로컬에 저장");
                        [UserHandler createNewUser:user complete:^(BOOL complete) {
                            NSLog(@"유저 생성");
                        }];
                    }];
                    finished(succeeded);
                } else {
                    // There was a problem, check error.description
                }
            }];
            
            // parse에 저장된 url로 프로필 이미지 다운
//            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:imageUrl
//                                                                options:0
//                                                               progress:^(NSInteger receivedSize, NSInteger expectedSize)
//             {
//                 // progression tracking code
//             }
//                                                              completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
//             {
//                 if (image && finished)
//                 {
//                     [GeneralUtil saveFile:image fileName:@"image.png" complete:^(BOOL complete) {
//                         NSLog(@"페이스북 이미지 로컬에 저장 완료 및 사용자 정보 로컬에 저장");
//                         [UserHandler createNewUser:user complete:^(BOOL complete) {
//                             NSLog(@"유저 생성");
//                         }];
//                     }];
//                 }
//             }];

            // parse 저장
//            [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                if(succeeded){
////                    user[@"imageUrl"] = imageFile.url;
////                    [[PFUser currentUser] saveInBackground];
//                    
//                    PFObject *imageUpload = [PFObject objectWithClassName:@"Upload"];
//                    imageUpload[@"imageFile"] = [NSString stringWithFormat:@"%@.png", user.objectId];
//                    imageUpload[@"userId"] = user;
//
//                    [imageUpload saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                        if (succeeded) {
//                            // The object has been saved.
//                            finished(succeeded);
//                        } else {
//                            // There was a problem, check error.description
//                        }
//                    }];
//                }
//            } progressBlock:^(int percentDone) {
//                NSLog(@"percent  :%d", percentDone);
//            }];
        }
    }];
}
+ (void)signUpWithEmail:(NSDictionary *)parameters complete:(void (^)(bool))complete{
    
    // parse 연동
    PFUser *user = [PFUser user];
    
    // new sign up
    user.username = parameters[@"id"];
    user.password = parameters[@"password"];
    
    //    PFACL *defaultACL = [PFACL ACL];
    //    [defaultACL setPublicReadAccess:YES];
    //    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    
    // 당사자만 접근 허용
    //    [PFACL setDefaultACL:[PFACL ACL] withAccessForCurrentUser:YES];
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hooray! Let them use the app now.
            NSLog(@"sign succeed");
            [UserHandler createNewUser:user complete:^(BOOL complete) {
                NSLog(@"유저 생성");
            }];
            complete(succeeded);
        } else {
//            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"error : %@", [error userInfo][@"error"]);
            complete(NO);
        }
    }];
}

+ (void)logIn:(NSDictionary *)parameters complete:(void (^)(bool))complete{
    
    NSString *userId = parameters[@"id"];
    NSString *password = parameters[@"password"];
    
    [PFUser logInWithUsernameInBackground:userId password:password
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            // Do stuff after successful login.
                                            NSLog(@"Do stuff after successful login.");
                                            complete(YES);
                                        } else {
                                            // The login failed. Check error to see why.
                                            NSLog(@"login failed");
                                            complete(NO);
                                        }
                                    }];
}

@end
