//
//  BLEHandler.h
//  Arki
//
//  Created by ken on 2015. 4. 17..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LGBluetooth/LGBluetooth.h>

@interface BLEHandler : NSObject{
    // OTA관련
    
    uint8_t convertSendData[20];
    uint8_t tempData01[20], tempData02[20];
    
    int m_FW_NewVersion[3];
    int m_FW_CurVersion[3];
    //    uint8_t mFileBuffer[FILE_BUFFER_SIZE];
    uint8_t *mFileBuffer;
    NSData *subData;
    
    int mi_NewImgSize;
    int mi_NewImgType;
    int mi_NewImg_AppType;
    int mi_CurImg_AppType;
    
    int mi_CurImgType;
    int mi_CurImgSize;
    
    int mi_Sequence;
    int mi_CurFrame;
    int mi_TotalFrame;
    int mi_CurByte;
    int mi_PrePercent;
    
    bool mb_Thread;
    bool mb_OTA_Success;
    
    bool mb_OTA_Write;				// OTA에서 현재 패킷을 보냈으니 다음 패킷 보내기 위한 변수
    bool mb_OTA_LimitWrite;
    
    bool mb_OTAMode;
    long ml_limitLength;
    
    int mi_byteIndex;
}

+ (BLEHandler *)sharedInstance;
//- (void)scanBLEWithComplete:(void(^)(NSArray *data))success;
- (void)scanBLEWithCompleteWithType:(NSInteger)BLEType complete:(void(^)(id data))complete;
- (void)stopBLEScan:(NSTimer *)timer;
- (void)disconnectWithNoCallback; // 성공시에 외부 클래스에서 디스컨넥트

@property (nonatomic, strong)NSMutableData *totalData;
@property (nonatomic, strong)NSMutableArray *total;
@property NSMutableData *testData;
@property NSTimer *resendTimer;
@property (strong, nonatomic) NSString *deviceUUID;
@property (strong, nonatomic) NSDate *now;
@property (strong, nonatomic) LGPeripheral *tempPeripheral;
@property (assign)float syncDataLength;
@property (assign)int recallCount;
@property(strong, nonatomic) NSThread *OTA_Thread;
@property(strong, nonatomic)NSData *otaData;
@property NSInteger selectedBLEType, lengthBodyScore;
@property (nonatomic)BOOL isGot700503;
@end
