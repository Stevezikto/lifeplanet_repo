//
//  BandHandler.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 11..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "BandHandler.h"
#import "ArkiBandData.h"
#import "WalkingScoreData.h"
#import <Parse/Parse.h>

#define AmountPerHour = 60;
#define DATA_AMOUNT_PER_DAY 1440
#define OTA_BLOCK_SIZE          16
#define RoundOff(x, dig) (floor((x) * pow(10,dig) + 0.5) / pow(10,dig))
#define SLEEP_THRESHOLD 3
#define MAX_STEP_PER_DAY 20000

@implementation BandHandler

+ (BandHandler *)sharedInstance
{
    static dispatch_once_t pred;
    static BandHandler *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[BandHandler alloc] init];
    });
    
    return view;
}

- (id)init{
    
    self = [super init];
    if (self)
    {
        _realm = [RLMRealm defaultRealm];
    }
    return self;
}

#pragma mark - UUID

- (void)saveDeviceUUID:(NSString *)uuid{

    NSLog(@"UUID 저장");
    [[NSUserDefaults standardUserDefaults] setObject:uuid forKey:@"device_uuid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)loadDeviceUUID{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"device_uuid"];
}

- (void)saveDeviceScore:(NSData *)score{
    [[NSUserDefaults standardUserDefaults] setObject:score forKey:@"device_score"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -

- (NSData *)loadDeviceScore{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"device_score"];
}

#pragma mark - Update 

- (void)updateProperty:(NSDictionary *)property{
    
    NSLog(@"property : %@", property);
    
    // 밴드 데이터 없을 때 예외 처리 추가
    NSDate *date = [NSDate date];
//    NSDate *today = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
    NSDate *today = [GeneralUtil startOfToday:0];
    NSDate *tomorrow = [today dateByAddingDays:1];

    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", today, tomorrow];
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    
    NSLog(@"resutl.count :%ld",result.count);
    NSString * key = [property allKeys][0];
    int value = [property[key] intValue];
    
    if(result.count){
      
        ArkiBandData *firstBand = result[0];
  
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        [realm beginWriteTransaction];
        
        ArkiBandData *newBand = [ArkiBandData new];
        newBand.index = firstBand.index;
        newBand.date = firstBand.date;
        newBand.totalPedoCount = value;
        newBand.uid = firstBand.uid;
        newBand.leftRight = firstBand.leftRight;
        newBand.authen = firstBand.authen;
        newBand.Activity = firstBand.Activity;
        newBand.pedoCount = firstBand.pedoCount;
        newBand.walkNum = firstBand.walkNum;
        newBand.accelMag = firstBand.accelMag;
        newBand.diffCounter = firstBand.diffCounter;
        
        [ArkiBandData createOrUpdateInRealm:realm withValue:newBand];
        [realm commitWriteTransaction];
        NSLog(@"update New Band : %@", newBand);
    }
    else{
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        [realm beginWriteTransaction];

        NSDate *lastSyncDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"syncDate"];
        NSLog(@"lastSyncDate : %@", lastSyncDate);
        
        ArkiBandData *newBand = [ArkiBandData new];
        newBand.index = 0;
        newBand.date = lastSyncDate;
        newBand.totalPedoCount = value;
        newBand.uid = @"0";
        newBand.leftRight = 0;
        newBand.authen = 0;
        newBand.Activity = 7;
        newBand.pedoCount = 0;
        newBand.walkNum = 0;
        newBand.accelMag = 0;
        newBand.diffCounter = 0;
        
        [ArkiBandData createOrUpdateInRealm:realm withValue:newBand];
        [realm commitWriteTransaction];
       
        NSLog(@"create New Band : %@", newBand);
    }
}

#pragma mark -
- (void)updateUid{
    
    RLMResults *result = [ArkiBandData allObjects];
    
    for(ArkiBandData *band in result){
//        band.uid = [NSString stringWithFormat:@"%ld",band.index];
        NSLog(@"New Band.uid : %@", band.uid);
    }
}

#pragma mark -

- (id)returnHourData:(NSArray *)originalData range:(int)range{
    
    NSMutableArray *hourTempData= [NSMutableArray new];
    NSMutableArray *hourData = [NSMutableArray new];
    
    int dataCount = (int)originalData.count;

    for(NSArray *data in originalData){
        
        [hourTempData addObject:data];
        
        if(hourTempData.count == range || hourTempData.count == dataCount){
            // range와 같거나 range보다 작은 숫자가 남아있을 때
            [hourData addObject:[hourTempData copy]];
            [hourTempData removeAllObjects];
            dataCount -= range;
        }
    }
    return hourData;
}

#pragma mark -

- (id)returnMinutedData:(NSArray *)hourData index:(int)index{
    
    // hourData : 24
    // hour : 60
    // minute : 8,6,5
    NSMutableArray *minuteData = [NSMutableArray new];
    
    for(NSArray *hour in hourData){
        int selectedValue = 0;

        for(NSArray *minute in hour){
            NSNumber *number = minute[index];
            selectedValue += number.intValue;
            int test = number.intValue << 1;

            test += [GeneralUtil randomOddEvenValue];

            NSLog(@"test : %d", test);
        }

        [minuteData addObject:@(selectedValue)];
        selectedValue = 0;
    }
    
//    NSArray *hour = hourData[0];
//    int count = 0;
//    int roof = 0;
//    for(NSArray *minute in hour){
//        NSNumber *number = minute[index];
//        count += number.intValue;
//        roof++;
//        NSLog(@"count : %d / roof : %d", number.intValue, roof);
//    }
//    NSLog(@"count : %d",count);

    return minuteData;
}

#pragma mark -

- (id)returnPedoData:(NSArray *)hourData index:(int)index{
    
    NSMutableArray *minuteData = [NSMutableArray new];
    
    for(NSArray *hour in hourData){
        int pedo = 0;
        
        for(NSArray *minute in hour){
            NSNumber *number = minute[index];
            pedo += number.intValue << 1; // pedoCount 범위 shift 0~31 > 0 ~62
//            NSLog(@"pedo1 : %d",  pedo);
//            pedo += [GeneralUtil randomOddEvenValue]; // 홀,짝 랜덤하게 결정
//            NSLog(@"pedo2 : %d", pedo);
        }
        
        [minuteData addObject:@(pedo)];
        pedo = 0;
    }
    
    return minuteData;
}

#pragma mark - 기간 동안 데이터를 시간 단위로 묶음

- (NSMutableArray *)parsePerHour:(RLMResults *)all{
    
    NSMutableArray *hours = [NSMutableArray new];
    NSPredicate *pred;
//    NSLog(@"all : %@", all);
    
    ArkiBandData *band = all[0];

    NSDate *startOfToday = [GeneralUtil startOfTheDay:[band.date toGlobalTime]];
    NSLog(@"startoftoday : %@ , band : %@ , 3 : %@", startOfToday, band.date, [GeneralUtil startOfTheDay:[band.date toGlobalTime]]);

    for(int i=0; i < 24; i++){

        NSDate *startHour = [startOfToday dateBySettingHour:startOfToday.hour + i minute:0 second:0]; // 시작은 0
        NSDate *afterHour = [startHour dateBySettingHour:startHour.hour + 1 minute:0 second:0];
        
        pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <%@", startHour, afterHour];
        RLMResults *hour = [all objectsWithPredicate:pred];
    
        if(hour.count >= 30){
            [hours addObject:hour];
        }
        else{
            // RLMResult로 된 배열로 묶음
            [hours addObject:[self makeFakeValue:startHour]];
        }
        
        NSLog(@"first : %@ / after : %@ / resutl2.count : %ld",startHour,afterHour, hour.count);
    }

    return hours;
}

- (id)makeFakeValue:(NSDate *)start{
    
    NSLog(@"fake Start : %@", start);
//    ArkiBandData *bandData = [band parseData:data type:activityNum date:[syncDate dateByAddingMinutes:index]];
    
    NSMutableArray *fakes = [NSMutableArray new];
    for(int i=0; i<60; i++){
        ArkiBandData *band = [ArkiBandData new];
        band.Activity = 3;
        band.date = [start dateByAddingMinute:i];
        [fakes addObject:band];
    }
    
    return fakes;
}

- (NSArray *)getDataForDay:(NSPredicate *)pred{
    
    RLMResults *result = [self returnQueryData:pred];

    NSMutableArray *hours, *hourWithActivityType;
    
    if(result.count){
        
        
        // 60개씩 24개의 배열(1시간)로 만듬 , 1개 : 1분
        // sync 시간과 비교해서 한시간 단위로 데이터 셋팅
//        ArkiBandData *firstBand = result[0];
//        ArkiBandData *lastBand = result[result.count - 1];
//
//        NSDate *syncDate = (NSDate *)firstBand.date;
//        NSDate *lastSyncDate = (NSDate *)lastBand.date;
//        
//        NSInteger syncHour = [syncDate toGlobalTime].nearestHour; // first date or 싱크한 시간의 hour를 받음 , 그릴 시작 시간
//        NSInteger lastSyncHour = [lastSyncDate toGlobalTime].nearestHour;
//        
//        NSInteger lastIndex = lastBand.index; // 가져와야 할 length 설정
        
        hours = [self parsePerHour:result]; // 24 시간 데이터를 1시간 단위로 묶음
        
        hourWithActivityType = [self parsePerMinute:hours];

    }
    
    return hourWithActivityType;
}

#pragma mark -

- (NSMutableArray *)parsePerMinute:(NSMutableArray *)hours{
    
    NSMutableArray *hourWithActivity = [NSMutableArray new];
    
    int minutePedo = 0;
    int activity_num = 0;

    int isSleep = 0;
    
    for(id hour in hours){
        
        int test[8] = {0};
        
        for(ArkiBandData *minute in hour){
            
            activity_num = minute.Activity;
            test[activity_num]++;
            
            if(CategoryTypeWalk == activity_num || CategoryTypeSleep == activity_num || CategoryTypeActivity == activity_num)
                minutePedo += minute.pedoCount;
        }
        
        int xmax = -MAXFLOAT;
        int xmin = MAXFLOAT;
        
        for(int i=0; i<8; i++){
            int x = test[i];
            if(x < xmin){
                xmin = x;
            }
            if(x > xmax){
                xmax = x;
                activity_num = i;
            }
        }
        
        // 예외 타입은 Other Activity로 처리
        if(activity_num != CategoryTypeWalk && activity_num != CategoryTypeSleep && activity_num != 3)
            activity_num = CategoryTypeActivity;
        
//        [minutes addObject:@[@(minutePedo),@(activity_num)]];
        
        [hourWithActivity addObject: @[@(activity_num),@(minutePedo), hour]];

        minutePedo = 0;
        isSleep = 0;
        activity_num = 0;

    }
    
    return hourWithActivity;
}

- (NSMutableArray *)parseDataByCategory:(id)hours{

    int activity_num = 0;
    int test[8] = {0};
   
    NSMutableArray *minutes = [NSMutableArray new];
    
    for(id hour in hours){
        
        for(ArkiBandData *minute in hour){
            
            activity_num = minute.Activity;
            test[activity_num]++;
        }
        
        int xmax = -MAXFLOAT;
        int xmin = MAXFLOAT;
        
        for(int i=0; i<8; i++){
            int x = test[i];
            if(x < xmin){
                xmin = x;
            }
            if(x > xmax){
                xmax = x;
                activity_num = i;
            }
        }
        
        if(activity_num == 0)
            activity_num = CategoryTypeActivity;
        
        ArkiBandData *arki = hour[0];
        ArkiBandData *arkiEnd = [hour lastObject];
        [minutes addObject:@[@(activity_num), arki.date, arkiEnd.date]];
        
        // Init
        activity_num = 0;
        
        for(int i=0; i<8; i++){
            test[i] = 0;
        }
    }
    
//    NSLog(@"result :%@", minutes);
    return minutes;
}

#pragma mark - Sleep

//- (id)parseSleepByDate:(RLMResults *)allSleeps{
//    
//    NSMutableArray *bundle = [NSMutableArray new];
//    
//    NSDate *today = [GeneralUtil startOfToday:0];
//    NSDate *yesterday = [today dateByAddingDays:-1];
//    
//    for(int i=0; i < 48; i++){
//        NSDate *startHour = [yesterday dateBySettingHour:yesterday.hour + i minute:0 second:0];
//        NSDate *afterHour = [startHour dateBySettingHour:startHour.hour + 1 minute:0 second:0];
//        
//        NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <%@", startHour, afterHour];
//        RLMResults *result2 = [allSleeps objectsWithPredicate:pred];
//
//        if(result2.count >= 30){
//            [bundle addObject:result2];
//        }
//    }
//    return bundle;
//}

- (id)parseSleepByDate:(RLMResults *)allSleeps StartDate:(NSDate *)theDay{
    
    NSMutableArray *bundle = [NSMutableArray new];
    
    NSDate *today = [GeneralUtil startOfTheDay:theDay];
    NSDate *yesterday = [today dateByAddingDays:-1];
    NSLog(@"startoftheday : %@", today);
    for(int i=0; i < 48; i++){
        NSDate *startHour = [yesterday dateBySettingHour:yesterday.hour + i minute:0 second:0];
        NSDate *afterHour = [startHour dateBySettingHour:startHour.hour + 1 minute:0 second:0];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <%@", startHour, afterHour];
        RLMResults *result2 = [allSleeps objectsWithPredicate:pred];
        
        if(result2.count >= 30){
            [bundle addObject:result2];
        }
        else{
//            NSLog(@"fakevalue : %@", [self makeFakeValue]);
            
            [bundle addObject:[self makeFakeValue:startHour]];
        }
    }
    
    
    for(id aaa in bundle){
//        NSLog(@"bbb : %@", aaa);
    }

    return bundle;
}


- (id)parseConnection:(NSArray *)allSleeps{
    
    NSMutableArray *totalBundle = [NSMutableArray new];
    NSMutableArray *bundle = [NSMutableArray new];
    
    for(int i=0; i<allSleeps.count; i++){
        RLMResults *result = allSleeps[i];
        ArkiBandData *band = result[0];
//        NSLog(@"band.date : %@", band.date);
        
        if(i != 0){
            
            RLMResults *previous = allSleeps[i-1]; // bundle도 동일
            ArkiBandData *previousBand = previous[0];
            
            NSTimeInterval distanceBetweenDates = [band.date timeIntervalSinceDate:previousBand.date];
            double secondsInAnHour = 60;
            
            NSInteger minsBetweenDates = distanceBetweenDates / secondsInAnHour;
//            NSLog(@"minsBetweenDates : %ld", minsBetweenDates);
            if(minsBetweenDates > 60){ // 60분 이상 혹은 마지막
                
                if(bundle.count > 3) // 기준값 이하는 버림
                    [totalBundle addObject:[bundle copy]];
                
                [bundle removeAllObjects];
            }
            else if(i == allSleeps.count - 1){
        
                [bundle addObject:result[0]];
                [bundle addObject:result[result.count - 1]];
                
                if(bundle.count > 3) // 기준값 이하는 버림
                    [totalBundle addObject:[bundle copy]];
                
                break;
            }
        }
        
        [bundle addObject:result[0]];
        [bundle addObject:result[result.count - 1]];
        
    }
    
    return totalBundle;
}

- (RLMResults *)parseSleepPerMinute:(RLMResults *)hours{
    
//    NSMutableArray *sleepData = [NSMutableArray new];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"Activity == 1 OR Activity == 2 OR Activity == 7"];
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"Activity == 2"];
    RLMResults *result = [hours objectsWithPredicate:pred];
    
//    NSNumber * a = [result sumOfProperty:@"pedoCount"];
//    NSLog(@"a : %d", a.intValue);
//    NSLog(@"result.count : %ld", result.count);
    
//        for(ArkiBandData *minute in hours){
//           if(minute.Activity == 2)
//               [sleepData addObject:minute];
//        }
    return result;
}

- (id)parseSleepData:(NSPredicate *)pred{
    
//    NSMutableArray *sleeps = [NSMutableArray new];
    NSMutableArray *bandBundlePerHour = [NSMutableArray new];
    
//    NSDate *today = [GeneralUtil startOfToday:0];
//    NSDate *yesterday = [today dateByAddingDays:-1];
//    NSData *endOfToday = [GeneralUtil endOfToday:0];

//    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
//    
//    for(ArkiBandData *perMinute in result){
//        if(perMinute.Activity == CategoryTypeSleep){
//            [bandBundlePerHour addObject:perMinute];
//            
//            if(bandBundlePerHour.count >= 60){
////                [sleeps addObject:bandBundlePerHour];
//                [sleeps addObjectsFromArray:bandBundlePerHour];
//                NSLog(@"슬립 카운트 : %ld", bandBundlePerHour.count);
//                [bandBundlePerHour removeAllObjects];                
//            }
//        }
//    }
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];      // 기간으로 검색
    
    RLMResults *allCategorySleep = [self parseSleepPerMinute:result];   // 기간안에서의 모든 Category 결과
//    NSArray *sleepByHour = [self parseSleepByDate:allCategorySleep];    // 기간 동안의 Category를 한시간 단위로 묶은 결과
    NSArray *sleepByHour = [self parseSleepByDate:allCategorySleep StartDate:nil];    // 기간 동안의 Category를 한시간 단위로 묶은 결과
    NSArray *sleepForToday = [self parseConnection:sleepByHour];        // 연속된 Sleep Category의 배열
    
   
    // 여기서부터 테스트
    NSArray  *x = [self parseDataByCategory:sleepByHour]; // 대표카테고리와 Date를 한시간 단위로 묶은 배열
    NSLog(@"allCategorySleep:%ld , sleepByHour:%ld , x:%ld",allCategorySleep.count, sleepByHour.count, x.count);
    int counter = 0;
    int index[24] = {0}; // length 동적할당 필요
  
    for(int i=0; i<x.count; i++){
        
        NSNumber *w = [x[i] objectAtIndex:0];

        if(w.intValue == CategoryTypeSleep){
           index[counter+1]++ ;
        }
        else{
            counter = i;
        }
    }
    
    NSMutableArray *a1 = [NSMutableArray new];
    for(int i=0; i<24; i++){
        
        NSArray *a2;
        if(index[i] > SLEEP_THRESHOLD){
            NSLog(@"i:%d , index:%d", i, index[i]);
            a2 = @[@(i), @(index[i])];
            [a1 addObject:a2];
        }
    }
    
//    NSLog(@"a1:%@", a1);
    
    NSArray *z2 = [a1 lastObject];
    int length = [[z2 lastObject] intValue];
    int index2 = [z2[0] intValue];
    
    NSMutableArray *f4 = [NSMutableArray new];
    for(int i=0; i<length; i++){
        [f4 addObject:x[index2+i]];
    }
    
//    NSLog(@"f4 :%@", f4);
    NSArray *k4 = @[[f4 firstObject][1], [[f4 lastObject] lastObject]];
    NSLog(@"k4 : %@", k4);
    
    RLMResults *r1 = [allCategorySleep objectsWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", [k4 firstObject],[k4 lastObject]]];
  
    
    NSInteger diff2 = [[r1 sumOfProperty:@"diffCounter"] intValue];
    NSLog(@"diff : %ld", diff2);
    
    // 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
    //         2 2 2 2 2 2     2  2           2  2  2  2        2  2
    // 1 2 3 4 + + + + + +  5  +  +  6  7  8  +  +  +  +  9 10  +  +
    // 1 2 3 4 + + + + 5 6  7  8  9 10 11 12  +  +  +  + 13 14 15 16
   
    
    // 동작하는 코드(미사용)
    NSMutableArray * ak = [NSMutableArray new];
    NSMutableArray *ka = [NSMutableArray new];
    for(int i=0; i<x.count; i++){
        
        NSNumber *w = [x[i] objectAtIndex:0];
        if(w.intValue == CategoryTypeSleep){
            [ak addObject:x[i]];
        }
        else{
            if(ak.count > SLEEP_THRESHOLD){
                [ka addObject:[ak copy]];
                [ak removeAllObjects];
            }
            else{
                [ak removeAllObjects];
            }
        }
    }
    
    return bandBundlePerHour;
}

- (id)getStartEndDate:(RLMResults *)result theDay:(NSDate *)theDay{

//    NSLog(@"count: %ld , result : %@",result.count, result);
    
//    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];  // 기간으로 검색
    NSArray *byHour = [self parseSleepByDate:result StartDate:theDay];          // 기간 동안의 Category를 한시간 단위로 묶은 결과
    NSArray *categories = [self parseDataByCategory:byHour];   // 카테고리별 Data를 한시간 단위로 묶은 배열
    
    NSLog(@"byHour : [%ld] , categories:[%ld]",byHour.count,categories.count);
    NSLog(@"count : %ld , catego : %@", categories.count, categories);
    
    
    int counter = 0;
    int index[48] = {0}; // length 동적할당 필요
    
    for(int i=0; i<categories.count; i++){
        
        NSNumber *w = [categories[i] objectAtIndex:0];
//        NSLog(@"www : %@", w);
        if(w.intValue == CategoryTypeSleep){
            if(counter == 0)
                index[counter]++ ;
            else
                index[counter+1]++ ;
        }
        else{
            counter = i;
        }
    }
    
    for(int i=0; i<48; i++)
        NSLog(@"iiiii [index]%d : %d",i, index[i]);
    
    NSMutableArray *sleeps = [NSMutableArray new];
   
    for(int i=0; i<48; i++){
        
        NSArray *a;
//        NSLog(@"i:%d , index:%d", i, index[i]);
        if(index[i] > SLEEP_THRESHOLD){
            
            a = @[@(i), @(index[i])];
            [sleeps addObject:a];
        }
    }
    NSLog(@"sleeps : %@",sleeps);
    
    NSArray *startEndDates;
    NSArray *period;
    NSMutableArray *periodDates;
    
    if(sleeps.count){
        period = [sleeps lastObject];
       
        NSLog(@"period : %@", period);
        
        int length = [[period lastObject] intValue];
        int index2 = [[period firstObject] intValue];
        
        NSLog(@"length : %d , index : %d",length, index2);
        
        periodDates = [NSMutableArray new];
//        for(int i = index2; i <= length; i++){
        for(int i = index2; i < (index2 + length); i++){
//            [periodDates addObject:categories[index2+i]];
            [periodDates addObject:categories[i]];

        }
        NSLog(@"periodDates :%@", periodDates);
//        startEndDates = @[[periodDates firstObject][1], [[periodDates lastObject] lastObject]];
    }
       
    // 정확한 시작, 끝 시간 받기
    
    if(periodDates.count){
        startEndDates = [self returnExactStartEndTime:periodDates];
    }
    
    NSLog(@"startEndDates : %@", startEndDates);
    
    return startEndDates;
}

// 위 메소드와 하나로 합쳐야 함
- (NSInteger)getSleepHour:(RLMResults *)result theDay:(NSDate *)theDay{
    
    NSArray *byHour = [self parseSleepByDate:result StartDate:theDay];          // 기간 동안의 Category를 한시간 단위로 묶은 결과
    NSArray *categories = [self parseDataByCategory:byHour];   // 카테고리별 Date를 한시간 단위로 묶은 배열
    
    int counter = 0;
    int index[48] = {0}; // length 동적할당 필요
    
    for(int i=0; i<categories.count; i++){
        
        NSNumber *w = [categories[i] objectAtIndex:0];
        
        if(w.intValue == CategoryTypeSleep){
            if(counter == 0)
                index[counter]++ ;
            else
                index[counter+1]++ ;
        }
        else{
            counter = i;
        }
    }
    
    NSMutableArray *sleeps = [NSMutableArray new];
    
    for(int i=0; i<48; i++){
        
        NSArray *a;
        
        if(index[i] > SLEEP_THRESHOLD){
            
            a = @[@(i), @(index[i])];
            [sleeps addObject:a];
        }
    }
  
    NSLog(@"sleeps : %@", sleeps);
    
    NSArray *period;
    NSMutableArray *periodDates;
    
    if(sleeps.count){
        period = [sleeps lastObject];
        
        int length = [[period lastObject] intValue];
        int index2 = [[period firstObject] intValue];

        periodDates = [NSMutableArray new];
        for(int i=index2; i<(index2 + length); i++){
//            [periodDates addObject:categories[index2+i]];
            [periodDates addObject:categories[i]];
        }
    }
    
    NSLog(@"period count: %ld", periodDates.count);

    return periodDates.count;
}

- (id)returnExactStartEndTime:(NSArray *)periodDates{

    // 정확한 시작 , 끝 시간 받기
    
    NSDate *startSleepTime = [periodDates firstObject][1];
    NSDate *startSleepTimeEnd = [periodDates firstObject][2];
    
    NSDate *endSleepTime = [periodDates lastObject][1];
    NSDate *endSleepTimeEnd = [periodDates lastObject][2];
    
    NSDate *exactStartTime;
    NSDate *exactEndTime;
    
    NSLog(@"startSleepTime : %@ , startSleepTime2 : %@ / end : %@ , end2 : %@",startSleepTime, startSleepTimeEnd, endSleepTime, endSleepTimeEnd);
    
    BandHandler *handler = [BandHandler sharedInstance];
    
    if(startSleepTime){
      
        RLMResults *startTimeResult = [handler getDataWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", startSleepTime, startSleepTimeEnd]];
 
        if(startTimeResult.count){
            
            for(ArkiBandData *band in startTimeResult){
                
                NSLog(@"sleepBand : %@",band);
                
                if(band.Activity == CategoryTypeSleep){
                    exactStartTime = band.date;
                    
                    break;
                }
            }
            
            RLMResults *endTimeResult = [handler getDataWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", endSleepTime, endSleepTimeEnd]];
            
            int endTimeIndex = (int)endTimeResult.count - 1;
            
            for(int i = 0; i < endTimeResult.count; i++){
                
                ArkiBandData *band = endTimeResult[endTimeIndex - i];
                
                NSLog(@"BandSleep : %@ , i : %d", band, i);
                
                if(band.Activity == CategoryTypeSleep){
                    exactEndTime = band.date;
                    
                    break;
                }
            }
        }
    }
    
    return @[exactStartTime, exactEndTime];
}

- (id)getSleepDiffCount:(NSPredicate *)pred theDay:(NSDate *)theDay{
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];  // 기간으로 검색
    
    NSArray *startEndDates = [self getStartEndDate:result theDay:theDay];
 
    NSMutableArray *diffData = [NSMutableArray new];
    NSMutableArray *diffResult = [NSMutableArray new];
    
    if(startEndDates.count){

        RLMResults *dateResult = [result objectsWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", [startEndDates firstObject],[startEndDates lastObject]]];
        RLMResults *categorySleepOrActivity = [dateResult objectsWithPredicate:[NSPredicate predicateWithFormat:@"Activity == %d OR Activity == %d", CategoryTypeSleep, CategoryTypeActivity]];
        
        NSLog(@"categorySleepOrActivity : %ld", categorySleepOrActivity.count);
        
//        NSLog(@"dateResult : %ld, %@",dateResult.count, dateResult);
        if(categorySleepOrActivity.count){
            
            int diffSum = 0, i = 0;
            
            for(ArkiBandData * a in categorySleepOrActivity){
                //        NSLog(@"date: %@ , diff : %d",a.date, a.diffCounter);
                
                int diff = a.diffCounter;
                
                diffSum += diff;
                
                // 5분씩 저장 (0이 아니면)
                if(( i + 1 ) % 5 == 0 && i != 0){
                    
                    [diffData addObject:@(diffSum)];
                    
                    diffSum = 0;
                }
                
                i++;
            }
            NSInteger diff = [[categorySleepOrActivity sumOfProperty:@"diffCounter"] intValue];
            
            [diffResult addObject:@(diff)];
            NSLog(@"diff : %ld , i : %d , count : %ld", diff, i, diffData.count);
        }
    }
    
    [diffResult addObject:diffData];

    return diffResult;
}

- (RLMResults *)getDataWithPredicate:(NSPredicate *)pred{
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    
    if(result.count){
        return result;
    }
    return nil;
}

- (NSInteger)getSleepScore:(NSDate *)date{
    
    NSInteger sleepScore = 0;
    
    NSDate *yesterday = [date dateByAddingDays:-1];
    NSData *endOfToday = [GeneralUtil endOfTheDay:date];
    
//    NSLog(@"yes : %@ , end : %@", yesterday, endOfToday);
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", yesterday, endOfToday];
    
    NSArray *diffData = [self getSleepDiffCount:pred theDay:date];
//    NSLog(@"diffData:%@",diffData);

    NSInteger totalDiffSum = 0;

    if(diffData.count > 1){
        totalDiffSum = [[diffData firstObject] integerValue];        
    }
//    NSLog(@"totalDiffSum:%ld", totalDiffSum);
    
    id data = [self getDataWithPredicate:pred];
    NSArray *period = [self getStartEndDate:data theDay:date];
    NSInteger sleepHour = [self getSleepHour:data theDay:date];
    
    if(period.count){
        RLMResults *result = [self getDataWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", period[0], period[1]]];
        
        NSInteger sleepMinute = result.count;
//        NSLog(@"result : %ld", sleepMinute);
        
        NSInteger sleepScore = ((float)sleepHour / 8.0) * 50;
        
        if(sleepScore > 50)
            sleepScore = 50;
        
        NSInteger diffCounterScore = totalDiffSum / sleepMinute;
        
        NSInteger diffScore = 50 * ((1.0 - ((float)diffCounterScore / 5.0)));
        
        if(diffScore > 50)
            diffScore = 50;
        
        
        NSInteger diffResult = sleepScore + diffScore;

        sleepScore = diffResult;
    }
    
    return sleepScore;
}

- (NSString *)getSleepTime:(NSDate *)startDate{
    
    NSString *hour;
    NSString *minute;
    NSString *time;
    
    NSDate *today = [GeneralUtil startOfTheDay:startDate];
    NSDate *yesterday = [today dateByAddingDays:-1];
    
    NSData *endOfToday = [GeneralUtil endOfTheDay:startDate];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", yesterday, endOfToday];
    
    RLMResults *data = [self getDataWithPredicate:pred];

    if(data.count){
        
        NSArray *period = [self getStartEndDate:data theDay:startDate];

        if(period.count){
            NSDate *exactStart = [period firstObject];
            NSDate *exactEnd = [period lastObject];
            
            hour = [NSString stringWithFormat:@"%ld", [exactStart minutesBeforeDate:exactEnd] / 60];
            minute = [NSString stringWithFormat:@"%ld", [exactStart minutesBeforeDate:exactEnd] % 60];
            
            time = [NSString stringWithFormat:@"%@H %@m", hour, minute];
        }
    }
    else{
        time = nil;
    }
    
    
    NSLog(@"시차:%@ , 분차:%@", hour, minute);
    
    return time;
}

#pragma mark - SoundWalking

- (id)parseByHourInDate:(RLMResults *)allSleeps{
    
    NSMutableArray *bundle = [NSMutableArray new];
    
    NSDate *yesterday = [GeneralUtil startOfToday:0];
    
    NSArray *bundle2;

    // 24를 날짜로 변경해야함
    for(int i=0; i < 24; i++){
      
        NSDate *lastWalkingTime;
        NSDate *startHour = [yesterday dateBySettingHour:yesterday.hour + i minute:0 second:0];
        NSDate *afterHour = [startHour dateBySettingHour:startHour.hour + 1 minute:0 second:0];
        
        NSLog(@"start : %@ , after : %@", startHour, afterHour);
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <%@", startHour, afterHour];
        RLMResults *result2 = [allSleeps objectsWithPredicate:pred];
        
        NSLog(@"result2 : %ld",result2.count);
        
        if(result2.count >= 30){
            
            NSInteger totalWalkNum = 0;
            
            for(int i=0; i<result2.count; i++){
                totalWalkNum ++;
                
            }
//            NSLog(@"토탈 : %ld", totalWalkNum);
            
            int index[8] = {0};
            int walkCategory = 0;
            
            // walknum이 10개 이상인지 체크
            if(totalWalkNum > 10){
               
                // 대표 walkingType 찾기
                
                for(int i=0; i<result2.count; i++){
                
                    ArkiBandData *band = result2[i];
                    index[band.walkNum]++;
                    
                    int walkType = band.walkNum;
                    
                    // 마지막 걸은 시간 저장
                    if(walkType != WalkTypeEmpty && walkType != WalkTypeEmpty2){
                        lastWalkingTime = band.date;
                    }
                }

                for(int i=0; i<result2.count; i++){
                    
                    int xmax = -MAXFLOAT;
                    int xmin = MAXFLOAT;
                    
                    for(int i=0; i<8; i++){
                        int x = index[i];
                        if(x < xmin){
                            xmin = x;
                        }
                        if(x > xmax){
                            xmax = x;
                            walkCategory = i;
                        }
                    }
                }
               
                ArkiBandData *first = result2[0];
//                ArkiBandData *last = result2[result2.count - 1];
                NSLog(@"lastWalkingDate : %@", lastWalkingTime);
                
                bundle2 = @[@(walkCategory),first.date, lastWalkingTime, @(totalWalkNum)];
                [bundle addObject:bundle2];
            }
        }
    }
    
//    NSLog(@"bundel : %@", bundle);
    
    return bundle;
}

- (id)getSpecificActivityTipeData:(id)aDay activity:(int)mActivityType{

//    NSLog(@"aDay : %@", aDay);
    
    NSMutableArray *result = [NSMutableArray new];
    
    for(id aHour in aDay){
        
        if([[aHour lastObject] isKindOfClass:[RLMResults class]]){
         
            if([[aHour firstObject] intValue] == mActivityType || [[aHour firstObject] intValue] == CategoryTypeActivity){
                [result addObject:aHour];
            }
        }
    }
    
    return result;
}

- (id)getWalkingActivity:(NSArray *)anHour{
  
    NSLog(@"anhour.count : %ld", anHour.count);
    
    NSMutableArray *lastObjects = [NSMutableArray new];
    
    for(int i=0; i<anHour.count; i++){
        
        RLMResults *result = [anHour[i] lastObject];
        NSLog(@"result.count : %ld", result.count);
        
        NSMutableArray * bands  = [NSMutableArray new];
        
        for(ArkiBandData *band in result){
            [bands addObject:band];
        }
        [lastObjects addObject:bands];
    }
   
    NSLog(@"lastobject.count : %ld", lastObjects.count);
    
    NSMutableArray *walks = [NSMutableArray new];
    
    for(NSArray *data in lastObjects){
        
        NSMutableArray * bands  = [NSMutableArray new];
        
        for(ArkiBandData *band in data){
            NSLog(@"bababa : %@", band);
            
            if(band.Activity == CategoryTypeWalk){
                [bands addObject:band];
            }
        }
        
        [walks addObject:bands];
        
    }
    
    return walks;
}

- (id)parseByHour:(NSArray *)aDay activityType:(int)mActivityType{
    
    NSMutableArray *activities = [NSMutableArray new];
    
    NSArray *hourByActivity = [self getSpecificActivityTipeData:aDay activity:mActivityType];
    
    NSArray *walkingActivity = [self getWalkingActivity:hourByActivity];
    
    NSLog(@"walkingActivity : %ld", walkingActivity.count);
    
//    for(ArkiBandData *test in walkingActivity){
////        NSLog(@"testttt : %@", test);
//    }
    
    NSDate *lastWalkingDate;
    
    NSLog(@"hourByActivity : %ld", hourByActivity.count);
    
    for(int i = 0; i <walkingActivity.count; i ++){
        
        NSArray *aHour = walkingActivity[i];

        NSLog(@"aHour.length : %ld , aHour1 : %@",aHour.count, aHour);
        
        int index[8] = {0};
        
        int walkCategory = 0;
        
//        if(totalWalkNum >= 10){
        
            // 한시간 데이터
        NSArray *result = aHour;
        
        for(int j = 0; j < result.count; j ++){
            
            ArkiBandData *band = result[j];
            index[band.walkNum]++;
            
            // 마지막 걸은 시간 저장
            int walkType = band.walkNum;
            
            if(walkType != WalkTypeEmpty && walkType != WalkTypeEmpty2){
                lastWalkingDate = band.date;
            }
        }
        
        for(int i=0; i<result.count; i++){
            
            int xmax = -MAXFLOAT;
            int xmin = MAXFLOAT;
            
            for(int i=0; i<8; i++){
                int x = index[i];
                if(x < xmin){
                    xmin = x;
                }
                if(x > xmax){
                    xmax = x;
                    walkCategory = i;
                }
            }
        }
        
        NSLog(@"대표 워크넘 : %d", walkCategory);

        for(int i=0; i<8; i++){
            NSLog(@"walkingIndex : %d i : %d", index[i], i);
        }
        
        int totalWalkCount = index[walkCategory];
        NSLog(@"totalWalkCount : %d", totalWalkCount);
        
        // 한가지 대표값이 10분 이상일 때만 노출
        if(totalWalkCount >= 10){

            ArkiBandData *first = result[0];
            
            NSArray *bundle = @[@(walkCategory),first.date, lastWalkingDate, @(index[walkCategory])];
            
            [activities addObject:bundle];
        }
    }
   
    return activities;
}


- (id)fetchSoundWalking:(RLMResults *)all firstInde:(int)start lastIndex:(int)end{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"index >= %d AND index < %d", start, end];
    RLMResults *result = [all objectsWithPredicate:pred];
    
    ArkiBandData *firstData = result[0];
    
    int oldType = firstData.walkNum, newType = 0;

    NSMutableArray *walks = [NSMutableArray new];
    NSMutableArray *equals = [NSMutableArray new];
    
    for(ArkiBandData *data in result){
        newType = data.walkNum;
        
        if(newType == oldType){
            [equals addObject:@(newType)];
        }
        else{
            // 같은 타입끼리 저장 , 다른 타입이 시작될 때 같은 타입의 마지막 인덱스를 갖고 있는게 나을듯
            [walks addObject:[equals copy]];
            [equals removeAllObjects];
            
            [equals addObject:@(newType)];
            
            oldType = newType;
        }
    }
    
    return walks;
}

- (id)getSoundWalking:(RLMResults *)result{
    
    ArkiBandData *firstData = result[0];
    
    int oldType = firstData.walkNum, newType = 0;
    
    NSMutableArray *walks = [NSMutableArray new];
    NSMutableArray *equals = [NSMutableArray new];
    
    for(ArkiBandData *data in result){
        newType = data.walkNum;
        
        if(newType == oldType){
            [equals addObject:@(newType)];
        }
        else{
            // 같은 타입끼리 저장 , 다른 타입이 시작될 때 같은 타입의 마지막 인덱스를 갖고 있는게 나을듯
            [walks addObject:[equals copy]];
            [equals removeAllObjects];
            
            [equals addObject:@(newType)];
            
            oldType = newType;
        }
    }
    
    return walks;
}

#pragma mark -

- (int)returnPedoCount:(NSPredicate *)pred{
    
    int totalPedoCount = 0;
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    //    NSLog(@"result : %@", result);
    for(ArkiBandData *band in result){
        totalPedoCount += band.pedoCount;
    }
//    NSLog(@"총걸음수 : %d", totalPedoCount);
    return totalPedoCount;
}

#pragma mark -

- (NSInteger)returnBandData:(NSPredicate *)pred property:(NSInteger)property{
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    NSString *propertyName;
    
    switch (property) {
        case BandPropertyTypeDate:
            propertyName = @"date";
            break;
        case BandPropertyTypePedo:
            propertyName = @"pedoCount";
            break;
        case BandPropertyTypeDiffCounter:
            propertyName = @"diffCounter";
            break;
        case BandPropertyTypeSleepCounter:
            propertyName = @"sleepCounter";
            break;
        default:
            break;
    }

    NSInteger totalValue = [self returnSelectedBandData:propertyName totalData:result];

    return totalValue;
}

#pragma mark -

- (NSInteger)returnSelectedBandData:(NSString *)property totalData:(RLMResults *)result{
    
    NSInteger totalValue = 0;
    for(ArkiBandData *band in result){
        totalValue += [band[property] integerValue];;
    }
    return totalValue;
}

#pragma mark - WalkingScore Calulate

- (float)calculateWalkingScore:(NSPredicate *)pred{
//- (float)calculateWalkingScore:(NSDate *)start end:(NSDate *)end{

    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    
    NSInteger maxStep = [[result sumOfProperty:@"totalPedoCount"] integerValue] ;
    
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", start, end];

    return (float)((float)maxStep / (MAX_STEP_PER_DAY)) * 100;
}

- (float)calculateOneWalkingScore:(NSPredicate *)pred{
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    NSLog(@"result : %@",result);
//    NSInteger maxStep = [[result sumOfProperty:@"pedoCount"] integerValue] ;
    NSInteger maxStep = [[result sumOfProperty:@"totalPedoCount"] integerValue] ;
    NSLog(@"maxStep : %ld", maxStep);
    
    if(maxStep > MAX_STEP_PER_DAY)
        maxStep = MAX_STEP_PER_DAY;
    
    return (float)((float)maxStep / (MAX_STEP_PER_DAY)) * 100;
}


#pragma mark - Time Sync

- (NSDate *)syncedDate:(NSPredicate *)pred{
 
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    NSLog(@"result:%@",result);
    
    return nil;
}

#pragma mark -

- (id)returnQueryData:(NSPredicate *)pred{
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    
    return result;
}

#pragma mark -

- (void)makeFakeData:(NSInteger)length{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    for(int i=0; i<length; i++){
        NSArray *temp = @[@0,@0,@0];
        ArkiBandData *bandData = [self parseData:temp type:0 date:[NSDate date]];
        [realm addObject:bandData];
    }
    [realm commitWriteTransaction];
}

#pragma mark -

- (void)saveDataToDB:(NSArray *)data type:(int)type date:(NSDate *)date{
    
    ArkiBandData *aData = [[ArkiBandData alloc] init];
    int pedoCount = 0;
    
    aData.date = date;
    aData.Activity = [data[0] intValue];
    pedoCount = [data[1] intValue];
    aData.pedoCount = pedoCount + [GeneralUtil randomOddEvenValue]; // 홀,짝 랜덤하게 결정
    aData.index = [[ArkiBandData allObjects] count];

}

#pragma mark - Save Parsed Data To DB

- (id)parseData:(NSArray *)data type:(int)type date:(NSDate *)date{
//    NSLog(@"Date : %@", date);
    ArkiBandData *aData = [[ArkiBandData alloc] init];
    int pedoCount = 0;
    
    aData.index = [[ArkiBandData allObjects] count];
    aData.uid = [NSString stringWithFormat:@"%ld", aData.index];
    aData.date = date;
    aData.leftRight = [data[0] intValue];
    aData.authen = [data[1] intValue];
    aData.Activity = [data[2] intValue];
    pedoCount = [data[3] intValue];
    aData.pedoCount = pedoCount + [GeneralUtil randomOddEvenValue]; // 홀,짝 랜덤하게 결정
    aData.accelMag = [data[4] intValue];
    
    
    switch (type) {
        case CategoryTypeWalk:
            aData.walkNum = [data[5] intValue];
            break;
        case CategoryTypeSleep:
//            NSLog(@"diff : %d", [data[5] intValue]);
            aData.diffCounter = [data[5] intValue];
            break;
        case CategoryTypeActivity:
            aData.diffCounter = [data[5] intValue];
            break;
    }
    
    
    
//    [self saveToDb:aData];
    return aData;
    
//    RLMRealm *realm = [RLMRealm defaultRealm];
//    [realm beginWriteTransaction];
//    [realm addObject:aData];
//    [realm commitWriteTransaction];
}

- (void)saveToDb:(id)data{
    
//    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [_realm beginWriteTransaction];
    [_realm addObject:data];
//    [realm commitWriteTransaction];
}

#pragma mark - Reset DB

- (void)resetDB{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *result = [ArkiBandData allObjects];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:result ];
    [realm commitWriteTransaction];
    NSLog(@"result.count : %ld", result.count);
    
}

- (void)removeObjectWithPredicate:(NSPredicate *)pred{
    
    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:result];
    [realm commitWriteTransaction];
    
}

- (void)removeObject:(id)band{
    
//    RLMResults *result = [ArkiBandData objectsWithPredicate:pred];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:band];
    [realm commitWriteTransaction];
    
}

#pragma mark - Send Raw Data

- (void)sendRawData:(NSData *)data{
    
    PFUser *user = [PFUser currentUser];
    
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:@"syncDate"];
   
    PFFile *file = [PFFile fileWithName:@"rawData.bin" data:data];
    [file saveInBackground];
    
    PFObject *binaryObject = [PFObject objectWithClassName:@"Binary_Log"];
    
    binaryObject[@"binaryFile"] = file;
    binaryObject[@"syncDate"] = date;
    binaryObject[@"userId"] = user;
    [binaryObject saveInBackground];
    
    NSLog(@"date : %@", date);
    
//    PFFile *binaryResume = binaryObject[@"binaryFile"];
//    NSData *resumeData = [binaryResume getData];
//    
//    NSLog(@"resumeData : %@", resumeData);
}

#pragma mark - Get Raw Data

- (void)getRawData:(NSDate *)date{

    NSLog(@"date : %@", date);
    PFUser *user = [PFUser currentUser];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"userId == %@ AND syncDate == %@",user, date];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"syncDate >= %@ AND syncDate <= %@", [[NSDate date] dateByAddingDays:0], [[NSDate date] dateByAddingDays:5]];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"userId == %@", user];
    PFQuery *query = [PFQuery queryWithClassName:@"Binary_Log" predicate:predicate];
    __block NSData *raw;
    
    NSLog(@"userId = %@ / syncDate = %@", user.objectId, date);
    
   
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!object) {
            NSLog(@"The getFirstObject request failed.");
        } else {
            // The find succeeded.
            NSLog(@"Successfully retrieved the object.");

            PFFile *binary = object[@"binaryFile"];
            raw = [binary getData];
            NSLog(@"raw : %@", raw);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getRawData" object:raw];
        }
    }];
}

- (void)getRawDataFromServer{
    
    NSLog(@"");
    
    PFUser *user = [PFUser currentUser];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"userId == %@",user];

    PFQuery *query = [PFQuery queryWithClassName:@"Binary_Log" predicate:predicate];

    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            // There was an error
        } else {
         
//            for(PFFile *file in objects){
//                NSLog(@"file : %@", file);
//            }
//            PFFile *binary = object[@"binaryFile"];
//            raw = [binary getData];
//            NSLog(@"raw : %@", raw);
//
            [[NSNotificationCenter defaultCenter] postNotificationName:@"storeRawData" object:objects];

        }
    }];
}


@end
