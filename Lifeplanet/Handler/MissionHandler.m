//
//  MissionHandler.m
//  Arki
//
//  Created by ken on 2015. 6. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "MissionHandler.h"
#import <Realm/realm/Realm.h>
#import "MissionData.h"

@implementation MissionHandler

+ (void)saveMission:(NSString *)mission clear:(BOOL)isClear{
    
    NSDate *weekStart = [[NSDate date] dateWeekStart];
    NSDate *weekStart2 = [GeneralUtil startOfTheDay:weekStart];
    
    NSDate *weekEnd = [[NSDate date] dateWeekEnd];
    NSDate *weekEnd2 = [GeneralUtil startOfTheDay:weekEnd];

    NSLog(@"start: %@ , end : %@", weekStart2, weekEnd2);
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"startWeek >= %@ AND endWeek <= %@", weekStart2, weekEnd2];
    RLMResults *result = [MissionData objectsWithPredicate:pred];
    
    NSLog(@"result : %@", result);
    
    NSString *missionCleared = @"no";
    
    if(isClear)
        missionCleared = @"yes";
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    
    if(result.count){

        MissionData *old = result[0];

        if([old[mission] isEqualToString:missionCleared]){
            return;
        }
        MissionData *new = [MissionData new];
        new.index = old.index;
        new.uid = old.uid;
        new.startWeek = old.startWeek;
        new.endWeek = old.endWeek;
        new.balance1 = old.balance1;
        new.balance2 = old.balance2;
        new.balance3 = old.balance3;
        new.sleep1 = old.sleep1;
        new.sleep2 = old.sleep2;
        new.sleep3 = old.sleep3;
        new.soundWalking1 = old.soundWalking1;
        new.soundWalking2 = old.soundWalking2;
        new.soundWalking3 = old.soundWalking3;
        new.pedo1 = old.pedo1;
        new.pedo2 = old.pedo2;
        new.pedo3 = old.pedo3;
        
        new[mission] = missionCleared;
        
        [MissionData createOrUpdateInRealm:realm withValue:new];
    }
    else{
        MissionData *new = [MissionData new];
        new.uid = [NSString stringWithFormat:@"%ld",[[MissionData allObjects] count]];
        new.index = [[MissionData allObjects] count];
        new.startWeek = weekStart2;
        new.endWeek = weekEnd2;
        new.balance1 = @"no";
        new.balance2 = @"no";
        new.balance3 = @"no";
        new.sleep1 = @"no";
        new.sleep2 = @"no";
        new.sleep3 = @"no";
        new.soundWalking1 = @"no";
        new.soundWalking2 = @"no";
        new.soundWalking3 = @"no";
        new.pedo1 = @"no";
        new.pedo2 = @"no";
        new.pedo3 = @"no";
        
        new[mission] = missionCleared;
        
        [realm addObject:new];
    }
    
    [realm commitWriteTransaction];
    
    NSLog(@"total mission : %@", [MissionData allObjects]);
}

+ (NSArray *)getWeekData:(NSDate *)date{
    
    NSDate *weekStart = [date dateWeekStart];
    NSDate *weekStart2 = [GeneralUtil startOfTheDay:weekStart];
    
    NSDate *weekEnd = [date dateWeekEnd];
    NSDate *weekEnd2 = [GeneralUtil startOfTheDay:weekEnd];
    
    NSLog(@"start: %@ , end : %@", weekStart2, weekEnd2);
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"startWeek >= %@ AND endWeek <= %@", weekStart2, weekEnd2];
    RLMResults *result = [MissionData objectsWithPredicate:pred];
    
    NSArray *data;
    
    if(result.count){
        MissionData *missionData = [result firstObject];
        data = @[missionData.balance1,missionData.balance2,missionData.balance3,missionData.sleep1,missionData.sleep2,missionData.sleep3,missionData.soundWalking1,missionData.soundWalking2,missionData.soundWalking3,missionData.pedo1,missionData.pedo2,missionData.pedo3];
    }
    
    
    return data;
}

@end
