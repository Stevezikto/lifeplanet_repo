//
//  BinaryHandler.h
//  Arki
//
//  Created by ken on 2015. 2. 16..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinaryHandler : NSObject

+ (void)fetchData:(NSDate *)date complete:(void (^)(BOOL complete))complete;

@end
