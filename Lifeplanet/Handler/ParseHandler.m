//
//  ParseHandler.m
//  Arki
//
//  Created by ken on 2015. 5. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "ParseHandler.h"

@implementation ParseHandler


+ (PFUser *)getParseUser{
    
    if([PFUser currentUser])
        return [PFUser currentUser];
    else
        return nil;
}


+ (void)getVideoList:(void(^)(id data))complete{

//    PFUser *user = [PFUser currentUser];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Video_list"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            // There was an error
        } else {
            // objects has all the Posts the current user liked.
            NSLog(@"object : %@", objects);
            
            
//            PFFile *userImageFile = anotherPhoto[@"imageFile"];
//            [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
//                if (!error) {
//                    UIImage *image = [UIImage imageWithData:imageData];
//                }
//            }];

            complete(objects);
        }
    }];
    
}

+ (void)getThumbNail:(PFObject *)object complete:(void(^) (id data))complete{

//    NSLog(@"object : %@", object.objectId);
    
    PFQuery *query = [PFQuery queryWithClassName:@"Video_list"];
    [query whereKey:@"objectId" equalTo:object.objectId];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!object) {
            NSLog(@"The getFirstObject request failed.");
        } else {
            // The find succeeded.
            NSLog(@"object : %@", object);

            NSMutableArray *thumbs = [NSMutableArray new];
            
            PFFile *thumbNail = object[@"videoThumb1"];
            PFFile *thumbNail2 = object[@"videoThumb2"];
            PFFile *thumbNail3 = object[@"videoThumb3"];
            
            // Thumbnail 이미지 블럭 코드 처리
            [thumbNail getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if(imageData){
                    [thumbs addObject:imageData];
                    [thumbNail2 getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                        if(imageData){
                            [thumbs addObject:imageData];
                            [thumbNail3 getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                                if(imageData){
                                    [thumbs addObject:imageData];
                                    complete(thumbs);
                                }
                            }];
                        }
                    }];
                }
            }];
        }
    }];
}

+ (void)getMissionList:(void (^)(id data))complete{
 
//    NSMutableArray *missions = [NSMutableArray new];
//    NSMutableArray *thumbs = [NSMutableArray new];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Mission"];
    [query addAscendingOrder:@"missionCategory"];
    [query addAscendingOrder:@"missionType"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            // There was an error
        } else {
            
//            NSLog(@"object : %@", objects);
            
            complete(objects);
            
//            __block NSInteger length = 1;
//            
//            for(PFObject *ob in objects){
//               
////                [missions addObject:ob];
////                NSLog(@"obob : %@", ob);
//                
//                PFQuery *query = [PFQuery queryWithClassName:@"Mission"];
//                [query whereKey:@"objectId" equalTo:ob.objectId];
//                
//                [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
//                    if (!object) {
//                        NSLog(@"The getFirstObject request failed.");
//                    } else {
//                        
//                        // The find succeeded.
//
//                        PFFile *thumbNail = object[@"missionIcon"];
//                        
//                        NSLog(@"오부 : %@", ob);
//
//                        [thumbNail getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
//                            if(imageData){
//
//                                [missions addObject:@[ob, imageData]];
////                                [thumbs addObject:imageData];
//                                
//                                length ++;
//                                
//                                if(length == objects.count)
//                                    complete(missions);
//                            }
//                        }];
//                    }
//                }];
//            }
//            complete(objects, thumbs);
        }
    }];
}

+ (void)getMissionThumbNale:(PFObject *)object index:(int)index complete:(void(^) (id data))complete{
    
//    NSMutableArray *thumbNails = [NSMutableArray new];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Mission"];
    [query whereKey:@"objectId" equalTo:object.objectId];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!object) {
            NSLog(@"The getFirstObject request failed.");
        } else {
            // The find succeeded.
//            NSLog(@"오브젝트 : %@", object);
            
            PFFile *thumbNail = object[@"missionIcon"];
            
//            NSData *imageData = [thumbNail getData];
//            
//            complete(@{@"image": imageData, @"index": @(index)});
            
            [thumbNail getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if(imageData){
                    complete(@{@"image": imageData, @"index": @(index)});
                }
            }];
        }
    }];
}

@end
