//
//  ParseHandler.h
//  Arki
//
//  Created by ken on 2015. 5. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface ParseHandler : NSObject

// Video
+ (void)getVideoList:(void(^)(id data))complete;
+ (void)getThumbNail:(id)object complete:(void(^) (id data))complete;
+ (PFUser *)getParseUser;

// Mission
+ (void)getMissionList:(void (^)(id data))complete;
+ (void)getMissionThumbNale:(PFObject *)object index:(int)index complete:(void(^) (id data))complete;
@end
