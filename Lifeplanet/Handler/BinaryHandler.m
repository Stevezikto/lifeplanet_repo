//
//  BinaryHandler.m
//  Arki
//
//  Created by ken on 2015. 2. 16..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "BinaryHandler.h"
#import "Mask.h"
#import "BandData.h"
#import "ArkiBandData.h"
#import <NSDate+Calendar/NSDate+Calendar.h>

#define AMOUNT_PER 8

@implementation BinaryHandler

/*
 1.마스킹 할 위치에 해당하는 부분의 값을 이진수 1로 바꾸고 나머지는 0으로 만든다
 2.해당 위치의 실제 데이터값(original)과 마스킹 값을 마스킹한다 (&)곱하기연산
 3.마스킹 한 결과값을 shifting 한다 (실제 값을 받기 위해 민다)
 
 int value = CFSwapInt32BigToHost(*(int*)([data4 bytes]));
 int value2 = CFSwapInt32LittleToHost(*(int*)([data4 bytes]));
 unsigned int len = (uint32_t)[data length];
 uint32_t little = (uint32_t)NSSwapHostIntToLittle(len);
 */

+ (void)fetchData:(NSDate *)date complete:(void (^)(BOOL complete))complete{
    
    NSString * path = [[NSBundle mainBundle] pathForResource:@"binfile_shawn0402-3" ofType:@"bin"];
    // Path 에서 바이너리 파일 읽음
    NSData *binary = [NSData dataWithContentsOfFile:path];
//    NSLog(@"binary.length : %ld", binary.length);
    int range = 0; // data에서 4bytes씩 가져오기 위한 value
    
    Mask *mask = [Mask sharedInstance]; // 받은 데이터를 마스킹 해서 필요한 값만 돌려주는 클래스
//    NSLog(@"binary : %@", binary);
//    BandData *band = [BandData sharedInstance]; // 데이터 모델
    
    // 실행시마다 db 초기화
//    NSString *defaultRealmPath = [RLMRealm defaultRealmPath];
//    [[NSFileManager defaultManager] removeItemAtPath:defaultRealmPath error:nil];
    
    for(int i=0; i< binary.length/AMOUNT_PER; i++){
        // 4byte씩 가져옴 (8 BYTE로 변경)
        NSData *fourBytes = [binary subdataWithRange:NSMakeRange(range, AMOUNT_PER)];

        UInt64 original = CFSwapInt64LittleToHost(*(UInt64*)([fourBytes bytes])); // 바이트 정렬 순서를 리틀엔디언 > 호스트의 byte order로 32bit(64) 인티져형으로 변환해줌
//                NSLog(@"fourbytes.original : %llu", original);
        NSArray *categoryData = [mask maskWithValue:original];
//                NSLog(@"FourBytesData : %@ / HexToDecimal : %u / Hex : %u",fourBytes, original, 0xf8b33e70);
        range +=AMOUNT_PER;
        // 카테고리 타입 구분
        NSNumber *category = categoryData[2];
//        NSLog(@"category : %@", categoryData);
        ArkiBandData *arki = [self parseData:categoryData type:category.intValue date:date];
        [self saveToDb:arki];
//        NSLog(@"i:%d",i);
    }
    complete(YES);
}


+ (id)parseData:(NSArray *)data type:(int)type date:(NSDate *)date{
//    NSLog(@"type : %d / data: %@", type, data);
    NSLog(@"date:%@", date);
    ArkiBandData *aData = [[ArkiBandData alloc] init];
    int pedoCount = 0;
    
    aData.date = date;
//    NSLog(@"date : %@", aData.date);
    
    pedoCount = [data[3] intValue];
    aData.pedoCount = pedoCount + [GeneralUtil randomOddEvenValue]; // 홀,짝 랜덤하게 결정
    //            bandData.pedoCount = [categoryData[1] intValue] << 1;
    
    switch (type) {
        case CategoryTypeWalk:
            aData.walkNum = [data[4] intValue];
                       break;
        case CategoryTypeSleep:
                      break;
//        case CategoryTypeActivity:
//            break;
    }
    
    aData.index = [[ArkiBandData allObjects] count];
    
    return aData;
}

+ (void)saveToDb:(id)data{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addObject:data];
    [realm commitWriteTransaction];
}

@end
