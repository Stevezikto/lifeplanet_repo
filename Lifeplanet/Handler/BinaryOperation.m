//
//  MyOperation.m
//  Arki
//
//  Created by ken on 2015. 2. 24..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "BinaryOperation.h"
#import "BinaryHandler.h"
#import "BLEHandler.h"
#import "NSDate+HowManyWeek.h"

@implementation BinaryOperation

- (void)main{
    
//    [BinaryHandler fetchData:[[NSDate date] toLocalTime] complete:^(BOOL complete) {       
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"fetchBinary" object:nil];
//    }];
    [self performSelector:@selector(scanBLE) withObject:nil afterDelay:0.1];
}

- (void)scanBLE{
//    BLEHandler *ble = [BLEHandler sharedInstance];
//    [ble scanBLEWithComplete:^(NSArray *data) {
//        NSLog(@"data:%@",data);
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"fetchBinary" object:nil];
//    }];
}
- (void)getNotification{
    
    __block __weak id notification;
    
    notification = [[NSNotificationCenter defaultCenter] addObserverForName:@"succedBLE" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [[NSNotificationCenter defaultCenter] removeObserver:notification
                                                        name:@"succedBLE"
                                                      object:nil];
    }];
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
