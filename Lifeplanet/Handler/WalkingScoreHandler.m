//
//  WalkingScoreHandler.m
//  Arki
//
//  Created by ken on 2015. 5. 20..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "WalkingScoreHandler.h"
#import <Realm/Realm.h>
#import "WalkingScoreData.h"

#define MAX_STEP_PER_DAY 20000

@implementation WalkingScoreHandler

+ (WalkingScoreHandler *)sharedInstance
{
    static dispatch_once_t pred;
    static WalkingScoreHandler *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[WalkingScoreHandler alloc] init];
    });
    
    return view;
}

- (id)init{
    
    self = [super init];
    if (self)
    {
    }
    return self;
}

- (void)addSoundWalkingDataToDB:(int)data withDate:(NSDate *)date{
  
    NSLog(@"date : %@ , data : %d", date, data);
    
    NSLog(@"temp date : %@", [date dateByAddingDays:-1]);
    
//    NSDate *today = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
    NSDate *today = date;
    NSDate *tomorrow = [today dateByAddingDays:1];
    
    NSLog(@"toda : %@ , tomo : %@", today, tomorrow);
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", today, tomorrow];
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    NSLog(@"result :%@", result);
    
    RLMRealm *realm = [RLMRealm defaultRealm];

    [realm beginWriteTransaction];
  
    if(result.count){
        WalkingScoreData *old = result[0];
        
        WalkingScoreData *scoreData = [WalkingScoreData new];
        scoreData.index = old.index;
        scoreData.uid = old.uid;
        scoreData.date = old.date;
        scoreData.soundWalkingScore = data;
        scoreData.bodyBalanceScore1 = old.bodyBalanceScore1;
        scoreData.bodyBalanceScore2 = old.bodyBalanceScore2;
        scoreData.bodyBalanceScore3 = old.bodyBalanceScore3;
        scoreData.pedoCountForDay = old.pedoCountForDay;
        
        [WalkingScoreData createOrUpdateInRealm:realm withValue:scoreData];
    }
    else{
        WalkingScoreData *scoreData = [WalkingScoreData new];
        scoreData.soundWalkingScore = data;
        scoreData.uid = [NSString stringWithFormat:@"%ld",[[WalkingScoreData allObjects] count]];
        scoreData.index = (int)[[WalkingScoreData allObjects] count];
        scoreData.date = date;

        [realm addObject:scoreData];
    }
    
    [realm commitWriteTransaction];
    
    NSLog(@"WalkingScoreData : %@", [WalkingScoreData allObjects]);
}

- (void)addBalanceDataToDB:(id)data withDate:(NSDate *)date{
  
    NSLog(@"date : %@, data : %@", date, data);
    
    NSArray *datas = (NSArray *)data;
    
//    NSDate *today = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
//    NSDate *today = [[date toLocalTime] dateBySettingHour:0 minute:0 second:0];
//    NSDate *today = [[[date toLocalTime] dateBySettingHour:0 minute:0 second:0] dateBySubtractingDays:1];
    NSDate *today = date;
    NSDate *tomorrow = [today dateByAddingDays:1];
    
    NSLog(@"toda : %@ , tomo : %@", today, tomorrow);
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", today, tomorrow];
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    
    NSLog(@"pred : %@", pred);
    
    NSLog(@"result : %@", result);
   
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    
    
    if(result.count){
       
        NSLog(@"밸런스 데이타 누적");
        
        WalkingScoreData *old = result[0];
        
        WalkingScoreData *scoreData = [WalkingScoreData new];
        scoreData.index = old.index;
        scoreData.uid = old.uid;
        scoreData.date = old.date;
        scoreData.soundWalkingScore = old.soundWalkingScore;
        scoreData.pedoCountForDay = old.pedoCountForDay;
        
        // data 수만큼 루프
        
        
        for(int i=0; i<datas.count; i++){
            if(i == 0)
                scoreData.bodyBalanceScore1 = [datas[0] intValue];
            else if(i == 1)
                scoreData.bodyBalanceScore2 = [datas[1] intValue];
            else if(i == 2)
                    scoreData.bodyBalanceScore3 = [datas[2] intValue]; // crash issue
        }
//        if(data[0])
//            scoreData.bodyBalanceScore1 = [data[0] intValue];
//        if(data[1])
//            scoreData.bodyBalanceScore2 = [data[1] intValue];
//        if(data[2])
//            scoreData.bodyBalanceScore3 = [data[2] intValue]; // crash issue
        
        
        [WalkingScoreData createOrUpdateInRealm:realm withValue:scoreData];
    }
    else{
        NSLog(@"밸런스 데이타 새로 만듬");
        
        WalkingScoreData *scoreData = [WalkingScoreData new];
        scoreData.soundWalkingScore = 0;
        scoreData.uid = [NSString stringWithFormat:@"%ld",[[WalkingScoreData allObjects] count]];
        scoreData.index = (int)[[WalkingScoreData allObjects] count];
        scoreData.date = date;
//        scoreData.soundWalkingScore = 0;
     
        for(int i=0; i<datas.count; i++){
            if(i == 0)
                scoreData.bodyBalanceScore1 = [datas[0] intValue];
            else if(i == 1)
                scoreData.bodyBalanceScore2 = [datas[1] intValue];
            else if(i == 2)
                scoreData.bodyBalanceScore3 = [datas[2] intValue]; // crash issue
        }
        
        [realm addObject:scoreData];
    }
    
    [realm commitWriteTransaction];
    
    NSLog(@"BalanceScoreData : %@", [WalkingScoreData allObjects]);
}

- (void)addPedoDataToDB:(int)data date:(NSDate *)date{
    
    NSDate *today = date;
    NSDate *tomorrow = [today dateByAddingDays:1];
    
    NSLog(@"toda : %@ , tomo : %@", today, tomorrow);
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", today, tomorrow];
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    NSLog(@"result :%@", result);
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    
    if(result.count){
        WalkingScoreData *old = result[0];
        
        WalkingScoreData *new = [WalkingScoreData new];
        new.index = old.index;
        new.uid = old.uid;
        new.date = old.date;
        new.soundWalkingScore = old.soundWalkingScore;
        new.bodyBalanceScore1 = old.bodyBalanceScore1;
        new.bodyBalanceScore2 = old.bodyBalanceScore2;
        new.bodyBalanceScore3 = old.bodyBalanceScore3;
        new.pedoCountForDay = data;
        
        [WalkingScoreData createOrUpdateInRealm:realm withValue:new];
    }
    else{
        WalkingScoreData *new = [WalkingScoreData new];
        new.pedoCountForDay = data;
        new.uid = [NSString stringWithFormat:@"%ld",[[WalkingScoreData allObjects] count]];
        new.index = (int)[[WalkingScoreData allObjects] count];
        new.date = date;
        
        [realm addObject:new];
    }
    
    NSLog(@"Today Result : %@", [WalkingScoreData objectsWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", today, tomorrow]]);
    
    NSLog(@"pedoData : %@", [WalkingScoreData allObjects]);
  
    [realm commitWriteTransaction];
}

- (void)saveScoreToDB:(id)data{
    
    NSInteger dateIndex = 0;
    
    NSDate *date = [NSDate date];
    NSDate *today = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
    NSDate *tomorrow = [today dateByAddingDays:1];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", today, tomorrow];
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    
    if(result.count){
        WalkingScoreData *old = result[0];
        
        
        WalkingScoreData *scoreData = [WalkingScoreData new];
        scoreData.index = old.index;
        scoreData.uid = old.uid;
        scoreData.date = old.date;
        scoreData.soundWalkingScore = [data[0] intValue];
        scoreData.bodyBalanceScore1 = [data[1] intValue];
        scoreData.bodyBalanceScore2 = [data[2] intValue];
        scoreData.bodyBalanceScore3 = [data[3] intValue];
        
        [WalkingScoreData createOrUpdateInRealm:realm withValue:scoreData];
    }
    else{
        WalkingScoreData *scoreData = [WalkingScoreData new];
        scoreData.soundWalkingScore = 0;
        scoreData.uid = [NSString stringWithFormat:@"%ld",[[WalkingScoreData allObjects] count]];
        scoreData.index = [[WalkingScoreData allObjects] count];
        scoreData.date = date;
        scoreData.soundWalkingScore = [data[0] intValue];
        scoreData.bodyBalanceScore1 = [data[1] intValue];
        scoreData.bodyBalanceScore2 = [data[2] intValue];
        scoreData.bodyBalanceScore3 = [data[3] intValue];
        
        [realm addObject:scoreData];
    }
    
    [realm commitWriteTransaction];
    
//    NSLog(@"WalkingScoreData : %@", [WalkingScoreData allObjects]);
}

- (float)calculatePedoScore:(NSPredicate *)pred{
    
    NSInteger pedoScoreSum = 0;
    
    NSInteger dayLength = 0;
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];

    if(result.count){
        pedoScoreSum = [[result sumOfProperty:@"pedoCountForDay"] integerValue];
        NSLog(@"pedoScoreSum : %ld", pedoScoreSum);
        
        // 있는 날짜까지만 평균 내기
        
        dayLength  = result.count;
        NSLog(@"dayLength : %ld", dayLength);
        
        return ((float)pedoScoreSum / (dayLength * MAX_STEP_PER_DAY)) * 100;
//        return ((float)pedoScoreSum / dayLength) / 100.0;
    }
    else{
        return 0;
    }
}


- (NSInteger)calculateWalkingScore:(NSPredicate *)pred{
    
    NSInteger walkingScoreSum = 0;
    NSInteger dayLength = 0;
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
//    NSLog(@"result:%ld , :%@",result.count, result);
    
    if(result.count){
        walkingScoreSum = [[result sumOfProperty:@"soundWalkingScore"] integerValue];
        NSLog(@"walkingScoreSum : %ld", walkingScoreSum);
        
    // 있는 날짜까지만 평균 내기

       dayLength  = result.count;
    
        return walkingScoreSum / dayLength;
    }
    else{
        return 0;
    }
//    NSLog(@"akakak : %ld , daylength : %ld",walkingScoreSum, dayLength);
//    
//    return walkingScoreSum / dayLength;
//    return walkingScoreSum;
}

- (float)calculateBalanceScore:(NSPredicate *)pred{
    
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    
    NSInteger scoreSum1 = 0;
    NSInteger scoreSum2 = 0;
    NSInteger scoreSum3 = 0;
    
    float sumResult = 0;
    
    if(result.count){
       
        int balanceIndex = (int)result.count - 1;
        
        for(int i = 0; i < result.count; i++){
            
            WalkingScoreData *data = result[balanceIndex - i];
            
            scoreSum1 = data.bodyBalanceScore1;
            scoreSum2 = data.bodyBalanceScore2;
            scoreSum3 = data.bodyBalanceScore3;
            
            sumResult = (float)(scoreSum1 + scoreSum2 + scoreSum3) / 3;
            
            if(sumResult > 0.1){
                break;
            }
        }
    }
    
    NSLog(@"sum1 :%ld , 2:%ld , 3:%ld, result:%f", scoreSum1,scoreSum2,scoreSum3,sumResult);
 
    return sumResult;
}

- (id)loadData:(NSPredicate *)pred{
    
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    return result[0];
}

- (id)getBalanceScore:(NSPredicate *)pred{
    
    RLMResults *result = [self returnAllObjects:pred];
    NSArray *scores;
    if(result.count){
     
        WalkingScoreData *score = result[0];
        
        int balanceSum = (score.bodyBalanceScore1 + score.bodyBalanceScore2 + score.bodyBalanceScore3) / 3;
        
        if(balanceSum > 0)
            scores = @[@(score.bodyBalanceScore1),@(score.bodyBalanceScore2),@(score.bodyBalanceScore3)];
        else
            score = nil;
    }
    
    return scores;
    
}

- (id)getLastBalanceScore{
    RLMResults *result = [WalkingScoreData allObjects];
    
    NSArray *scores;
    
//    for(WalkingScoreData *walking in result){
//        
//    }
    
    NSInteger index = result.count;
 
    for(int i=0; i< result.count; i++){
        WalkingScoreData *score = result[index - (i+1)];
       
        int balanceSum = (score.bodyBalanceScore1 + score.bodyBalanceScore2 + score.bodyBalanceScore3) / 3;
      
        if(balanceSum > 0){
            scores = @[@(score.bodyBalanceScore1),@(score.bodyBalanceScore2),@(score.bodyBalanceScore3)];
            
            break;
        }
    }
    return scores;
}

- (id)returnAllObjects:(NSPredicate *)pred{
    
    return [WalkingScoreData objectsWithPredicate:pred];
}

- (NSInteger)getSoundWalkingScore:(NSPredicate *)pred{

    NSInteger score = 0;
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    NSLog(@"result : %@", result);
    if(result.count){
        WalkingScoreData *walkingData = result[0];
        
        score = walkingData.soundWalkingScore;
    }
    
    return score;
}

- (NSArray *)getWeeklySoundWalkingScore:(NSDate *)date{
    
    NSDate *weekStart = [date dateWeekStart];
    NSDate *weekStart2 = [GeneralUtil startOfTheDay:weekStart];
    
    NSDate *weekEnd = [date dateWeekEnd];
    NSDate *weekEnd2 = [GeneralUtil startOfTheDay:weekEnd];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", weekStart2, weekEnd2];
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    
    NSMutableArray *walkings = [NSMutableArray new];
    
    if(result.count){
        for(WalkingScoreData *data in result){
            [walkings addObject:@(data.soundWalkingScore)];
        }
    }
    
    
    NSLog(@"new2 : %@, week s : %@ , e : %@", date, weekStart2, weekEnd2);
    
    NSLog(@"result : %@",result);

    return (NSArray *)walkings;
}

- (NSArray *)getWeeklyBalanceScore:(NSDate *)date{
    
    NSDate *weekStart = [date dateWeekStart];
    NSDate *weekStart2 = [GeneralUtil startOfTheDay:weekStart];
    
    NSDate *weekEnd = [date dateWeekEnd];
    NSDate *weekEnd2 = [GeneralUtil startOfTheDay:weekEnd];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", weekStart2, weekEnd2];
    RLMResults *result = [WalkingScoreData objectsWithPredicate:pred];
    
    NSMutableArray *balances = [NSMutableArray new];
    
    if(result.count){
        for(WalkingScoreData *data in result){
            NSInteger per = (data.bodyBalanceScore1 + data.bodyBalanceScore2 + data.bodyBalanceScore3) / 3; // 평균 점수 

            [balances addObject:@(per)];
        }
    }
    
    
    NSLog(@"new2 : %@, week s : %@ , e : %@", date, weekStart2, weekEnd2);
    
    NSLog(@"result : %@",result);
    
    return (NSArray *)balances;
}



- (void)copyData{
    
    RLMResults * result = [WalkingScoreData allObjects];
    WalkingScoreData *old = [result firstObject];
    NSLog(@"old : %@", old);
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];

//    for(int i=0; i< 30; i++){
//        WalkingScoreData *data = [WalkingScoreData new];
//        
//        data.uid = [NSString stringWithFormat:@"%ld",old.index + i+1];
//        data.date  = [old.date dateByAddingDays:i+1];
//        data.soundWalkingScore = old.soundWalkingScore;
//        data.bodyBalanceScore1 = old.bodyBalanceScore1;
//        data.bodyBalanceScore2 = old.bodyBalanceScore2;
//        data.bodyBalanceScore3 = old.bodyBalanceScore3;
//        
//        [realm addObject:data];
//    }
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"index >2"];
    RLMResults *a = [WalkingScoreData objectsWithPredicate:pred];
    
    for(WalkingScoreData *b in a){
        
        WalkingScoreData *scoreData = [WalkingScoreData new];

        scoreData.date = b.date;
        scoreData.uid = b.uid;
        scoreData.bodyBalanceScore1 = old.bodyBalanceScore1;
        scoreData.bodyBalanceScore2 = old.bodyBalanceScore2;
        scoreData.bodyBalanceScore3 = old.bodyBalanceScore3;
        
        [WalkingScoreData createOrUpdateInRealm:realm withValue:b];
    }
        
    
    [realm commitWriteTransaction];
}

- (void)reset{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *result = [WalkingScoreData allObjects];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:result ];
    [realm commitWriteTransaction];

}

@end
