//
//  NotifyHandler.m
//  Arki
//
//  Created by ken on 2015. 6. 24..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "NotifyHandler.h"

@implementation NotifyHandler

+ (void)setZiktoDayNotification:(NSDate *)ziktoDay{

    NSLog(@"ziktoDay : %@", ziktoDay);
    
    // clear all notification when reschedule
    
    [self cancelAllNotifications];
    
    UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    UILocalNotification *notification = [UILocalNotification new];
    
//    NSInteger repeatInterval = NSCalendarUnitWeekOfYear; // weekly
    NSInteger repeatInterval = NSCalendarUnitDay; // hourly

    if (notification)
    {
        {
            notification.fireDate = ziktoDay;
            notification.timeZone = [NSTimeZone localTimeZone];
            notification.repeatInterval = repeatInterval;
            
            NSLog(@"fireDate : %@", notification.fireDate);
            
//            switch (repeatInterval) {
//                case 0:
//                    notification.repeatInterval = NSCalendarUnitDay;
//                    break;
//                case 1:
//                    notification.repeatInterval = NSCalendarUnitWeekOfYear;
//                    break;
//                case 2:
//                    notification.repeatInterval = NSCalendarUnitYear;
//                    break;
//                default:
//                    notification.repeatInterval = 0;
//                    break;
//            }
        }
       
       notification.alertBody = NSLocalizedString(@"notify_ziktoday", @"Today is the Zikto day !");
       
//        if (allowsBadge)
//        {
//            notification.applicationIconBadgeNumber = 1;
//        }
//        if (allowsSound)
//        {
//            notification.soundName = UILocalNotificationDefaultSoundName;
//        }
        
        // this will schedule the notification to fire at the fire date
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        // this will fire the notification right away, it will still also fire at the date we set
//        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

+ (void)cancelAllNotifications{
    
    NSLog(@"");
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
@end
