//
//  DayBandHandler.m
//  Arki
//
//  Created by ken on 2015. 6. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "DayBandHandler.h"
#import <Realm/realm/Realm.h>
#import "DayBandData.h"

@implementation DayBandHandler

+ (void)updateData:(NSArray *)allData{

    NSDate *startOfToday = [GeneralUtil startOfToday:0];
    NSLog(@"startToday : %@", startOfToday);
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    
    int index = 0;
    
    for(NSArray *hour in allData){
        
        NSDate *now = [[startOfToday dateBySettingHour:index minute:0 second:0] toLocalTime];
        NSDate *after = [[startOfToday dateBySettingHour:index+1 minute:0 second:0] toLocalTime];
        
        NSLog(@"now : %@ , after : %@", now, after);
        
        RLMResults *result = [self getDataWithPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", now, after]];
        
        if(result.count){
            
            DayBandData *old = result[0];
            
            DayBandData *new = [DayBandData new];
            new.index = old.index;
            new.uid = old.uid;
            new.totalPedoCount = old.totalPedoCount;
            new.activityType = old.activityType;
            new.date = old.date;

            [DayBandData createOrUpdateInRealm:realm withValue:new];
        }
        else{
            DayBandData *new = [DayBandData new];
            new.index = [[DayBandData allObjects] count];
            new.uid = [NSString stringWithFormat:@"%ld",new.index];
            new.totalPedoCount = [[hour firstObject] integerValue];
            new.activityType = [[hour lastObject] integerValue];
                        
            new.date = now;
            
            [realm addObject:new];
        }
        index ++;
    }
    
    [realm commitWriteTransaction];
    
//    NSLog(@"DayBandData  :%ld", [DayBandData allObjects].count);
//    
//    for(DayBandData *aa in [DayBandData allObjects])
//        NSLog(@"aa : %@", aa);
}

+ (id)getDataWithPredicate:(NSPredicate *)pred{
    
    RLMResults *result = [DayBandData objectsWithPredicate:pred];
    return result;
}

+ (void)removeAllData{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *result = [DayBandData allObjects];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:result ];
    [realm commitWriteTransaction];
    NSLog(@"result.count : %ld", result.count);
}

@end
