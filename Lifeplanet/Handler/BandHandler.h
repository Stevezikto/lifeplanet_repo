//
//  BandHandler.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 11..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import <Parse/Parse.h>
#import "ArkiBandData.h"

@interface BandHandler : NSObject{
    // OTA관련
    
    uint8_t convertSendData[20];
    uint8_t tempData01[20], tempData02[20];
    
    int m_FW_NewVersion[3];
    int m_FW_CurVersion[3];
    //    uint8_t mFileBuffer[FILE_BUFFER_SIZE];
    uint8_t *mFileBuffer;
    NSData *subData;
    
    int mi_NewImgSize;
    int mi_NewImgType;
    int mi_NewImg_AppType;
    int mi_CurImg_AppType;
    
    int mi_CurImgType;
    int mi_CurImgSize;
    
    int mi_Sequence;
    int mi_CurFrame;
    int mi_TotalFrame;
    int mi_CurByte;
    int mi_PrePercent;
    
    bool mb_Thread;
    bool mb_OTA_Success;
    
    bool mb_OTA_Write;				// OTA에서 현재 패킷을 보냈으니 다음 패킷 보내기 위한 변수
    bool mb_OTA_LimitWrite;
    
    bool mb_OTAMode;
    long ml_limitLength;
    
    int mi_byteIndex;
}

@property RLMRealm *realm;
@property(strong, nonatomic) NSThread *OTA_Thread;
@property(strong, nonatomic) NSMutableArray *test;
+ (BandHandler *)sharedInstance;
- (void)saveDeviceUUID:(NSString *)uuid;
- (NSString *)loadDeviceUUID;
- (void)saveDeviceScore:(NSData *)score;
- (NSData *)loadDeviceScore;
- (void)resetDB;
- (void)updateProperty:(NSDictionary *)property; // 특정 프로퍼티를 업데이트 함
- (void)updateUid; // uid 추가용 임시 메소드
- (void)sendRawData:(NSData *)data; // Server로 로우데이터 보내는 메소드
- (void)getRawData:(NSDate *)date; // Server에서 로우 데이터 받음

- (id)returnHourData:(NSArray *)originalData range:(int)range;
- (id)returnMinutedData:(NSArray *)minuteData index:(int)index;
- (id)returnPedoData:(NSArray *)hourData index:(int)pedoIndex;
- (NSInteger)returnBandData:(NSPredicate *)pred property:(NSInteger)property; // 프로퍼티에 대한 값 반환

- (id )parsePerHour:(RLMResults *)all;
- (id)parsePerMinute:(id)hours;
- (NSArray *)getDataForDay:(NSPredicate *)pred;

#pragma mark - Walking
- (float)calculateOneWalkingScore:(NSPredicate *)pred;
- (float)calculateWalkingScore:(NSPredicate *)pred;

#pragma mark - SoundWalking
- (id)parseByHour:(NSArray *)aDay activityType:(int)mActivityType;
- (id)parseByHourInDate:(RLMResults *)allSleeps; // 하루 동안의 데이터를 한시간 단위로 나눈 값
- (id)getSoundWalking:(RLMResults *)result;
- (id)fetchSoundWalking:(RLMResults *)all firstInde:(int)start lastIndex:(int)end;

- (RLMResults *)parseSleepPerMinute:(RLMResults *)hours; // DB에서 슬립 데이터만 추출해서 넘김
- (int)returnPedoCount:(NSPredicate *)pred; // 조건 범위에 따라 pedo 반환
//- (NSInteger)calculateWalkingScore:(NSPredicate *)pred;

//- (NSDate *)syncedDate:(NSPredicate *)pred; // 싱크한 날짜 받기
- (id)returnQueryData:(NSPredicate *)pred; // 특정 조건에 맞는 데이터 반환
- (id)parseData:(NSArray *)data type:(int)type date:(NSDate *)date; // 마스킹 된 데이터를 밴드 스키마에 맞게 저장
- (void)saveToDb:(id)data; // 로컬 디비에 저장
- (void)makeFakeData:(NSInteger)length; // 하루치에 부족한 만큼 가라 데이터 생성
- (void)saveDataToDB:(NSArray *)data type:(int)type date:(NSDate *)date; // 마스킹 된 값 디비에 저장

#pragma mark - Sleep
- (NSInteger)getSleepScore:(NSDate *)date;// 슬립 점수를 자체적으로 계산해서 반환
- (id)parseSleepData:(NSPredicate *)pred;
//- (id)getStartEndDate:(RLMResults *)result;
- (id)getStartEndDate:(RLMResults *)result theDay:(NSDate *)theDay;
//- (id)getSleepDiffCount:(NSPredicate *)pred;
- (id)getSleepDiffCount:(NSPredicate *)pred theDay:(NSDate *)theDay;
- (NSInteger)getSleepHour:(RLMResults *)result theDay:(NSDate *)theDay;
- (RLMResults *)getDataWithPredicate:(NSPredicate *)pred;
- (NSString *)getSleepTime:(NSDate *)startDate; // Daily footer에서 사용하는 수면 시간 반환
#pragma mark - Reset

- (void)removeObjectWithPredicate:(NSPredicate *)pred;
- (void)removeObject:(id)band;

#pragma mark - Server Sync
- (void)getRawDataFromServer;



@end
