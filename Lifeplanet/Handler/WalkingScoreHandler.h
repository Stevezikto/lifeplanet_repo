//
//  WalkingScoreHandler.h
//  Arki
//
//  Created by ken on 2015. 5. 20..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WalkingScoreHandler : NSObject
+ (WalkingScoreHandler *)sharedInstance;
- (void)addSoundWalkingDataToDB:(int)data withDate:(NSDate *)date; // Int Data를 WalkingScoreData에 저장
- (void)addBalanceDataToDB:(id)data withDate:(NSDate *)date; // 객체 Data를 WalkingScoreData에 저장
- (void)addPedoDataToDB:(int)data date:(NSDate *)date;
- (id)loadData:(NSPredicate *)pred; // 조건에 맞는 데이터 반환
- (void)addGaitDataToDB:(id)data;
- (void)saveScoreToDB:(id)data; // Sync 후 Score 업데이트
- (id)returnAllObjects:(NSPredicate *)pred; //
- (void)reset;

#pragma mark -
- (id)getBalanceScore:(NSPredicate *)pred;
- (id)getLastBalanceScore;
- (float)calculatePedoScore:(NSPredicate *)pred;                // PedoCount 
- (NSInteger)calculateWalkingScore:(NSPredicate *)pred;         // soundwalking Score 1주일치 분량 반환
- (float)calculateBalanceScore:(NSPredicate *)pred;             // balance score 1주일치 분량 반환
- (NSInteger)getSoundWalkingScore:(NSPredicate *)pred;          // soundwalking score 반환
- (NSArray *)getWeeklySoundWalkingScore:(NSDate *)date;         // 일주일치 사운드 워킹 스코어
- (NSArray *)getWeeklyBalanceScore:(NSDate *)date;              // 일주일치 밸런스 스코어
- (void)copyData;
@end
