//
//  MissionHandler.h
//  Arki
//
//  Created by ken on 2015. 6. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MissionHandler : NSObject

+ (void)saveMission:(NSString *)mission clear:(BOOL)isClear;
+ (NSArray *)getWeekData:(NSDate *)date;

@end
