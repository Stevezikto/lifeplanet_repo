//
//  UserHandler.h
//  Arki
//
//  Created by ken on 2015. 4. 14..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm/Realm.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "UserData.h"

@interface UserHandler : NSObject

+ (void)removeAllUserData;
+ (void)createNewUser:(PFUser *)parameters complete:(void (^)(BOOL complete))complete;
+ (void)updateUser:(PFUser *)pfUser;
+ (UserData *)fetchData:(void (^)(BOOL complete))complete;
+ (void)fetchUserData:(NSPredicate *)pred complete:(void (^)(BOOL complete, UserData* user))complete;
+ (int)fetchUserCoachingLevel;
+ (void)hasUserImageFile:(PFUser *)pfUser;
+ (void)saveUserData:(NSDictionary *)user complete:(void (^)(BOOL complete))complete;
+ (void)storeDeviceUUID:(NSString *)uuid;
+ (void)getDeviceUUID:(PFUser *)user callback:(void(^)(id uuid))callback;
//+ (UserData *)getCurrentUser:(NSPredicate *)pred; // 현재 로그인한 사용자 정보 획득
+ (void)getCurrentUser:(NSPredicate *)pred complete:(void (^)(UserData *mUser))success;
@end
