//
//  DayBandHandler.h
//  Arki
//
//  Created by ken on 2015. 6. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "RLMObject.h"

@interface DayBandHandler : RLMObject

+ (void)updateData:(NSArray *)allData;
+ (void)removeAllData;

@end
