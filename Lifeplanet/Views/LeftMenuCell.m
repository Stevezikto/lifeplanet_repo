//
//  LeftMenuCell.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 16..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "LeftMenuCell.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
    if (selected) {
        self.textLabel.textColor = [UIColor colorWithRed:81.0 / 255.0 green:188.5 / 255.0 blue:225.0 / 255.0 alpha:1.0];
    } else {
        self.textLabel.textColor = [UIColor darkGrayColor];
    }
}


- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    
}

@end
