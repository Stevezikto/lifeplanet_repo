//
//  ErrorType.h
//  Arki
//
//  Created by ken on 2015. 3. 26..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ErrorType) {
    ErrorTypeMissingEmail,
    ErrorTypeMissingPassword,
    ErrorTypeMissingPasswordConform,
    ErrorTypeAccountNotMatched,
    ErrorTypePasswordNotMatched,
    ErrorTypeAlreadySignedUp,
    ErrorTypeLessPassword,
    ErrorTypeMissingAlphabetAt,
    ErrorTypePasswordLength,
};
@interface CommonErrorType : NSObject

+ (void)showErrorWithType:(NSInteger)type;

@end
