
//
//  CustomColor.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 20..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEFAULT_YELLOW          [UIColor colorWithRed:255.0 / 255.0 green:218.0 / 255.0 blue:85.0 / 255.0 alpha:1.0f]
#define DEFAULT_BLUE            [UIColor colorWithRed:81.0 / 255.0 green:197.0 / 255.0 blue:231.0 / 255.0 alpha:1.0f]
#define DEFAULT_GREEN           [UIColor colorWithRed:143.0 / 255.0 green:223.0 / 255.0 blue:211.0 / 255.0 alpha:1.0f]
#define DEFAULT_RED             [UIColor colorWithRed:243.0 / 255.0 green:89.0 / 255.0 blue:103.0 / 255.0 alpha:1.0f]

#define PedoNavy                [UIColor colorWithRed:16.0 / 255.0 green:78.0 / 255.0 blue:139.0 / 255.0 alpha:1.0f]
#define DEFAULT_NAVY            [UIColor colorWithRed:54.0 / 255.0 green:75.0 / 255.0 blue:114.0 / 255.0 alpha:1.0f]
#define CIRCLE_WHITE            [UIColor colorWithRed:216.0 / 255.0 green:215.0 / 255.0 blue:209.0 / 255.0 alpha:1.0]

#define PedoBlue                [UIColor colorWithRed:24.0 / 255.0 green:116.0 / 255.0 blue:205.0 / 255.0 alpha:1.0f]
#define PedoSkyBlue             [UIColor colorWithRed:0 / 255.0 green:191.0 / 255.0 blue:255.0 / 255.0 alpha:1.0f]

#define PedoDarkNavy            [UIColor colorWithRed:0 / 255.0 green:58.0 / 255.0 blue:109.0 / 255.0 alpha:1.0f]
#define PedoDarkBlue            [UIColor colorWithRed:10.0 / 255.0 green:86.0 / 255.0 blue:175.0 / 255.0 alpha:1.0f]
#define PedoDarkSkyBlue         [UIColor colorWithRed:0 / 255.0 green:171.0 / 255.0 blue:225.0 / 255.0 alpha:1.0f]

#define ACTIVITY_HIGH           [UIColor colorWithRed:11.0 / 255.0 green:134.0 / 255.0 blue:122.0 / 255.0 alpha:1.0f]
#define ACTIVITY_MEDIUM         [UIColor colorWithRed:49.0 / 255.0 green:195.0 / 255.0 blue:175.0 / 255.0 alpha:1.0f]
#define ACTIVITY_LOW            [UIColor colorWithRed:84 / 255.0 green:164.0 / 255.0 blue:149.0 / 255.0 alpha:1.0f]
#define ACTIVITY_SLEEP          [UIColor colorWithRed:255.0 / 255.0 green:255.0 / 255.0 blue:255.0 / 255.0 alpha:1.0f]

#define ACTIVITY_HIGH_DARK      [UIColor colorWithRed:9.0 / 255.0 green:117.0 / 255.0 blue:107.0 / 255.0 alpha:1.0f]
#define ACTIVITY_MEDIUM_DARK    [UIColor colorWithRed:43.0 / 255.0 green:169.0 / 255.0 blue:152.0 / 255.0 alpha:1.0f]
#define ACTIVITY_LOW_DARK       [UIColor colorWithRed:76.0 / 255.0 green:147.0 / 255.0 blue:133.0 / 255.0 alpha:1.0f]
#define ACTIVITY_SLEEP_DARK     [UIColor colorWithRed:226.0 / 255.0 green:226.0 / 255.0 blue:226.0 / 255.0 alpha:1.0f]

#define ARDarkNavy              [UIColor colorWithRed:0 / 255.0 green:58.0 / 255.0 blue:109.0 / 255.0 alpha:1.0f]
#define ARDarkBlue              [UIColor colorWithRed:10.0 / 255.0 green:86.0 / 255.0 blue:175.0 / 255.0 alpha:1.0f]
#define ARDarkSkyBlue           [UIColor colorWithRed:0 / 255.0 green:171.0 / 255.0 blue:225.0 / 255.0 alpha:1.0f]

#define TREND_STEP              [UIColor colorWithRed:11.0 / 255.0 green:134.0 / 255.0 blue:122.0 / 255.0 alpha:1.0f]
#define TREND_WALKING           [UIColor colorWithRed:49.0 / 255.0 green:195.0 / 255.0 blue:175.0 / 255.0 alpha:1.0f]
#define TREND_BALANCE           [UIColor colorWithRed:255.0 / 255.0 green:255.0 / 255.0 blue:255.0 / 255.0 alpha:1.0f]

#define BODY_COLOR_RED          [UIColor colorWithRed:255.0 / 255.0 green:119.0 / 255.0 blue:131.0 / 255.0 alpha:1.0f]
#define BODY_COLOR_YELLOW       [UIColor colorWithRed:255.0 / 255.0 green:214.0 / 255.0 blue:66.0 / 255.0 alpha:1.0f]
#define BODY_COLOR_GREEN        [UIColor colorWithRed:163.0 / 255.0 green:212.0 / 255.0 blue:29.0 / 255.0 alpha:1.0f]

#define SleepLine               [UIColor colorWithRed:138.0 / 255.0 green:180.0 / 255.0 blue:224.0 / 255.0 alpha:1.0f]
@interface CustomColor : NSObject

+ (UIImage *)imageFromColor:(UIColor *)color;

@end
