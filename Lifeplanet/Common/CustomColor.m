//
//  CustomColor.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 20..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "CustomColor.h"

@implementation CustomColor

+ (UIImage *)imageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

@end
