//
//  ErrorType.m
//  Arki
//
//  Created by ken on 2015. 3. 26..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "CommonErrorType.h"

@implementation CommonErrorType

/*
 // error
 "email" = "Email";
 "username" = "USername";
 "password" = "Password";
 "password_again" = "Password Again";
 "forgot_password" = "Forgot password";
 "re_enter_password" = "Re-enter password";
 "empty_email" = "Please enter your email";
 "empty_password" = "Please enter your password";
 "error_match" = "The email and password you entered don\'t match";
 "error_login_failed" = "Could not log in, please try again later";
 "facebook_login_fail" = "Facebook login failed";
 "error_password_length" = "Password must be at least 6 character long";
 "error_email_already" = "Email is already taken, please choose a different one";
 "error_re_enter_password" = "Please re-enter your password";
"error_invalid_email" = "Email is invalid, please correct it and try again";
 "error_invalid_password" = "Password don't match, pleast try again";
 */
+ (void)showErrorWithType:(NSInteger)type{
    
    switch (type) {
        case ErrorTypeMissingEmail:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"empty_email", @"Please enter your email") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypeMissingPassword:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"empty_password", @"Please enter your password") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypeMissingPasswordConform:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_re_enter_password", @"Please re-enter your password") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypeAccountNotMatched:
             [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_match", @"The email and password you entered don't match") cancel:NSLocalizedString(@"ok", @"Ok")
                           delegate:nil others:nil];
            break;
        case ErrorTypePasswordNotMatched:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_invalid_password", @"Password don't match, pleast try again") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypeAlreadySignedUp:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_email_already", @"Email is already taken, please choose a different one") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypeLessPassword:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_password_length", @"Password must be at least 6 character long") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypeMissingAlphabetAt:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_invalid_email", @"Email is invalid, please correct it and try again") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        case ErrorTypePasswordLength:
            [GeneralUtil showAlert:NSLocalizedString(@"error", @"Error") message:NSLocalizedString(@"error_password_length", @"Password must be at least 6 character long") cancel:NSLocalizedString(@"ok", @"Ok")];
            break;
        default:
            break;
    }
    return;
}

@end
