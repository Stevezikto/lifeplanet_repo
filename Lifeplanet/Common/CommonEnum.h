//
//  CommonEnum.h
//  Arki
//
//  Created by ken on 2015. 3. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, MenuType){
    MenuTypeMain = 2,
    MenuTypeSoundWalking = 0,
    MenuTypeBodyBalance = 1,
    MenuTypeAboutme = 3,
    MenuTypeSetting = 4,
    MenuTypeSleep = 6,
    MenuTypeGait = 5,
};

typedef NS_ENUM(NSInteger, WalkType) {
    WalkTypePower = 0,
    WalkTypeGood = 1,
    WalkTypeNormal = 2,
    WalkTypeBad = 3,
    WalkTypeEmpty = 4,
    WalkTypePocket = 5,
    WalkTypeEmpty2 = 6,
    WalkTypeSmartphone = 7,
};

typedef NS_ENUM(int, FaceBookLoginType) {
    FaceBookLoginTypeSignUp = 1,
    FaceBookLoginTypeSignIn = 2,
};

typedef NS_ENUM(NSInteger, WalkingType) {
    WalkingTypepPower = 0,
    WalkingTypeHealth = 1,
    WalkingTypeNormal = 2,
    WalkingTypeBad = 3,
    WalkingTypeInPocket = 5,
    WalkingTypePhone = 7,
};

typedef NS_ENUM(int, GenderType) {
    GenderTypeMale = 1,
    GenderTypeFemale = 2,
};

typedef NS_ENUM(int, CategoryType) {
    CategoryTypeWalk =     1,
    CategoryTypeSleep =    2,
    CategoryTypeActivity = 7,
};

typedef NS_ENUM(NSInteger, BLEType) {
    BLETypeDisconnect                   = 0,
    BLETypeStart                        = 1,
    BLETypeStartTime                    = 2,
    BLETypeStartEnd                     = 3,
    BLETypeGaitStart                    = 4,
    BLETypeGaitStartResponse            = 5,
    BLETypeGaitRequest                  = 6,
    BLETypeGaitRequestResponse          = 7,
    BLETypeDailySyncStart               = 8,
    BLETypeDailySyncResponse            = 9,
    BLETypeDailySyncEnd                 = 10,
    BLETypeOTAStart                     = 11,
    BLETypeErase                        = 12,
    BLETypeTimeResponse                 = 13,
    BLETypePedo                         = 14,
    BLETypeDailySyncStartFirst          = 15,
    BLETypeSyncEndFirst                 = 16,
    BLETypeTimeResponseFirst            = 17,
    BLETypeSoundWalkingStart            = 18,
    BLETypeBodyBalanceStart             = 19,
    BLETypeBodyBalanceResponse          = 20,
    BLETypeBodyBalanceTransfer          = 21,
    BLETypeBodyBalanceTransferResponse  = 22,
    BLETypeGaitWaiting                  = 23,
    BLETypeEraseAll                     = 24,
    BLETypeSoundWalkingResponse         = 25,
    BLETypePedoCommand                  = 26,
};

typedef NS_ENUM(NSInteger, BLEProtocol) {
    BLEProtocol700103,
    BLEProtocol7002Time,
    BLEProtocol700203,
    BLEProtocol700403,
    BLEProtocol700503,
    BLEProtocol710503,
    BLEProtocol71050003,
    BLEProtocol710903,
    BLEProtocol700603,
    BLEProtocol700703,
    BLEProtocol700803,
    BLEProtocol700903,
    BLEProtocol6003,
    BLEProtocol6103,
    BLEProtocolSyncDataStart,               // 60
    BLEProtocolSyncDataTransfer,            // 61
    BLEProtocolCommand,                     // 70
    BLEProtocolCommandTransfer,             // 71
    BLEProtocolFindMe,                      // 01
    BLEProtocolTimeSync,                    // 02
    BLEProtocolSyncReset,                   // 04
    BLEProtocolRegister,                    // 05
    BLEProtocolGetPedo,                     // 06
    BLEProtocolEraseAll,                    // 07
    BLEProtocolGetSoundWalkingScore,        // 08
    BLEProtocolGetBodyBalanceScore,         // 09
    BLEProtocolOTA,
    BLEProtocolOTATransfer,
    BLEProtocol5103,
    BLEProtocol5003,
    BLEProtocolError,
    
};
typedef NS_OPTIONS(NSInteger, CategoryType2) {
    CategoryType2Walk     = 1 << 0,
    CategoryType2Sleep    = 1 << 1,
    CategoryType2Activity = 1 << 2,
};
//index
//*date;
//isLeft;
//gaitId;
//ActivityNum;
//pedoCount;
//walkNum;
//accelxMean;
//zScore;
//impactScore;
//intervalScore
//yawRatio;
//gyroZ;
//diffCounter;
//sleepCounter;
typedef NS_ENUM(NSInteger, BandPropertyType) {
    BandPropertyTypeIndex           = 0,
    BandPropertyTypeDate            = 1,
    BandPropertyTypeIsLeft          = 2,
    BandPropertyTypeGaitID          = 3,
    BandPropertyTypeActivityNum     = 4,
    BandPropertyTypePedo            = 5,
    BandPropertyTypeWalkNum         = 6,
    BandPropertyTypeDiffCounter     = 7,
    BandPropertyTypeSleepCounter    = 8,
};

typedef NS_ENUM(NSInteger, SoundWalkingType) {
    SoundWalkingTypeExcellent   = 90,
    SoundWalkingTypeGreat       = 80,
    SoundWalkingTypeGood        = 70,
    SoundWalkingTypeOkay        = 60,
    SoundWalkingTypeBad         = 50,
    SoundWalkingTypeWarning     = 49,
};

typedef NS_ENUM(NSInteger, MissionType) {
    MissionTypeBalance,
    MissionTypeSleep,
    MissionTypeSoundWalking,
    MissionTypePedo,
};

typedef NS_ENUM(NSInteger, ZiktoDay) {
    ZiktoDaySunday              = 1,
    ZiktoDayMonday              = 2,
    ZiktoDayTuesday             = 3,
    ZiktoDayWednesday           = 4,
    ZiktoDayThursday            = 5,
    ZiktoDayFriday              = 6,
    ZiktoDaySaturday            = 7,
};