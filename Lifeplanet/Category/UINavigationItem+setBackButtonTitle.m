//
//  UINavigationItem+setBackButtonTitle.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 14..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UINavigationItem+setBackButtonTitle.h"

@implementation UINavigationItem (setBackButtonTitle)

+ (void)setBackButtonWithoutTitle:(UINavigationItem *)item{

    UIBarButtonItem *btn =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [item setBackBarButtonItem:btn];
}

@end
