//
//  UIScrollView+currentPage.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 8..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (currentPage)

- (NSInteger)currentPage:(UIScrollView *)scrollView;

@end
