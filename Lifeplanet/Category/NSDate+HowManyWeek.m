//
//  NSDate+HowManyWeek.m
//  Arki
//
//  Created by ken on 2015. 4. 7..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "NSDate+HowManyWeek.h"

@implementation NSDate (HowManyWeek)

+ (NSInteger)calcurateWeeksForMonth:(NSInteger)thisMonth day:(NSInteger)thisDay{
    
    NSInteger weekCount = 0;
//
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [currentCalendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSCalendarUnitWeekOfMonth fromDate:[NSDate date]];
    
//    NSInteger month = [components month];
//    NSInteger nextMonth = ([components month] +1);
    NSInteger someMonth = thisMonth;
    
    [components setMonth:someMonth];
//    [components setDay:thisDay];

    // 특정달을 받음
    NSDateComponents *someComponents = [currentCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSCalendarUnitWeekOfMonth)
                                                          fromDate:[currentCalendar dateFromComponents:components]];


    // 마지막 날짜가 며칠인지 받음
    NSRange daysRange = [currentCalendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:[currentCalendar dateFromComponents:someComponents]];
//    NSLog(@"Day Length : %ld", daysRange.length);

    // 15.5월 마지막 날이 5월의 몇 번째 주인지 받음
//    NSLog(@"Week Count:%ld", [someComponents weekOfMonth]);

    weekCount = daysRange.length;
   
    return weekCount;
}

+ (NSInteger)calcurateWeekIndexOfMonth:(NSInteger)thisMonth{
    
    NSInteger weekIndex = 0;
    
    // 특정달을 받음
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [currentCalendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSCalendarUnitWeekOfMonth fromDate:[NSDate date]];
    
    NSInteger someMonth = thisMonth;
    [components setMonth:someMonth];
    
    NSRange daysRange = [currentCalendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:[currentCalendar dateFromComponents:components]];
    [components setDay:daysRange.length];
    
    NSDateComponents *someComponents = [currentCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSCalendarUnitWeekOfMonth)
                                                          fromDate:[currentCalendar dateFromComponents:components]];

    
    weekIndex = [someComponents weekOfMonth];
//    NSLog(@"총 며칠: %ld", daysRange.length);
//    NSLog(@"총 몇주 : %ld", [someComponents weekOfMonth]);
    
    return weekIndex;
}

+ (NSInteger)returnStartDayOfWeek:(NSInteger)thisMonth{

    NSInteger startDay = 0;
    
    NSDate *currentDate  = [NSDate date];
//        NSCalendar *gregorianCalendar = [[NSCalendar alloc]  initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendar *currentCalendar = [[NSCalendar alloc]  initWithCalendarIdentifier:NSGregorianCalendar];
    [currentCalendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    NSDateComponents *components = [currentCalendar components:(NSYearCalendarUnit| NSMonthCalendarUnit
                                                                  | NSDayCalendarUnit| NSWeekdayCalendarUnit|NSWeekCalendarUnit)  fromDate:currentDate];
    
//    [components setMonth:thisMonth];

    
//    NSDateComponents *weekComponents = [currentCalendar components:(NSYearCalendarUnit| NSMonthCalendarUnit
//                                                                    | NSDayCalendarUnit| NSWeekdayCalendarUnit|NSWeekCalendarUnit) fromDate:[currentCalendar dateFromComponents:components]];
    NSDateComponents *weekComponents = [[NSDateComponents alloc] init];
    
    [weekComponents setWeekOfMonth:[components weekOfMonth]];
    [weekComponents setWeekday:2]; // 월요일
    [weekComponents setMonth:[components month]];
    [weekComponents setYear:[components year]];
    
    NSDate *monday = [currentCalendar dateFromComponents:weekComponents];
    startDay = [weekComponents weekday];
    
    NSLog(@"monday: %@", monday);
//    NSLog(@"startDay : %ld", startDay);
    
    return startDay;
}

+ (NSDate *)startOfWeek :(NSDate *)date{

    NSCalendar *gregorianCalendar = [[NSCalendar alloc]  initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorianCalendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDateComponents* components = [gregorianCalendar components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:date];
    [components setDay:([components day] - ([components weekday] - 1))];
    
    NSLog(@"시작요일 : %ld", [components weekday]);
    return [gregorianCalendar dateFromComponents:components];
}

+ (NSDate *)endOfWeek:(NSDate *)date {
    NSCalendar *gregorianCalendar = [[NSCalendar alloc]  initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorianCalendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDateComponents* components = [gregorianCalendar components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:date];
    
    [components setDay:([components day] + (7 - [components weekday]))];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    return [gregorianCalendar dateFromComponents:components];
}

+ (NSString *)convertYearMonthDayToString{

    // 오늘로 초기화
    NSDate *today = [NSDate date];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:currentLanguage]];
    [formatter setDateFormat:@"yyyy"];
    
    NSString *year = [formatter stringFromDate:today];
    [formatter setDateFormat:@"MM"];
    NSString *month = [formatter stringFromDate:today];
    [formatter setDateFormat:@"dd"];
    NSString *day = [formatter stringFromDate:today];
    
    NSString *dateString = [NSString stringWithFormat:@"%@.%@.%@",year,month,day];
//    [_btnCalendar setTitle:dateString forState:UIControlStateNormal];
    
    return dateString;
}

+ (NSString *)convertYearMonthDayToStringFromDate:(NSDate *)date{
    
    // 오늘로 초기화
    NSDate *today = date;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:currentLanguage]];
    [formatter setDateFormat:@"yyyy"];
    
    NSString *year = [formatter stringFromDate:today];
    [formatter setDateFormat:@"MM"];
    NSString *month = [formatter stringFromDate:today];
    [formatter setDateFormat:@"dd"];
    NSString *day = [formatter stringFromDate:today];
    
    NSString *dateString = [NSString stringWithFormat:@"%@.%@.%@",year,month,day];
    //    [_btnCalendar setTitle:dateString forState:UIControlStateNormal];
    
    return dateString;
}


- (NSDate*)toLocalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

- (NSDate*)toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

@end
