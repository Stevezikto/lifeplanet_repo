//
//  UILabel+UILabel_Extras.m
//  Arki
//
//  Created by ken on 2015. 6. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UILabel+UILabel_Extras.h"

@implementation UILabel (UILabel_Extras)

- (void)alignTop{
    CGSize fontSize = [self.text sizeWithAttributes:@{NSFontAttributeName:self.font}];
    double finalHeight = fontSize.height * self.numberOfLines;
    double finalWidth = self.frame.size.width;    //expected width of label
    CGRect rect = [self.text boundingRectWithSize:CGSizeMake(finalWidth, finalHeight) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:self.font} context:nil];
    CGSize theStringSize = rect.size;
    int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
    for(int i=0; i< newLinesToPad; i++)
        self.text = [self.text stringByAppendingString:@" \n"];
}

@end
