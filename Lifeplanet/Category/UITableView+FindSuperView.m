//
//  UITableView+FindSuperView.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 14..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UITableView+FindSuperView.h"

@implementation UITableView (FindSuperView)

- (NSIndexPath *)indexPathForCellContainingView:(UIView *)view {
    while (view != nil) {
        if ([view isKindOfClass:[UITableViewCell class]]) {
            return [self indexPathForCell:(UITableViewCell *)view];
        } else {
            view = [view superview];
        }
    }
    
    return nil;
}

@end
