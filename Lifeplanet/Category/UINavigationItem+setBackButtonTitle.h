//
//  UINavigationItem+setBackButtonTitle.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 14..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (setBackButtonTitle)

+ (void)setBackButtonWithoutTitle:(UINavigationItem *)item;

@end
