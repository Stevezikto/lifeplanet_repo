//
//  NSDate+HowManyWeek.h
//  Arki
//
//  Created by ken on 2015. 4. 7..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (HowManyWeek)

+ (NSInteger)calcurateWeeksForMonth:(NSInteger)thisMonth day:(NSInteger)thisDay;
+ (NSInteger)calcurateWeekIndexOfMonth:(NSInteger)thisMonth;
+ (NSInteger)returnStartDayOfWeek:(NSInteger)thisMonth;
+ (NSDate *)startOfWeek:(NSDate *)date;
+ (NSDate *)endOfWeek:(NSDate *)date;

+ (NSString *)convertYearMonthDayToString; // 오늘을 yyyyMMdd 포맷으로 된 스트링 반환
+ (NSString *)convertYearMonthDayToStringFromDate:(NSDate *)date;

- (NSDate*)toLocalTime;
- (NSDate*)toGlobalTime;
@end
