//
//  UILabel+UILabel_Extras.h
//  Arki
//
//  Created by ken on 2015. 6. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (UILabel_Extras)

- (void)alignTop;

@end
