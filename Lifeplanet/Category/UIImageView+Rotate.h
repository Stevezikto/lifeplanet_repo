//
//  UIImageView+Rotate.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 22..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageView (Rotate)
- (void)rotate360WithDuration:(CGFloat)duration repeatCount:(float)repeatCount;
- (void)pauseAnimations;
- (void)resumeAnimations;
- (void)stopAllAnimations;
@end