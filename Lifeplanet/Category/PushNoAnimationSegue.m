//
//  ReplaceSegue.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 12..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "PushNoAnimationSegue.h"


@implementation PushNoAnimationSegue

-(void) perform{
    [[[self sourceViewController] navigationController] pushViewController:[self   destinationViewController] animated:NO];
}

@end
