//
//  UIScrollView+currentPage.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 8..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UIScrollView+currentPage.h"

@implementation UIScrollView (currentPage)

- (NSInteger)currentPage:(UIScrollView *)scrollView{
    
    float fractionalPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    NSInteger page1 = lround(fractionalPage);
    
    return page1;
}

@end
