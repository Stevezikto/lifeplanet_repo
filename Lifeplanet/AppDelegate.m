//
//  AppDelegate.m
//  Lifeplanet
//
//  Created by KIM Seongho on 2015. 7. 9..
//  Copyright (c) 2015년 com.Zikto. All rights reserved.
//

#import "AppDelegate.h"
#import "SlideNavigationController.h"
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
//#import "MainViewController.h"
#import "SCFacebook.h"
//#import <CoreTelephony/CTCall.h>
//#import <CoreTelephony/CTCallCenter.h>
//#import <CoreTelephony/CTCarrier.h>
//#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "AFNetworkActivityIndicatorManager.h"
//#import "FitnessMovieViewController.h"

#import <Realm/Realm/Realm.h>
#import "UserData.h"
#import "ArkiBandData.h"
#import "WalkingScoreData.h"
#import "MissionData.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    [Parse setApplicationId:@"Ezjpj29C4KiWtFTjsALwfIowXO7MpKqP6WFrP6gZ"
                  clientKey:@"tCsqVHLqERpTfCIUz5Zh921eJnptGNG5cDAF90sg"];
    
//    //    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
//    [PFFacebookUtils initializeFacebook];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    // 좌,우 메뉴 버튼 설정 (여기에서 브레이크 포인트 걸림)
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)]; // 여기에서 브레이크 포인트 한번
    [button setImage:[UIImage imageNamed:@"menu_ico"] forState:UIControlStateNormal];
    [button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UIButton *button2  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [button2 setImage:[UIImage imageNamed:@"sync_ico"] forState:UIControlStateNormal];
    [button2 addTarget:[SlideNavigationController sharedInstance] action:@selector(touchRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button2];
    
    //    UIButton *rightButton2  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    //    [button2 setImage:[UIImage imageNamed:@"17_button_right"] forState:UIControlStateNormal];
    //    [button2 addTarget:[SlideNavigationController sharedInstance] action:@selector(touchRightMenu) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *rightBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:rightButton2];
    
    [SlideNavigationController sharedInstance].leftBarButtonItem = leftBarButtonItem;
    [SlideNavigationController sharedInstance].rightBarButtonItem = rightBarButtonItem;
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[mainStoryboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    UINavigationController *leftNavi = [[UINavigationController alloc] initWithRootViewController:leftMenu];
    [SlideNavigationController sharedInstance].leftMenu = leftNavi;
    [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
