//
//  UserData.h
//  Arki
//
//  Created by ken on 2015. 2. 25..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "RLMObject.h"

@interface UserData : RLMObject
@property NSString *uuid;
@property NSString *deviceUUID;
@property NSString *name;
@property int gender;
@property NSString *age;
//@property NSString *height;
//@property NSString *weight;
@property int height;
@property int weight;
@property NSString *imageUrl;
@property NSString *day;
@property int ziktoDay;
@property int stepCount;
@property int soundWalking;
@property int sleepTarget;
@property int coachingLevel;
@property BOOL isFeet;
@property BOOL isPound;

@end
