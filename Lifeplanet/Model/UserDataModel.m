//
//  UserDataModel.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 12..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UserDataModel.h"

@implementation UserDataModel

{
//    NSUserDefaults *defaults;
}

+ (UserDataModel *)sharedInstance
{
    static dispatch_once_t pred;
    static UserDataModel *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[UserDataModel alloc] init];
    });
    
    return view;
}

- (id)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

+ (void)setUserData:(NSDictionary *)parameters{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:parameters forKey:@"user_data"];
}

+ (NSDictionary *)getUserData{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"user_data"];
}

+ (BOOL)checkSignedUser{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults boolForKey:@"user_signed"];
}

+ (void)signedUser{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"user_signed"];
    
    [defaults synchronize];
}

+ (void)deleteSignedUser{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"user_signed"];
    [defaults synchronize];
}

+ (void)deleteUserData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"user_data"];
}

@end
