//
//  WalkingScoreData.h
//  Arki
//
//  Created by ken on 2015. 5. 20..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "RLMObject.h"

@interface WalkingScoreData : RLMObject
@property NSString *uid;
@property int soundWalkingScore;
@property int bodyBalanceScore1;
@property int bodyBalanceScore2;
@property int bodyBalanceScore3;
@property int pedoCountForDay;
@property int index;
@property NSDate *date;
@end
