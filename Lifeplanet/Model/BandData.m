//
//  BandData.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 4..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "BandData.h"

@implementation BandData

+ (BandData *)sharedInstance
{
    static dispatch_once_t pred;
    static BandData *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[BandData alloc] init];
    });
    
    return view;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _totalData = [NSMutableArray new];
        _walkData = [NSMutableArray new];
        _sleepData = [NSMutableArray new];
        _otherData = [NSMutableArray new];
    }
    return self;
}

- (void)appendWalkData:(NSArray *)data{
    
    for(int i=1; i<data.count; i++){
        NSNumber *value = data[i];
        NSNumber *originalValue = _walkData[i-0];
        
        int total = value.intValue + originalValue.intValue;
        
        [_walkData replaceObjectAtIndex:i-0 withObject:@(total)];
    }
}

@end
