//
//  UserDataModel.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 12..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataModel : NSObject

+ (UserDataModel *)sharedInstance;

+ (BOOL)checkSignedUser;
+ (void)signedUser;
+ (void)deleteSignedUser;
+ (void)setUserData:(NSDictionary *)parameters;
+ (NSDictionary *)getUserData;
+ (void)deleteUserData;


@end
