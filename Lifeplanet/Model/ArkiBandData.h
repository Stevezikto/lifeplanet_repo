//
//  ArkiBandData.h
//  Arki
//
//  Created by ken on 2015. 2. 17..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface ArkiWalkData : RLMObject
@property int categoryType;
@property int pedoCount;
@property int walkType;
@property int zScore;
@property int impactScore;
@property int intervalScore;
@property int yawRatio;
@property int gyroZAmplitude;


@end

RLM_ARRAY_TYPE(ArkiWalkData)

@interface ArkiSleepData : RLMObject

@property int categoryType;
@property int pedoCount;
@property int accelXMean;
@property int accelXMax;
@property int accelXMin;

@end

RLM_ARRAY_TYPE(ArkiSleepData)

@interface ArkiOtherData : RLMObject

@property int categoryType;
@property int pedoCount;
@property int aarInformation;
@property int accelXOffset;
@property int ftScore;

@end

RLM_ARRAY_TYPE(ArkiOtherData)

@interface ArkiBandData : RLMObject
//@property RLMArray<ArkiWalkData> *walks;
//@property RLMArray<ArkiSleepData> *sleeps;
//@property RLMArray<ArkiOtherData> *others;

@property NSString *uid;
@property long int index;
@property int totalPedoCount;
@property NSDate *date;
// walk
@property int leftRight;
@property int authen;
@property int Activity;
@property int pedoCount;
@property int walkNum;
@property int accelMag;
//@property int zScore;
//@property int impactScore;
//@property int intervalScore;
//@property int yawRatio;
//@property int gyroZ;
// sleep
@property int diffCounter;
//@property int sleepCounter;
// activity

@end

// This protocol enables typed collections. i.e.:
// RLMArray<ArkiBandData>
RLM_ARRAY_TYPE(ArkiBandData)

