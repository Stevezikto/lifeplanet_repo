//
//  DayBandData.h
//  Arki
//
//  Created by ken on 2015. 6. 18..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "RLMObject.h"

@interface DayBandData : RLMObject

@property NSString *uid;
@property NSInteger index;
@property NSDate *date;
@property NSInteger totalPedoCount;
@property NSInteger activityType;
@end
