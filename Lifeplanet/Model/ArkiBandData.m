//
//  ArkiBandData.m
//  Arki
//
//  Created by ken on 2015. 2. 17..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "ArkiBandData.h"

@implementation ArkiBandData

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

//+ (NSString *)primaryKey{
//    return @"id"
//}

+ (NSString *)primaryKey {
    return @"uid";
}

@end

@implementation ArkiWalkData

@end

@implementation ArkiSleepData

@end

@implementation ArkiOtherData

@end
