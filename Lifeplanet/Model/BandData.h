//
//  BandData.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 4..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BandData : NSObject

@property(strong, nonatomic)NSMutableArray *totalData;
@property(strong, nonatomic)NSMutableArray *walkData;
@property(strong, nonatomic)NSMutableArray *sleepData;
@property(strong, nonatomic)NSMutableArray *otherData;

+ (BandData *)sharedInstance;
- (void)appendWalkData:(NSArray *)data;

@end
