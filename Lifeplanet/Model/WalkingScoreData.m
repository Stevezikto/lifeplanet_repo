//
//  WalkingScoreData.m
//  Arki
//
//  Created by ken on 2015. 5. 20..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "WalkingScoreData.h"

@implementation WalkingScoreData
+ (NSString *)primaryKey {
    return @"uid";
}
@end
