//
//  UserData.m
//  Arki
//
//  Created by ken on 2015. 2. 25..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "UserData.h"

@implementation UserData

+ (NSString *)primaryKey {
    return @"uuid";
}

@end
