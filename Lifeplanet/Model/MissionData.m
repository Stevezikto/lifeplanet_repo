//
//  MissionData.m
//  Arki
//
//  Created by ken on 2015. 6. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "MissionData.h"

@implementation MissionData

+ (NSString *)primaryKey {
    return @"uid";
}

@end
