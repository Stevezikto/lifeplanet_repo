//
//  MissionData.h
//  Arki
//
//  Created by ken on 2015. 6. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "RLMObject.h"

@interface MissionData : RLMObject

@property NSString *uid;
@property NSInteger index;
@property NSDate *startWeek;
@property NSDate *endWeek;
@property NSString *balance1;
@property NSString *balance2;
@property NSString *balance3;
@property NSString *sleep1;
@property NSString *sleep2;
@property NSString *sleep3;
@property NSString *soundWalking1;
@property NSString *soundWalking2;
@property NSString *soundWalking3;
@property NSString *pedo1;
@property NSString *pedo2;
@property NSString *pedo3;

@end
