//
//  Mask.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 3..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mask : NSObject

+ (Mask *)sharedInstance;
- (NSArray *)maskWithValue:(UInt64)value;
- (void)testWithIndex:(int)index shiftIndex:(int)shiftIndex;
@property (strong, nonatomic)NSArray *maskData, *shiftData;
@property (nonatomic)int callCount;
@end
