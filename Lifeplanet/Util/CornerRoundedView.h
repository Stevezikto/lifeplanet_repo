//
//  CornerRoundedView.h
//  Arki
//
//  Created by ken on 2015. 6. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CornerRoundedView : UIView

- (void)setCornerRadius:(UIView *)view bounds:(CGRect)bounds full:(BOOL)isFull;

@end
