//
//  Mask.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 3..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "Mask.h"

#define UINT32(x) [NSNumber numberWithUnsignedInt:x]
#define INT(x) [NSNumber numberWithInt:x]

// Walking Mask
//const UInt64 CATEGORY_MASK = 0xE0000000; // 위치 : 왼쪽 첫 3자리
//const UInt64 PEDO_MASK = 0x1F000000; // 앞이 1이면 16진수를 완주했다는 의미 즉 16 + F(15) : 31
//const UInt64 WALKTYPE_MASK = 0xF00000;
//const UInt64 ZSCORE_MASK = 0x1E0000;
//const UInt64 IMPACTSCORE_MASK = 0x1E000;
//const UInt64 INTERVALSCORE_MASK = 0x1E00;
//const UInt64 YAWRATIO_MASK = 0x1E0;
//const UInt64 GYROZAMPLITUDE_MASK = 0x1F;
const UInt64 CATEGORY_MASK =       0xC00000000000; // 위치 : 왼쪽 첫 3자리
const UInt64 PEDO_MASK =           0x200000000000; // 앞이 1이면 16진수를 완주했다는 의미 즉 16 + F(15) : 31
const UInt64 WALKTYPE_MASK =       0x1C0000000000;
const UInt64 ZSCORE_MASK =         0x3F000000000;
const UInt64 IMPACTSCORE_MASK =    0xFC0000000;
const UInt64 INTERVALSCORE_MASK =  0x3FC00000;
const UInt64 YAWRATIO_MASK =       0x1E0;
const UInt64 GYROZAMPLITUDE_MASK = 0x1F;

// Sleep Mask
const UInt64 ACCELXMEAN_MASK = 0xF00000;
const UInt64 ACCELXMAX_MASK = 0xF0000;
const UInt64 ACCELXMIN_MASK = 0xF00;

// Activity Mask
const UInt64 AARINFORMATION_MASK = 0xFF0000;

// Walking Shift
const int CATEGORY_SHFIT = 46;
const int PEDO_SHIFT = 45;
const int WALKTYPE_SHIFT = 42;
const int ZSCORE_SHIFT = 36;
const int IMPACTSCORE_SHIFT = 30;
const int INTERVALSCORE_SHIFT = 9;
const int YAWRATIO_SHIFT = 5;
const int GYROZAMPLITUDE_SHIFT = 0;

typedef NS_ENUM(UInt64, WalkMask) {
    WalkMaskIsLeft =          0xC00000000000,
    WalkMaskIsGaitId =        0x200000000000,
    WalkMaskActivityNum =     0x1C0000000000,
    WalkMaskPedoCount =       0x3F000000000,
    WalkMaskWalkNum =         0xE00000000,
    WalkMaskAccelxMean =      0x1E0000000,
    WalkMaskZScore =          0x1E000000,
    WalkMaskImpactScore =     0x1E00000,
    WalkMaskIntervalScore =   0x1E0000,
    WalkMaskYawRatio =        0x1E000,
    WalkMaskGyroZAmplitude =  0x1F00,
};

typedef NS_ENUM(UInt64, SleepMask) {
    SleepMaskIsLeft  =        0xC00000000000,
    SleepMaskIsGaitId =       0x200000000000,
    SleepMaskActivityNum =    0x1C0000000000,
    SleepMaskPedoCount =      0x3F000000000,
    SleepMaskDiffCounter =    0xFC0000000,
    SleepMaskSleepCounter =   0x3FC00000,
};

typedef NS_ENUM(UInt64, ActivityMask) {
    ActivityMaskIsLeft =      0xC00000000000,
    ActivityMaskGaitId =      0x200000000000,
    ActivityMaskActivityNum = 0x1C0000000000,
    ActivityMaskPedoCount =   0x3F000000000,
};

typedef NS_ENUM(int, WalkShift) {
    WalkShiftIsLeft =          46,
    WalkShiftGaitId =          45,
    WalkShiftActivityNum =     42,
    WalkShiftPedoCount =       36,
    WalkShiftWalkNum =         33,
    WalkShiftAccelxMean =      29,
    WalkShiftZScore =          25,
    WalkShiftImpactScore =     21,
    WalkShiftIntervalScore =   17,
    WalkShiftYawRatio =        13,
    WalkShiftGyroZAmplitude =   8,
    
};

typedef NS_ENUM(int, SleepShift) {
    SleepShiftIsLeft =         46,
    SleepShiftGaitId =         45,
    SleepShiftActivityNum =    42,
    SleepShiftPedoCount =      36,
    SleepShiftDiffCounter =    30,
    SleepShiftSleepCounter =   22,
};

typedef NS_ENUM(int, ActivityShift) {
    ActivityShiftIsLeft =      46,
    ActivityShiftGaitId =      45,
    ActivityShiftActivityNum = 42,
    ActivityShiftPedoCount =   36,
};

typedef NS_OPTIONS(int, MaskingType){

    MaskingTypeNone = 0,
    MaskingTypeFirst = 1 << 0,
    MaskingTypeSecond = 1 << 1,
    MaskingTypeThird = 1 << 2,
};

@implementation Mask

+ (Mask *)sharedInstance
{
    static dispatch_once_t pred;
    static Mask *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[Mask alloc] init];
        
    });
    
    return view;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _callCount = 0;
        // Bitwise 테스트 
//        int flag = MaskingTypeFirst | MaskingTypeThird;
//        NSLog(@"flag : %x", flag);
//        
//        if(flag & 1){
//            NSLog(@"flag = 1");
//        }
//        if(flag % 5){
//            NSLog(@"flag = 5");
//        }
//        if(flag & 4){
//            NSLog(@"flag = 4");
//        }
        
    }
    return self;
}

- (NSArray *)maskWithValue:(UInt64)value{
    
    /*
     1.마스킹 할 위치에 해당하는 값만 1로 바꾸고 나머지는 0으로 만든다
     2.해당 위치의 실제 데이터값(original)과 마스킹 값을 마스킹한다 (&)곱하기연산
     3.마스킹 한 결과값을 shifting 한다 (실제 값을 받기 위해 민다)
     */
    // 마스킹 값을 미리 셋팅해놓고 아키에서 들어온 값과 마스킹 한 결과 값만 뽑아내면 됨
    // Mask : 마스킹 할 위치를 나타냄

    // original : 마스킹 할 값 (마스킹 위치에 해당하는 실제 데이터)
    // inter : 마스킹 한 값을 받는 변수

    UInt64 original = value; // 실제 arki에서 보내주는 값
    UInt64 inter; // masking 값
    UInt64 result; // shifting 값
    
    
    // Category 값 분류 (1:Walk 2:Sleep 3:Activity)
    UInt64 testResult = [self shift:WalkShiftActivityNum value:[self mask:WalkMaskActivityNum value:original]];
//    NSLog(@"result : %llu", result);

    _maskData = [self returnMaskDataWithType:testResult];
    _shiftData = [self returnShiftDataWithType:testResult];
//    NSLog(@"maskcount : %ld / shift : %ld", _maskData.count, _shiftData.count);
    NSMutableArray *values = [NSMutableArray new];
    
    for(int i=0; i<_maskData.count; i++){
//        NSLog(@"type : %llu / i : %d", testResult, i);
        NSNumber *convertedValue = _maskData[i];
//        NSLog(@"convertedValue:%@",convertedValue);
        inter = [self mask:convertedValue.unsignedLongLongValue value:original];
//        NSLog(@"original : %u", original);
        NSNumber *convertedValue2 = _shiftData[i];
//        convertedValue2 = _shiftData[i];
//        NSLog(@"shift : %d", convertedValue.unsignedIntValue);
//        NSLog(@"mask : %u", convertedValue.unsignedIntValue);
        result = [self shift:convertedValue2.unsignedLongLongValue value:inter];
        
        NSNumber *finalValue = [NSNumber numberWithUnsignedLongLong:result];
        [values addObject:finalValue]; // 기존:저장 신규: 새값과 대체
//        [values replaceObjectAtIndex:i withObject:finalValue];
    }
//    NSLog(@"totlaValue : %@", values);
    
    return values;
}

- (UInt64)mask:(UInt64)mask value:(UInt64)value{

    UInt64 inter = mask & value;

    return inter;
}

- (UInt64)shift:(UInt64)shift value:(UInt64)value{
    
    UInt64 result = value >> shift;
    
    return result;
}

- (NSArray *)returnMaskDataWithType:(UInt64)category{

    NSArray *masks;
    
    if(category == CategoryTypeWalk){
        masks = @[@(WalkMaskIsLeft),
                  @(WalkMaskIsGaitId),
                  @(WalkMaskActivityNum),
                  @(WalkMaskPedoCount),
                  @(WalkMaskWalkNum),
                  @(WalkMaskAccelxMean),
                  @(WalkMaskZScore),
                  @(WalkMaskImpactScore),
                  @(WalkMaskIntervalScore),
                  @(WalkMaskYawRatio),
                  @(WalkMaskGyroZAmplitude)];
    }
    else if(category == CategoryTypeSleep){
        masks = @[@(SleepMaskIsLeft),
                  @(SleepMaskIsGaitId),
                  @(SleepMaskActivityNum),
                  @(SleepMaskPedoCount),
                  @(SleepMaskDiffCounter),
                  @(SleepMaskSleepCounter)];
    }
    else if(category == CategoryTypeActivity){
        masks = @[@(ActivityMaskIsLeft),
                  @(ActivityMaskGaitId),
                  @(ActivityMaskActivityNum),
                  @(ActivityMaskPedoCount)];
    }
//    else
//        NSLog(@"정의되지 않은 카테고리");
    
    return masks;
}

- (NSArray *)returnShiftDataWithType:(UInt64)category{
    
    NSArray *shifts;
    
    if(category == CategoryTypeWalk){
        shifts = @[@(WalkShiftIsLeft),
                   @(WalkShiftGaitId),
                   @(WalkShiftActivityNum),
                   @(WalkShiftPedoCount),
                   @(WalkShiftWalkNum),
                   @(WalkShiftAccelxMean),
                   @(WalkShiftZScore),
                   @(WalkShiftImpactScore),
                   @(WalkShiftIntervalScore),
                   @(WalkShiftYawRatio),
                   @(WalkShiftGyroZAmplitude)];
    }
    else if(category == CategoryTypeSleep){
        shifts = @[@(SleepShiftIsLeft),
                   @(SleepShiftGaitId),
                   @(SleepShiftActivityNum),
                   @(SleepShiftPedoCount),
                   @(SleepShiftDiffCounter),
                   @(SleepShiftSleepCounter)];
    }
    else if(category == CategoryTypeActivity){
        shifts = @[@(ActivityShiftIsLeft),
                   @(ActivityShiftGaitId),
                   @(ActivityShiftActivityNum),
                   @(ActivityShiftPedoCount)];
    }
    
    return shifts;
}


- (void)test{

    UInt32 result[2];
    NSLog(@"result : %p", &result);
    [self shiftBitsIntoInt:&result[0] originalValue:0 index:0 size:0];
}

- (void)shiftBitsIntoInt:(UInt32 *)result originalValue:(UInt32)original index:(int)index size:(int)size{
    
    NSLog(@"result : %p", &result);
    
    UInt32 mask = 0x00;
    UInt32 temp;
    int subIndex = 0;
    
    for(int i=0; i<size; i++){
        
    }
}

int shift_bits_into_int(uint32_t* result,uint32_t val,uint8_t index, uint8_t size)
{
    uint32_t mask = 0x00;
    uint32_t temp;
    uint8_t sub_index;
    
    uint8_t i;
    
    for(i = 0 ; i < size ; i ++)
    {
        mask |= (1<<i);
    }
    
    if(index > 31)
    {
        temp = result[0];
        sub_index = index - 32;
        val &= mask;
        val = val << sub_index;
        temp |= val;
        result[0] = (uint32_t)temp;
    }
    else
    {
        temp=result[1];
        sub_index = index;
        val &= mask;
        val = val << sub_index;
        temp |= val;
        result[1] = (uint32_t)temp;
    }
    
    return 1;
}

@end
