//
//  ArkiMask.h
//  LGBluetoothExample
//
//  Created by ken on 2015. 4. 23..
//  Copyright (c) 2015년 David Sahakyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArkiMask : NSObject

+ (ArkiMask *)sharedInstance;
- (NSArray *)maskWithValue:(UInt64)value;
- (void)maskWithData:(NSData *)sixByte index:(int)mIndex length:(int)mLength complete:(void(^) (id data))complete;
- (void)maskPedoData:(NSData *)lengthData complete:(void (^)(UInt32 data))complete;
- (void)maskWithData:(NSData *)sixByte complete:(void(^) (id data))complete;
- (void)maskLengthData:(NSData *)lengthData complete:(void (^)(UInt32 data))complete;
- (void)maskOneByte:(NSData *)data compelte:(void (^)(UInt32 data))complete;
- (id)parseBodyBalance:(NSData *)scores length:(NSInteger)length; // 바디밸런스 마스킹
- (UInt32)parseOneByte:(NSData *)data compelte:(void (^)(UInt32 data))complete;

@property (strong, nonatomic)NSMutableArray *arkiData;
@property (strong, nonatomic)NSArray *maskData, *shiftData;
@property (nonatomic)int callCount;

@end
