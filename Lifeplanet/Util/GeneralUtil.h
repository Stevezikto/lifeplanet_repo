//
//  GeneralUtil.h
//  SwingWallet
//
//  Created by tulurira on 2014. 8. 27..
//  Copyright (c) 2014년 tulurira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <UICountingLabel/UICountingLabel.h>

@interface GeneralUtil : UIView

#pragma mark - Save File To Local

+ (void)saveFile:(UIImage *)image fileName:(NSString *)name complete:(void(^)(BOOL complete))complete;
+ (NSString *)loadFileWithName:(NSString *)name;
+ (BOOL)checkIsFileExist:(NSString *)filePath;
+ (NSString*)loadImage;
+ (NSURL *)applicationDocumentsDirectory;
+ (NSString *)getImagePath;

#pragma mark - Time & Date
+ (id)startOfTheDay:(NSDate *)date;
+ (id)startOfToday:(NSInteger)day;
+ (long)getCurrentTime;
+ (NSDate *)returnSpecificDateFromString:(NSString *)dateString;
+ (NSString *)currentTime:(NSDate *)date;
+ (NSString *)calculateDate:(NSDate *)date;
+ (NSString *)todayDate:(NSDate *)date;
+ (NSDate *)today;
+ (id)endOfToday:(NSInteger)day;
+ (id)endOfTheDay:(NSDate *)date;
+ (NSInteger)convertDateToInt:(NSString *)date;

#pragma mark - Alert

+ (void)showAlert:(NSString *)title message:(NSString *)message cancel:(NSString *)cancel delegate:(id)delegate others:(NSString *)ok;
+ (void)showAlert:(NSString *)title message:(NSString *)message cancel:(NSString *)cancel;

#pragma mark - Imaging

+ (void)makeImageToRoundType:(UIImageView *)imageView;
+ (void)setRoundEffect:(UIImageView *)imageView;
+ (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage;
+ (UIImage *)convertNSDataToUIImage:(NSURL *)imageUrl;
+ (UIImage *)changeImageColor:(UIImage *)image withColor:(UIColor *)color;
+ (UIImage *)takeSnapshotOfView:(UIView *)view;
+ (void)changeButton:(UIButton *)oldButton image:(NSString *)imageName title:(NSString *)title;
+ (UIImage *)returnImageFromData:(NSData *)imageData;
+ (UIImage *)getScaledImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height;

#pragma mark - Animation & Transtion

+ (void)changePoint:(UIView *)first second:(UIView *)second;
+ (void)changePosition:(UIView *)view position:(CGFloat)y;
+ (void)changeRect:(UIView *)first second:(UIView *)second;
+ (void)move:(UIView *)view position:(CGPoint)point time:(CGFloat)delay;
+ (void)performIncreseAnimation:(UICountingLabel *)view score:(NSInteger)score complete:(void(^)(bool complete))complete;
+ (void)setSpringAnimation:(UIView *)view;
+ (void)performAnimation:(UIView *)view complete:(void(^)(BOOL complete))complete;
#pragma mark - Color

+ (UIColor *)setColorWithR:(int)r G:(int)g b:(int)b;

#pragma mark - Location

+ (NSString *)currentCountryCode;
+ (NSString *)currentLanguages;

#pragma mark - Text

+(CGSize) GetSizeOfLabelForGivenText:(UILabel*)label Font:(UIFont*)fontForLabel Size:  (CGSize) constraintSize;
- (NSString *)urlEncoding:(NSString *)urlString;
+ (BOOL)checkHasString:(NSString *)string string2:(NSString *)string2;

#pragma mark - Etc
+ (BOOL)chcekValidEmail:(NSString *)text;
+ (NSInteger)randomNumber;
+ (int)randomOddEvenValue;

#pragma mark - Convert
+ (NSData *) dataFromInt:(int)num;
+ (NSMutableArray *)makeFakeValueNeedsCount:(int)count;
+ (NSInteger )calcurateUserBirth:(NSString *)userBirth;
+ (int)makeRandomValue;
+ (int) intFromData:(NSData *)data;
+ (int) intFromDataReverse:(NSData *)data;
+ (NSString *)returnRightMetric:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType;
+ (NSString *)returnRightMetric2:(NSString *)theMeasure typeOfMetricUsed2:(NSString *)metricType;
+ (NSString *)convertDateToString:(NSInteger)date;

#pragma mark - Frame

+ (float)getCurrentDeviceHeight;

@end
