//
//  GeneralUtil.m
//  SwingWallet
//
//  Created by tulurira on 2014. 8. 27..
//  Copyright (c) 2014년 tulurira. All rights reserved.
//

#import "GeneralUtil.h"
#include <sys/time.h>


#define AnimationDuration 0.4

__weak id currentFirstResponder;

@implementation GeneralUtil

//+ (CGSize)get_visible_size {
//    CGSize result;
//    
//    CGSize size = [[UIScreen mainScreen] bounds].size;
//    
//    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
//        result.width = size.height;
//        result.height = size.width;
//    }
//    else {
//        result.width = size.width;
//        result.height = size.height;
//    }
//    
//    size = [[UIApplication sharedApplication] statusBarFrame].size;
//    result.height -= MIN(size.width, size.height);
//    
//    if (self.navigationController != nil) {
//        size = self.navigationController.navigationBar.frame.size;
//        result.height -= MIN(size.width, size.height);
//    }
//    
//    if (self.tabBarController != nil) {
//        size = self.tabBarController.tabBar.frame.size;
//        result.height -= MIN(size.width, size.height);
//    }
//    
//    return result;
//}


#pragma mark - Arki

+ (void)saveFile:(UIImage *)image fileName:(NSString *)name complete:(void(^)(BOOL complete))complete{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:name];
    
    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
    
    complete(YES);
}

+ (NSString *)loadFileWithName:(NSString *)name{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:name];
    
    return filePath;
}

+ (UIImage *)convertNSDataToUIImage:(NSURL *)imageUrl{
    
    NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
    UIImage *image = [UIImage imageWithData:imageData];
    
    return image;
}

+ (NSString*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"image.png" ];
//    UIImage* image = [UIImage imageWithContentsOfFile:path];
//    return image;
    return path;
}

+ (BOOL)checkIsFileExist:(NSString *)filePath{
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        NSLog(@"파일존재");
        return YES;
    }
    else {
        NSLog(@"파일없음");
        return NO;
    }
}

+ (NSDate *)today{
    
    NSDate *date = [NSDate date];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM-dd-yyyy HH:mm";
    
    NSTimeZone *zone = [NSTimeZone localTimeZone];
    formatter.timeZone = zone;
    
    NSString *dateString = [formatter stringFromDate:date];

    return [formatter dateFromString:dateString];
}

+ (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

+ (NSString *)getImagePath{
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"image.png"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"image.png" ofType:nil];
    
    return filePath;
}

+ (BOOL)chcekValidEmail:(NSString *)text{

    BOOL isValid;
    
    NSRange aRange = [text rangeOfString:@"@"];
    if (aRange.location ==NSNotFound)
    {
        isValid = NO;
    }
    else
    {
        isValid = YES;
    }
    NSLog(@"isValid : %d", isValid);
    return isValid;
}

+ (UIImage *)changeImageColor:(UIImage *)image withColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, rect, image.CGImage);
    //    CGContextSetFillColorWithColor(context, [[UIColor greenColor] CGColor]);
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+ (void)performIncreseAnimation:(UICountingLabel *)view score:(NSInteger)score complete:(void(^)(bool complete))complete{
    
    view.format = @"%ld";
    [view countFrom:0 to:score withDuration:AnimationDuration];
    
    complete(YES);
}

+ (UIImage *)takeSnapshotOfView:(UIView *)view
{
//    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
//    
//    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
//    
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
    
    CALayer *layer = [[UIApplication sharedApplication] keyWindow].layer;
    CGFloat scale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(layer.frame.size, NO, scale);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    
    return screenshot;
}

+ (void)changeButton:(UIButton *)oldButton image:(NSString *)imageName title:(NSString *)title{
    
    [oldButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [oldButton setTitle:title forState:UIControlStateNormal];
}

+ (void)setSpringAnimation:(UIView *)view{
    
    view.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:1.0
                          delay:0.0f
         usingSpringWithDamping:0.2
          initialSpringVelocity:9.0
                        options:0
                     animations:^{                        
                         view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

+ (NSInteger)randomNumber{
    
    int value = arc4random() % 100;
    
    // x 좌표 셋팅
    return value;
}

+ (int)randomOddEvenValue{
    
    int value = (int) (arc4random() % 2) + 1;
    
    return value;
}

+ (NSMutableArray *)makeFakeValueNeedsCount:(int)count{
    
    NSMutableArray * values = [NSMutableArray new];

    for(int i=0; i<count; i++){
        
        int value = arc4random() % 100;
        NSNumber *value1 = [NSNumber numberWithInt:value];
        
        [values addObject:value1];
    }
    
    return values;
}

+ (NSInteger )calcurateUserBirth:(NSString *)userBirth{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy"];
    
    NSDate *now = [NSDate date];
    
    NSInteger today = [formatter stringFromDate:now].integerValue;
    
    NSArray *separate = [userBirth componentsSeparatedByString:@"/"];
    NSString *userYear = separate[separate.count - 1];

    return today - userYear.integerValue;
}

+ (int)makeRandomValue{
    
    return arc4random() % 100;
}


+ (NSData *) dataFromInt:(int)num {
    unsigned char * arr = (unsigned char *) malloc(sizeof(num) * sizeof(unsigned char));
    for (int i = sizeof(num) - 1 ; i >= 0; i --) {
        arr[i] = num & 0xFF;
        num = num >> 8;
    }
    NSData * data = [NSData dataWithBytes:arr length:sizeof(num)];
    free(arr);
    return data;
}

// {[MSB], ..., ... ,[LSB]}
+ (int) intFromData:(NSData *)data
{
    int intSize = sizeof(int); // change it to fixe length
    unsigned char * buffer = malloc(intSize * sizeof(unsigned char));
    [data getBytes:buffer length:intSize];
    int num = 0;
    for (int i = 0; i < intSize; i++) {
        num = (num << 8) + buffer[i];
    }
    free(buffer);
    return num;
}

// {[LSB], ..., ... ,[MSB]}
+ (int) intFromDataReverse:(NSData *)data
{
    int intSize = sizeof(int);// change it to fixe length
    unsigned char * buffer = malloc(intSize * sizeof(unsigned char));
    [data getBytes:buffer length:intSize];
    int num = 0;
    for (int i = intSize - 1; i >= 0; i--) {
        num = (num << 8) + buffer[i];
    }
    free(buffer);
    return num;
}

+ (BOOL)checkHasString:(NSString *)string string2:(NSString *)string2{
    NSRange range;
    range = [string rangeOfString:string2];
    if (range.location != NSNotFound) {
        return YES;
    }
    else
        return NO;
}

+ (float)getCurrentDeviceHeight{
    
    float deviceHeight = [[UIScreen mainScreen] applicationFrame].size.height ;
    
    return deviceHeight;
}

+ (NSInteger)convertDateToInt:(NSString *)date{
    
    NSInteger convertedDate = 0;
    
    if([date isEqual:NSLocalizedString(@"sunday", @"Sunday")]){
        convertedDate = 1;
    }
    if([date isEqual:NSLocalizedString(@"monday", @"Monday")]){
        convertedDate = 2;
    }
    if([date isEqual:NSLocalizedString(@"tuesday", @"Tuesday")]){
        convertedDate = 3;
    }
    if([date isEqual:NSLocalizedString(@"wednesday", @"Wednesday")]){
        convertedDate = 4;
    }
    if([date isEqual:NSLocalizedString(@"thursday", @"Thursday")]){
        convertedDate = 5;
    }
    if([date isEqual:NSLocalizedString(@"friday", @"Friday")]){
        convertedDate = 6;
    }
    if([date isEqual:NSLocalizedString(@"saturday", @"Saturday")]){
        convertedDate = 7;
    }
    
    NSLog(@"date : %@ , int : %ld", date, convertedDate);
    
    return convertedDate;
}

+ (NSString *)convertDateToString:(NSInteger)date{
    
    NSLog(@"date : %ld", date);
    
    NSString *convertedDate;
    
    switch (date) {
        case ZiktoDaySunday:
            convertedDate = NSLocalizedString(@"sunday", @"Sunday");
            break;
        case ZiktoDayMonday:
            convertedDate = NSLocalizedString(@"monday", @"Monday");
            break;
        case ZiktoDayTuesday:
            convertedDate = NSLocalizedString(@"tuesday", @"Tuesday");
            break;
        case ZiktoDayWednesday:
            convertedDate = NSLocalizedString(@"wednesday", @"Wednesday");
            break;
        case ZiktoDayThursday:
            convertedDate = NSLocalizedString(@"thursday", @"Thursday");
            break;
        case ZiktoDayFriday:
            convertedDate = NSLocalizedString(@"friday", @"Friday");
            break;
        case ZiktoDaySaturday:
            convertedDate = NSLocalizedString(@"saturday", @"Saturday");
            break;
        default:
            break;
    }
    NSLog(@"converted : %@", convertedDate);
    return convertedDate;
}

+ (NSString *)returnRightMetric:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType
{
    NSString *result = nil;
    
    if ([metricType isEqualToString:@"inches"]) {
        if ([theMeasure isEqualToString:@""]) {
            return @"0";
        }
        NSArray* theConvertion = [theMeasure componentsSeparatedByCharactersInSet:
                                  [NSCharacterSet characterSetWithCharactersInString:@"’”"]];
        NSInteger value1 = [theConvertion[0] intValue];
        NSInteger value2 = [theConvertion[1] intValue];
        
        float number = ((value1 * 12) + value2) * 2.54;
        //        result = [NSString stringWithFormat:@"%.0f cm", round(number * 100.0) / 100.0];
        result = [NSString stringWithFormat:@"%.0f", round(number * 100.0) / 100.0];
        
        
    } else if ([metricType isEqualToString:@"cm"]) {
        float value = [theMeasure floatValue];
        float number = value / 2.54;
        
        if (roundf( number) >= 12.0) {
            if ((int)round( number) % 12==0) {
                result = [NSString stringWithFormat:@"%i’%i”", (int)roundf(number / 12.0), (int)round( number) % 12];
            }else{
                result = [NSString stringWithFormat:@"%i’%i”", (int)floorf(number / 12.0), (int)round( number) % 12];
            }
        } else {
            result = [NSString stringWithFormat:@"0’%i”", (int)round(number)];
        }
    }
    NSLog(@"result: %@", result);
    return result;
}

+ (NSString *)returnRightMetric2:(NSString *)theMeasure typeOfMetricUsed2:(NSString *)metricType
{
    NSString *result = nil;
    
    if ([metricType isEqualToString:@"inches"]) {
        if ([theMeasure isEqualToString:@""]) {
            return @"0";
        }
//        NSArray* theConvertion = [theMeasure componentsSeparatedByCharactersInSet:
//                                  [NSCharacterSet characterSetWithCharactersInString:@"’”"]];
        NSInteger value1 = [theMeasure integerValue];
//        NSInteger value2 = [theConvertion[1] intValue];
        NSLog(@"value1 : %ld", value1);
        float number = ((value1 * 12)) * 2.54;
        NSLog(@"number : %f", number);
        result = [NSString stringWithFormat:@"%.0f", round(number * 100.0) / 100.0];
        
        
    } else if ([metricType isEqualToString:@"cm"]) {
        float value = [theMeasure floatValue];
        float number = value / 2.54;
        
        if (roundf( number) >= 12.0) {
            if ((int)round( number) % 12==0) {
                result = [NSString stringWithFormat:@"%i’%i”", (int)roundf(number / 12.0), (int)round( number) % 12];
            }else{
                result = [NSString stringWithFormat:@"%i’%i”", (int)floorf(number / 12.0), (int)round( number) % 12];
            }
        } else {
            result = [NSString stringWithFormat:@"0’%i”", (int)round(number)];
        }
    }
    NSLog(@"result: %@", result);
    return result;
}


+ (UIImage *)returnImageFromData:(NSData *)imageData{
    
    UIImage *image = [UIImage imageWithData:imageData];
    
    return image;
}

+ (UIImage *)getScaledImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    
    if (oldWidth < width && oldHeight < height)
        return image;
    
    CGFloat scaleFactorW =1;
    CGFloat scaleFactorH =1;
    
    if (oldWidth > width)
        scaleFactorW = width / oldWidth;
    if(oldHeight > height)
        scaleFactorH = height / oldHeight;
    
    CGFloat scaleFactor = (scaleFactorW<scaleFactorH)?scaleFactorW:scaleFactorH;
    
    
    CGFloat newHeight = oldHeight * scaleFactor;
    //CGFloat newWidth = oldWidth * scaleFactor;
    CGSize newSize = CGSizeMake(width, newHeight);
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - Common

+ (long)getCurrentTime
{
    //    NSLog(@"%s",  __FUNCTION__);
    struct timeval time;
    gettimeofday(&time, NULL);
    long millis = (time.tv_sec * 1000) + (time.tv_usec / 1000);
    
    //    NSLog(@"millis = %ld", millis);
    
    return millis;
}

+ (void)showAlert:(NSString *)title message:(NSString *)message cancel:(NSString *)cancel delegate:(id)delegate others:(NSString *)ok {
    
    [[[UIAlertView alloc]
     initWithTitle:title
     message:message
     delegate:delegate
     cancelButtonTitle:cancel
      otherButtonTitles:ok, nil] show];
}

+ (void)showAlert:(NSString *)title message:(NSString *)message cancel:(NSString *)cancel{
    
    [[[UIAlertView alloc]
      initWithTitle:title
      message:message
      delegate:nil
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil, nil] show];
}

+ (void)makeImageToRoundType:(UIImageView *)imageView{
    
    CALayer * l = [imageView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:20.0];
    
    // You can even add a border
//    [l setBorderWidth:4.0];
//    [l setBorderColor:[[UIColor blueColor] CGColor]];
}

+ (void)changePoint:(UIView *)first second:(UIView *)second{

    CGRect frame = first.frame;
    
    first.frame = CGRectMake(first.frame.origin.x, second.frame.origin.y, first.frame.size.width, first.frame.size.height);
    second.frame = CGRectMake(second.frame.origin.x, frame.origin.y, second.frame.size.width, second.frame.size.height);;
}

+ (void)changeRect:(UIView *)first second:(UIView *)second{
    
//    NSLog(@"first = %f/ second = %f", first.origin.x, second.origin.x);
//    NSLog(@"first.x = %f y = %f", first.frame.origin.x, first.frame.origin.y);
//    NSLog(@"second.x = %f y = %f", second.frame.origin.x, second.frame.origin.y);
    
    CGRect frame = first.frame;
    
    first.frame = second.frame;
    
    second.frame = frame;
}

+ (void)changePosition:(UIView *)view position:(CGFloat)y{
    
    [UIView animateWithDuration:0.25 animations:^{
        view.frame = CGRectMake(view.frame.origin.x, y, view.frame.size.width, view.frame.size.height);
    }];
}

+ (void)setRoundEffect:(UIImageView *)imageView{
    
    imageView.layer.cornerRadius = imageView.frame.size.width /2;
    imageView.layer.masksToBounds = YES;
//    imageView.layer.borderWidth = 0;
}

+ (UIColor *)setColorWithR:(int)r G:(int)g b:(int)b{
    
    return [UIColor colorWithRed:r/255.0
                           green:g/255.0
                            blue:b/255.0
                           alpha:1.0];
}

+ (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage{
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    return [UIImage imageWithCGImage:masked];
    
}

+ (NSString *)calculateDate:(NSDate *)date {
    
    NSString *interval;
    int diffSecond = (int)[date timeIntervalSinceNow];
    
    if (diffSecond < 0) { //입력날짜가 과거
        
        //날짜 차이부터 체크
        int valueInterval;
        int valueOfToday, valueOfTheDate;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
        NSString *currentLanguage = [languages objectAtIndex:0];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:currentLanguage]];
        
        [formatter setDateFormat:@"yyyyMMdd"];
        
        NSDate *now = [NSDate date];
        valueOfToday = [[formatter stringFromDate:now] intValue]; //오늘날짜
        valueOfTheDate = [[formatter stringFromDate:date] intValue]; //입력날짜
        valueInterval = valueOfToday - valueOfTheDate; //두 날짜 차이
        
        if(valueInterval <= 30)
            interval = [NSString stringWithFormat:@"%dd", valueInterval];
        
//        if(valueInterval == 1)
//            interval = @"1d";
//        else if(valueInterval == 2)
//            interval = @"2d";
//        else if(valueInterval == 3)
//            interval = @"3d";
//        else if(valueInterval > 31) { //4일 이상일때는 그냥 요일, 날짜 표시
        
        else if(valueInterval > 31) { //4일 이상일때는 그냥 요일, 날짜 표시
            if ([currentLanguage compare:@"ko"] == NSOrderedSame)
                [formatter setDateFormat:@"EEEE, yyyy년 MMM d일"]; //locale 한국일 경우 "년, 일" 붙이기
            else
                [formatter setDateFormat:@"EEEE, yyyy. MMM d"];
            interval = [formatter stringFromDate:date];
        }
        else if(valueInterval >= 4)
        { //4일 이상일때는 그냥 요일, 날짜 표시
            if ([currentLanguage compare:@"ko"] == NSOrderedSame)
                [formatter setDateFormat:@"EEEE, yyyy년 MMM월 d일"]; //locale 한국일 경우 "년, 일" 붙이기
            else
                [formatter setDateFormat:@"EEEE, yyyy. MMM d"];
            interval = [formatter stringFromDate:date];
        }
        else { //날짜가 같은경우 시간 비교
            
            [formatter setDateFormat:@"HH"];
            
            valueOfToday = [[formatter stringFromDate:now] intValue]; //오늘시간
            valueOfTheDate = [[formatter stringFromDate:date] intValue]; //입력시간
            valueInterval = valueOfToday - valueOfTheDate; //두 시간 차이
            
            if(valueInterval == 1)
                interval = @"1h";
            else if(valueInterval >= 2)
                interval = [NSString stringWithFormat:@"%ih", valueInterval];
            else { //시간이 같은 경우 분 비교
                
                [formatter setDateFormat:@"mm"];
                
                valueOfToday = [[formatter stringFromDate:now] intValue]; //오늘분
                valueOfTheDate = [[formatter stringFromDate:date] intValue]; //입력분
                valueInterval = valueOfToday - valueOfTheDate; //두 분 차이    
                
                if(valueInterval == 1)
                    interval = @"1m";
                else if(valueInterval >= 2)
                    interval = [NSString stringWithFormat:@"%im", valueInterval];
                else //분이 같은 경우 차이가 1분 이내
                    interval = @"now";
                
            }
            
        }
        
    }
    else { //입력날짜가 미래
        NSLog(@"%s, 입력된 날짜가 미래임", __func__);
        interval = @"지금 등록";
    }
    
    
    return interval;
}

+ (id)startOfTheDay:(NSDate *)date{
    
//    NSDate *today = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
//    NSDate *today = [date dateByAddingDays:0];
    NSDate *today2 = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
    
    return today2;
}

+ (id)endOfTheDay:(NSDate *)date{
    
//    NSDate *date = [[NSDate date] dateByAddingDays:day];
    
    NSDate *today = [[[[date dateBySettingHour:23] dateBySettingMinute:59] dateBySettingSecond:59] toLocalTime];
    
    return today;
}

+ (id)startOfToday:(NSInteger)day{
    NSDate *date = [[NSDate date] dateByAddingDays:day];
//    NSDate *date = [[NSDate date] dateBySubtractingDays:day];
    NSDate *today = [[[[date dateBySettingHour:0] dateBySettingMinute:0] dateBySettingSecond:0] toLocalTime];
    
    return today;
}

+ (id)endOfToday:(NSInteger)day{
    
    NSDate *date = [[NSDate date] dateByAddingDays:day];
    NSDate *today = [[[[date dateBySettingHour:23] dateBySettingMinute:59] dateBySettingSecond:59] toLocalTime];
    
    return today;
}

+ (NSDate *)returnSpecificDateFromString:(NSString *)dateString{
    
//    NSString *dateString = @"08-05-1983";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    
    return dateFromString;
}


+ (NSString *)todayDate:(NSDate *)date {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];

    NSString *interval;
    int valueOfToday;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:currentLanguage]];
    
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    

    NSDate *now = [NSDate date];
    valueOfToday = [[formatter stringFromDate:now] intValue]; //오늘날짜
    NSLog(@"valueOfToday = %@", [formatter stringFromDate:now]);
    [formatter setDateFormat:@"EEEE, yyyy. MMM d HH:mm"];
    interval = [formatter stringFromDate:date];
 
    return interval;
}

+ (NSString *)currentTime:(NSDate *)date {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
//    NSString *interval;
//    int valueOfToday;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:currentLanguage]];
    
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    
    
    NSDate *now = [NSDate date];
    NSString * currentTime = [formatter stringFromDate:now]; //오늘날짜
//    NSLog(@"valueOfToday = %@", [formatter stringFromDate:now]);
    
    return currentTime;
}

+ (void)performAnimation:(UIView *)view complete:(void(^)(BOOL complete))complete{
    
    __block CGRect frame = view.frame;
    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + view.frame.size.height, view.frame.size.width, 1);
    view.hidden = NO;
    
    [UIView animateWithDuration:AnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ view.frame = frame; }
                     completion:^(BOOL finished) {
                         complete(YES);
                     }];
}

+ (void)move:(UIView *)view position:(CGPoint)point time:(CGFloat)delay{
    
    CGRect frame = view.frame;
    frame.origin = point;
    
    [UIView animateWithDuration:delay
                     animations:^{view.frame = frame;}
                     completion:^(BOOL finished){
                         }
     ];
}

+ (NSString *)convert:(int)digit amount:(NSString *)amount{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.maximumFractionDigits = digit;
    
    double value = amount.doubleValue;
    
    NSNumber * amountToNumber = [NSNumber numberWithDouble:value];
    NSString * price = [formatter stringFromNumber:amountToNumber];
    
    NSLog(@"amountToNumber = %@", amountToNumber);
    
    return price;
}

+ (NSString *)currentCountryCode{
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
//    NSLog(@"countryCode = %@", countryCode);

    return countryCode;
}

+ (NSString *)currentLanguages{
    
    NSString * locale = [[NSLocale preferredLanguages] objectAtIndex:0];
    return locale;
}

// 특수문자 강제 URL 인코딩
- (NSString *)urlEncoding:(NSString *)urlString
{
    NSMutableString *resultString = [NSMutableString stringWithString:urlString];
    
    [resultString replaceOccurrencesOfString:@"," withString:@"%2C" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"+" withString:@"%2B" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"&" withString:@"%26" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"{" withString:@"%7B" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"}" withString:@"%7D" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"[" withString:@"%5B" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"]" withString:@"%5D" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@":" withString:@"%3A" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    [resultString replaceOccurrencesOfString:@"\"" withString:@"%22" options:NSLiteralSearch range:NSMakeRange(0, [resultString length])];
    
    return resultString;
}

+(CGSize) GetSizeOfLabelForGivenText:(UILabel*)label Font:(UIFont*)fontForLabel Size:  (CGSize) constraintSize{
    label.numberOfLines = 0;
    CGRect labelRect = [label.text boundingRectWithSize:constraintSize options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:fontForLabel} context:nil];
    return (labelRect.size);
}
@end
