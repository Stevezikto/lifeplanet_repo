//
//  Mask.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 2. 3..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "ArkiMask.h"

#define UINT32(x) [NSNumber numberWithUnsignedInt:x]
#define INT(x) [NSNumber numberWithInt:x]
#define FIRST_BYTE_INDEX 32
#define EIGHTEEN_BYTE 18.0

typedef NS_ENUM(int, WalkMask) {
//    WalkMaskIsLeft =          2, // size : 사용 비트 size

    WalkMaskLeftRight           = 2,
    WalkMaskAuthen              = 1,
    WalkMaskActivity            = 3,
    WalkMaskPedoCount           = 6,
    WalkMaskAccelMag            = 4,
    WalkMaskWalkNum             = 3,
};

typedef NS_ENUM(int, SleepMask) {
    
    SleepMaskLeftRight          = 2,
    SleepMaskAuthen             = 1,
    SleepMaskActivity           = 3,
    SleepMaskPedoCount          = 6,
    SleepMaskAccelMag           = 4,
    SleepMaskDiffCounter        = 8,
};

typedef NS_ENUM(int, ActivityMask) {

    ActivityMaskLeftRight       = 2,
    ActivityMaskAuthen          = 1,
    ActivityMaskActivity        = 3, 
    ActivityMaskPedoCount       = 6,
    ActivityMaskAccelMag        = 4,
    ActivityMaskDiffCounter     = 8,
};

typedef NS_ENUM(UInt8, WalkShift) {
    //    WalkShiftIsLeft =          46, // 48bit를 기준으로 작업한 것과 마찬가지(앞 16비트를 제외하고 시작해서 0번째 인덱스부터 작업한것과 동일한 상황)
    //    WalkShiftGaitId =          45,
    //    WalkShiftActivityNum =     42,
    //    WalkShiftPedoCount =       36,
    //    WalkShiftWalkNum =         33,
    //    WalkShiftAccelxMean =      29,
    //    WalkShiftZScore =          25,
    //    WalkShiftImpactScore =     21,
    //    WalkShiftIntervalScore =   17,
    //    WalkShiftYawRatio =        13,
    //    WalkShiftGyroZAmplitude =   8,
//    WalkShiftIsLeft =          46, // 48bit를 기준으로 작업한 것과 마찬가지(앞 16비트를 제외하고 시작해서 0번째 인덱스부터 작업한것과 동일한 상황)
//    WalkShiftGaitId =          45, // 시작값(index)
    WalkShiftLeftRight          = 46,
    WalkShiftAuthen             = 43,
    WalkShiftActivity           = 42,
    WalkShiftPedoCount          = 36,
    WalkShiftAccelMag           = 32,
    WalkShiftWalkNum            =  0,
};

typedef NS_ENUM(int, SleepShift) {
//    SleepShiftIsLeft =         46,
//    SleepShiftGaitId =         45,
    SleepShiftLeftRight         = 46,
    SleepShiftAuthen            = 43,
    SleepShiftActivity          = 42,
    SleepShiftPedoCount         = 36,
    SleepShiftAccelMag          = 32,
    SleepShiftDiffCounter       =  0,
};

typedef NS_ENUM(int, ActivityShift) {
    ActivityShiftLeftRight      = 46,
    ActivityShiftAuthen         = 43,
    ActivityShiftActivity       = 42,
    ActivityShiftPedoCount      = 36,
    ActivityShiftAccelMag       = 32,
    ActivityShiftDiffCounter    =  0,
};

typedef NS_ENUM(int, CommonMask) {
    
    CommonMaskWalking           = 1,
    CommonMaskSyncLength        = 32,
};

typedef NS_ENUM(int, CommonShift) {
    
    CommonShiftWalking          = 31,
    CommonShiftSyncLength       = 32,
};

typedef NS_OPTIONS(int, MaskingType){
    
    MaskingTypeNone = 0,
    MaskingTypeFirst = 1 << 0,
    MaskingTypeSecond = 1 << 1,
    MaskingTypeThird = 1 << 2,
};

@implementation ArkiMask

+ (ArkiMask *)sharedInstance
{
    static dispatch_once_t pred;
    static ArkiMask *view = nil;
    
    dispatch_once(&pred, ^{
        view = [[ArkiMask alloc] init];
        
    });
    
    return view;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _arkiData = [NSMutableArray new];
        _maskData = [self returnMaskDataWithType:CategoryTypeWalk];
        _shiftData = [self returnShiftDataWithType:CategoryTypeWalk];
        _callCount = 0;
    }
    return self;
}

- (void)maskLengthData:(NSData *)lengthData complete:(void (^)(UInt32 data))complete{
    
//    NSLog(@"data : %@", lengthData);
//    UInt32 length = CFSwapInt32LittleToHost(*(UInt32*)([lengthData bytes]));
    UInt32 length = *(UInt32 *)[lengthData bytes];
    
//    int size = (int)_maskData[4];
//    UInt32 mask = [self createMaskSize:size];
//    UInt8 sub_index = (UInt8)_shiftData[4];
    
    int size = (int)CommonMaskSyncLength;
    UInt32 mask = [self createMaskSize:size];
//    UInt8 sub_index = (UInt8)CommonShiftSyncLength;
    
    UInt32 value;
//    NSLog(@"mask : %@ / shift : %@", _maskData, _shiftData);
//    NSLog(@"length : %u / size : %d / index : %X / mask : %X",length, size, sub_index, mask);
    
//    value = length >> sub_index;
    value = length & mask;
    NSLog(@"mask : %d , value : %u , length : %u",mask, value, (unsigned int)length);
    
    complete(ceil((float)length / EIGHTEEN_BYTE));
}

- (void)maskPedoData:(NSData *)lengthData complete:(void (^)(UInt32 data))complete{
    
    //    NSLog(@"data : %@", lengthData);
    UInt32 length = CFSwapInt32LittleToHost(*(UInt32*)([lengthData bytes]));
    
    NSLog(@"pedo 원래  : %u", length);
//    int size = (int)_maskData[2];
    UInt32 mask = [self createMaskSize:4];
    UInt8 sub_index = 28;
    
    UInt32 value;
    
    value = length >> sub_index;
    value = value & mask;
  
    complete(length);
}

- (void)maskOneByte:(NSData *)data compelte:(void (^)(UInt32 data))complete{
    
    UInt32 one = CFSwapInt32LittleToHost(*(UInt32*)([data bytes]));
//    UInt32 one = (UInt32)[data bytes];
    
    int size = CommonMaskWalking;
    UInt32 mask = [self createMaskSize:size];
    UInt8 sub_index = (UInt8)CommonShiftWalking;
    
    UInt32 value;
   
    value = one >> sub_index;
    value = value & mask;
//    NSLog(@"one : %u , mask : %u , subIndex : %d , value : %u",one,mask,sub_index, value);
    
    complete(one);
}

- (UInt32)parseOneByte:(NSData *)data compelte:(void (^)(UInt32 data))complete{
    
    UInt32 one = CFSwapInt32LittleToHost(*(UInt32*)([data bytes]));
    //    UInt32 one = (UInt32)[data bytes];
    
    int size = CommonMaskWalking;
    UInt32 mask = [self createMaskSize:size];
    UInt8 sub_index = (UInt8)CommonShiftWalking;
    
    UInt32 value;
    
    value = one >> sub_index;
    value = value & mask;
//    NSLog(@"one : %u , mask : %u , subIndex : %d , value : %u",one,mask,sub_index, value);
    
    complete(one);
    return one;
}

- (id)parseBodyBalance:(NSData *)scores length:(NSInteger)length{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for(int i = 0; i <length; i++){
        NSData *scoreData = [scores subdataWithRange:NSMakeRange(i, 1)]; // crash issue
        uint8_t first =  (*(uint8_t*)([scoreData bytes]));
        
        [array addObject:@(first)];
    }
    return array;
//    NSData *scoreData = [scores subdataWithRange:NSMakeRange(0, 1)];
//    uint8_t first =  (*(uint8_t*)([scoreData bytes]));
}

- (void)maskWithData:(NSData *)sixByte complete:(void(^) (id data))complete{

    NSData *firstTwoByte = [sixByte subdataWithRange:NSMakeRange(0, 2)]; // 앞 0,1 byte
    NSData *secondFourByte = [sixByte subdataWithRange:NSMakeRange(2, 4)]; // 뒤 2,3,4,5 byte 로 총 6byte
    
    UInt32 test[2];

    /*
     
     uint32 fuction( byte1, byte2, length, index)
     create mask with length
     
     index > 32
     
     subindex = index - 32;
     
     val = firstTwoByte >> subindex : 0번 위치로 이동
     val = val & mask
     
     index <=32
     
     subindex = index
     
     val = secondFourByte >> subinex
     val = val & mask
     
     
     */
    
//    test[0] = CFSwapInt32LittleToHost(*(UInt32*)([firstTwoByte bytes]));

    test[0] = (*(UInt32*)([firstTwoByte bytes]));
    test[1] = (*(UInt32*)([secondFourByte bytes]));

//    NSLog(@"test1:%u / test2:%u", test[0], test[1]);
    
    NSMutableArray *new = [NSMutableArray new];
    
    for(int i=0; i< _maskData.count; i++){
        // value < 32 : first , value > 32 : second byte
        NSNumber *value = [self shiftBitsIntoInt:test index:[_shiftData[i] intValue] size:[_maskData[i] intValue]];
        [new addObject:value];
    }

    complete(new);
}

- (NSNumber *)shiftBitsIntoInt:(UInt32 *)result index:(int)index size:(UInt8)size{
//    NSLog(@"INDEX : %D / SIZE : %D", index, size);
    UInt32 mask = [self createMaskSize:size];
    UInt8 sub_index;
    UInt32 value;
    
    if(index > FIRST_BYTE_INDEX){
        sub_index = index - FIRST_BYTE_INDEX;
        value = result[0] >> sub_index;
        value = value & mask;
    }
    else{
        sub_index = index;
        value = result[1] >> sub_index;
        value = value & mask;
    }
    
    return @(value);
}

- (UInt32)createMaskSize:(int)size{

    UInt32 mask = 0x00;
    
    for(int i = 0; i < size; i ++){
        mask |= (1<<i); // size 만큼 루프 돌면서 1로 만든다
    }
    return mask;
}

- (UInt64)mask:(UInt64)mask value:(UInt64)value{
    
    UInt64 inter = mask & value;
    
    return inter;
}

- (UInt64)shift:(UInt64)shift value:(UInt64)value{
    
    UInt64 result = value >> shift;
    
    return result;
}

- (NSArray *)returnMaskDataWithType:(int)category{
    
    NSArray *masks;
    
    if(category == CategoryTypeWalk){
        masks = @[
                  @(WalkMaskLeftRight),
                  @(WalkMaskAuthen),
                  @(WalkMaskActivity),
                  @(WalkMaskPedoCount),
                  @(WalkMaskAccelMag),
                  @(WalkMaskWalkNum)
                  ];
    }
    else if(category == CategoryTypeSleep){
        masks = @[
                  @(SleepMaskLeftRight),
                  @(SleepMaskAuthen),
                  @(SleepMaskActivity),
                  @(SleepMaskPedoCount),
                  @(SleepMaskAccelMag),
                  @(SleepMaskDiffCounter)
                  ];
    }
    else if(category == CategoryTypeActivity){
        masks = @[
                  @(ActivityMaskLeftRight),
                  @(ActivityMaskAuthen),
                  @(ActivityMaskActivity),
                  @(ActivityMaskPedoCount),
                  @(ActivityMaskAccelMag)
                  ];
    }
    
    return masks;
}

- (NSArray *)returnShiftDataWithType:(int)category{
    
    NSArray *shifts;
    
    if(category == CategoryTypeWalk){
        shifts = @[
                   @(WalkShiftLeftRight),
                   @(WalkShiftAuthen),
                   @(WalkShiftActivity),
                   @(WalkShiftPedoCount),
                   @(WalkShiftAccelMag),
                   @(WalkShiftWalkNum)
                   ];
    }
    else if(category == CategoryTypeSleep){
        shifts = @[
                   @(SleepShiftLeftRight),
                   @(SleepShiftAuthen),
                   @(SleepShiftActivity),
                   @(SleepShiftPedoCount),
                   @(SleepShiftAccelMag),
                   @(SleepShiftDiffCounter)
                   ];
    }
    else if(category == CategoryTypeActivity){
        shifts = @[
                   @(ActivityShiftLeftRight),
                   @(ActivityShiftAuthen),
                   @(ActivityShiftActivity),
                   @(ActivityShiftPedoCount),
                   @(ActivityShiftAccelMag),
                   ];
    }
    
    return shifts;
}

@end
