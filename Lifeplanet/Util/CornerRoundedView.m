//
//  CornerRoundedView.m
//  Arki
//
//  Created by ken on 2015. 6. 19..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "CornerRoundedView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CornerRoundedView

- (void)setCornerRadius:(UIView *)view bounds:(CGRect)bounds full:(BOOL)isFull{

    NSLog(@"");
    
    UIBezierPath *maskPath;
    UIRectCorner corner = UIRectCornerBottomLeft | UIRectCornerTopLeft;
    
    if(isFull){
        corner = (UIRectCornerBottomLeft|UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight);
    }

    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:bounds
                                     byRoundingCorners:corner
                                           cornerRadii:CGSizeMake(6.0, 6.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    
//    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//    animation.path = maskPath.CGPath ;
//    animation.fillMode = kCAFillModeForwards ;
//    animation.duration = 0.5f;
//    animation.removedOnCompletion = NO ;
//    [view.layer addAnimation:animation forKey:@"Animation3"];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
