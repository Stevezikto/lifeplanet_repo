//
//  AbstractViewController.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 12..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UINavigationItem+setBackButtonTitle.h"
#import "UINavigationBar+Addition.h"

@interface BaseViewController : UIViewController<UIDocumentInteractionControllerDelegate>

/* 네비바 이전 버튼에 타이틀 제거 */
- (void)setBackButtonWithoutTitle;

/* 네비바 컬러 커스터마이징*/
- (void)setNavigationBarCustomize;

/* 네비바 Footer 라인 컬러 커스터마이징 */
- (void)setNavigationBarLineColor;

/* 네비바에 특정 이미지 적용 */
//- (void)setNavigationBarImage;

/* 네비바에 특정 컬러 적용 */
- (void)setNavigationBarColor:(UIColor *)color;

/* 스테이터스 바 컬러 화이트 */
- (void)setStatusBarColorWhite;

///* Right Button*/
//- (void)share;

@end


