//
//  LaunchViewController.m
//  Arki
//
//  Created by ken on 2015. 2. 26..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "LaunchViewController.h"
#import <Parse/Parse.h>
//#import "SignHandler.h"


@interface LaunchViewController ()

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;

//    [PFUser logOut];
    // new sign up
//    if([SignHandler currentUser] == nil){
//        [self startLogin];
//    }
//    else{
//        [self startMain];
//    }
//    NSLog(@"currentUser : %@", [SignHandler currentUser]);
    
//    if(![SignHandler signedUser]){
//        [self startLogin];
//    }
//    else{
//        [self startMain];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)share{
    
}

- (void)setNavigationBarLineColor{
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar hideBottomHairline];
}

- (void)startLogin{
    
    [self performSegueWithIdentifier:@"LaunchToLogin" sender:self];
}

- (void)startMain{
    
    //LaunchToBodyBase,LaunchToDailyBase,LaunchToTrendBase
    [self performSegueWithIdentifier:@"LaunchToBodyBase" sender:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
