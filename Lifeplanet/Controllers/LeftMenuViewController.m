//
//  LeftMenuViewController.m
//  Arki
//
//  Created by tulurira on 2015. 1. 2..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "LeftMenuCell.h"
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "UINavigationBar+Addition.h"
//#import "ParseHandler.h"

@interface LeftMenuViewController ()
@property (strong, nonatomic) IBOutlet UITableView *mTableView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"");
    // 빈 셀 라인 제거
    self.mTableView.tableFooterView = [UIView new];
    
    self.slideOutAnimationEnabled = YES;
    
//    if([ParseHandler getParseUser])
//        [self.navigationController.navigationBar setBarTintColor:DEFAULT_BLUE];
//    else
//        [self.navigationController.navigationBar setBarTintColor:DEFAULT_BLUE];
    
    [self setNavigationBarLineColor];
    
//    [self setCellNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBarColor:) name:@"changeNaviColor" object:nil];
    
    // 셀 초기화
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [_mTableView selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionBottom];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSLog(@"");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc{
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}

#pragma mark - Private

- (void)setNavigationBarImage:(NSString *)imageName{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:imageName] forBarMetrics:UIBarMetricsDefault];
}

- (void)setNavigationBarColor:(UIColor *)color{
//   [self.navigationController.navigationBar setBarTintColor:color];
//    [self.navigationController.navigationBar setBackgroundImage:[CustomColor imageFromColor:color] forBarMetrics:UIBarMetricsDefault];
}

- (void)setNavigationBarLineColor{
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar hideBottomHairline];
}

- (void)changeBarColor:(NSNotification *)notification{
    NSString *tag = notification.userInfo[@"tag"];
    NSLog(@"selected Menu Type : %@", tag);
    
    [self setNavigationBarColor:[self changeCellColor:tag.intValue]];
}

- (void)setCellNotification{
 
//    [[NSNotificationCenter defaultCenter] addObserverForName:@"changeNaviColor"
//                                                      object:nil
//                                                       queue:[NSOperationQueue mainQueue]
//                                                  usingBlock:^(NSNotification *note) {
//                                                      NSString *tag = note.userInfo[@"tag"];
//                                                      NSLog(@"menu tag : %@", tag);
//                                                      [self setNavigationBarColor:[self changeCellColor:tag.intValue]];
//                                                  }];

}

- (UIColor *)changeCellColor:(int)cellIndex{
    
    UIColor *color;
    NSLog(@"color index : %d",cellIndex);
    
    switch (cellIndex){
        case MenuTypeMain:
            NSLog(@"green color");
            color = DEFAULT_GREEN;
            break;
            
        case MenuTypeSoundWalking:
            color = DEFAULT_BLUE;
            break;
            
        case MenuTypeBodyBalance://BodyBaseViewController
            color = DEFAULT_BLUE;
            break;
        
        case MenuTypeAboutme:
            color = DEFAULT_BLUE;
            break;
        case MenuTypeGait:
            color = DEFAULT_YELLOW;
            break;
        case MenuTypeSetting:
            color = DEFAULT_BLUE;
            break;
            
        case MenuTypeSleep:
            color = DEFAULT_NAVY;
            break;       
    }
    
    return color;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    LeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftMenuCell" forIndexPath:indexPath];
////    UIView *selectionColor = [[UIView alloc] init];
////    selectionColor.backgroundColor = [UIColor colorWithRed:81.0 / 255.0 green:188.5 / 255.0 blue:225.0 / 255.0 alpha:1.000];// 원하시는 색을 설정
////    cell.selectedBackgroundView = selectionColor;
//    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:18.0];
//    switch (indexPath.row)
//    {
//        case MenuTypeSoundWalking:
//            cell.textLabel.text = NSLocalizedString(@"menu_soundwalking", @"SoundWalking");
//            break;
//        case MenuTypeBodyBalance:
//            cell.textLabel.text = NSLocalizedString(@"menu_bodybalance", @"Body Balance");
//            break;
//        case MenuTypeMain:
//            cell.textLabel.text = NSLocalizedString(@"title_main", @"Daily Activity");
//            break;
//        case MenuTypeAboutme:
//            cell.textLabel.text = NSLocalizedString(@"menu_aboutme", @"About me");
//            break;
//        case MenuTypeSetting:
//            cell.textLabel.text = NSLocalizedString(@"menu_settings", @"Settings");
//            break;
//        case MenuTypeGait:
//            cell.textLabel.text = @"Gait1";
//            break;
//    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"");
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    UIViewController *vc ;

//    switch (indexPath.row)
//    {
//        case MenuTypeSoundWalking:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"BodyBaseViewController"];
//            break;
//        case MenuTypeBodyBalance:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"BodyDetailViewController"];
//            break;
//        case MenuTypeMain://DailyBaseViewController,GaitViewController
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"DailyBaseViewController"];
//            break;
//        case MenuTypeAboutme:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"];
//            break;
//        case MenuTypeSetting:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
//            break;
//        case MenuTypeGait:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
//            break;
//    }
    
    
    [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:vc withCompletion:^{
        [self setNavigationBarLineColor];
        NSLog(@"subController.count: %ld", [[SlideNavigationController sharedInstance] viewControllers].count);
    }];
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row == 0) {
//        [cell setSelected:YES animated:NO];
//    }
//    else{
//        [cell setSelected:NO];
//    }
//}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (cell && indexPath.row == 5 && indexPath.section == 0) {
//        
//        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, CGRectGetWidth(self.view.bounds)/2.0);
//    }
//}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    
////    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
//    UIView* footerView = [[UIView alloc] init];
//    footerView.backgroundColor = [UIColor whiteColor];
//    _mTableView.tableFooterView = footerView;
//  
//    return footerView;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
