//
//  AbstractViewController.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 12..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "BaseViewController.h"
#import "SlideNavigationController.h"
#import "UINavigationBar+Addition.h"
//#import "FCFileManager.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "Mask.h"
#import "BandData.h"
//#import "BandHandler.h"
//#import "BinaryHandler.h"
#import <Parse/Parse.h>
//#import "SyncHandler.h"


@interface BaseViewController()<UIGestureRecognizerDelegate>
@property (assign)int continuousCount;

- (void)setNavigationBarCustomize;
- (void)setNavigationBarLineColor;
- (void)setNavigationBarImage;
//- (void)share;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    [self test];
//    NSArray *fontFamilies = [UIFont familyNames];
//    for (int i = 0; i < [fontFamilies count]; i++)
//    {
//        NSString *fontFamily = [fontFamilies objectAtIndex:i];
//        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
//        NSLog (@"%@: %@", fontFamily, fontNames);
//    }
    
    [self setNavigationBarCustomize];
    
    [self setBackButtonWithoutTitle];
    
//    SyncHandler *sync = [SyncHandler sharedInstance];
    
//    // BarButton
//    UIButton *button2  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [button2 setImage:[UIImage imageNamed:@"17_button_right"] forState:UIControlStateNormal];
//    [button2 addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button2];
//    
//    // Setting
//    [SlideNavigationController sharedInstance].rightBarButtonItem = rightBarButtonItem;
    
    // uid 업데이트용 (한번만 부르면 됨)
//    dispatch_async(dispatch_get_main_queue(), ^{
//        BandHandler *band = [BandHandler sharedInstance];
//        [band updateUid];
//    });
    
//    BandHandler *handler = [BandHandler sharedInstance];
//    NSLog(@"Walk_UUID : %@", [handler loadDeviceUUID]);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

#pragma mark - Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)test{
    
    UInt32 result[2]; // result 값을 계속 넘기면서 오리지널 벨류와 mask 한다 (업데이트 개념)
    
    // size는 배열로 미리 값을 설정해놓고 roof 인덱스와 매칭시킨다
    [self shiftBitsIntoInt:result originalValue:40 index:0 size:32];
    
    NSLog(@"test : %u", result[1]);
}

- (void)shiftBitsIntoInt:(UInt32 *)result originalValue:(UInt32)original index:(int)index size:(int)size{
    
    UInt32 mask = 0x00;
    UInt32 temp;
    UInt8 sub_index;
    UInt32 value;
    UInt8 i;

    for(i = 0; i < size; i ++){
        mask |= (1<<i); // size 만큼 루프 돌면서 1로 만든다
    }
    
    // shifting은 31 하는 건 지양
    if(index > 31)
    {
        temp = result[0];
        sub_index = index - 32;
        original &= mask;
        original = original << sub_index;
        temp |= original;
        result[0] = (uint32_t)temp;
    }
    else
    {
        temp=result[1];
        sub_index = index;
        original &= mask;
        original = original << sub_index;
        temp |= original;
        result[1] = (uint32_t)temp;
    }
    
//    NSLog(@"mask : %u", (unsigned int)mask);
//    NSLog(@"resutl0 : %u / 1 : %u", result[0], result[1]);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

//- (void)setSwipeGestureState{
//    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    }
//}

- (void)setNavigationBarCustomize{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    // Navi TextColor
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,
//                                    [UIFont fontWithName:@"Helvetica-Bold" size:24.0], NSFontAttributeName,
                                    nil];

    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)setNavigationBarLineColor{
    
    UINavigationBar *navigationBar = [SlideNavigationController sharedInstance].navigationBar;
    [navigationBar hideBottomHairline];
}

- (void)setNavigationBarImage{
    
}

- (void)setNavigationBarColor:(UIColor *)color{

}

- (void)setBackButtonWithoutTitle{
//    [UINavigationItem setBackButtonWithoutTitle:self.navigationItem];
}

- (void)setStatusBarColorWhite{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return YES;
}


@end
