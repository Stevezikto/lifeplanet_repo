//
//  SignViewController.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 9..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "LoginViewController.h"
#import "UINavigationBar+Addition.h"
//#import "SyncContainerViewController.h"
//#import "UINavigationItem+setBackButtonTitle.h"
#import "UserDataModel.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
//#import "SignHandler.h"
//#import "UserHandler.h"
#import "UserData.h"
#import <Realm/Realm/Realm.h>
#define NoImage @"https://c2.staticflickr.com/6/5261/5897451020_1489822e6b_b.jpg"


@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIView *accountBackView;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSign;
@property (weak, nonatomic) IBOutlet UIButton *btnForgot;
@property (weak, nonatomic) IBOutlet UIButton *btnKeyboard;

- (IBAction)ButtonClick:(UIButton *)sender;


@end

@implementation LoginViewController
- (void)viewDidLoad {

    self.title = @"Sign in";
    
    [self localizeString];
}

- (void)viewWillAppear:(BOOL)animated{
   
    self.navigationController.navigationBarHidden = NO;
   
    [self setNavigationBarImage];
    
    [self setNavigationBarLineColor];
    
//    [UINavigationItem setBackButtonWithoutTitle:self.navigationItem];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private

- (void)localizeString{

    [_btnSign setTitle:NSLocalizedString(@"sign_in", @"Sign in") forState:UIControlStateNormal];
    [_btnForgot setTitle:NSLocalizedString(@"forgot_pass", @"Forgot your password?") forState:UIControlStateNormal];
    _tfEmail.placeholder = NSLocalizedString(@"email", @"Email");
    _tfPassword.placeholder = NSLocalizedString(@"password", @"Password");
}

- (IBAction) exitFromSignViewController:(UIStoryboardSegue *)segue
{
    NSLog(@"");
}

- (IBAction)closeKeyboard:(id)sender {
    
    [_tfEmail resignFirstResponder];
    [_tfPassword resignFirstResponder];
}


- (IBAction)ButtonClick:(UIButton *)sender {
    NSLog(@"");
    
    if([self checkEmptyAccount])
        return;
    
    [self logIn];
}

- (void)setNavigationBarLineColor{
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar hideBottomHairline];
}

- (void)setNavigationBarImage{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
}

- (void)logIn{
    
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
//    [SignHandler logIn:@{@"id": _tfEmail.text, @"password": _tfPassword.text} complete:^(bool success) {
    
//        NSLog(@"login : %d", success);
        
//        if(success){
//            
//            PFUser *parseUser = [PFUser currentUser];
//            NSLog(@"success : %d , parseUser : %@",success, parseUser);
//            
//            // Local DB에 저장 된 사용자 있는지 체크
//            NSPredicate *pred = [NSPredicate predicateWithFormat:@"uuid = %@",parseUser.objectId];
//            RLMResults *result = [UserData objectsWithPredicate:pred];
//         
//            NSLog(@"result : %@ , count:%ld", result , result.count);
//            
//            // 없으면 새로 생성
//            if(!result.count){
//               
//                [UserHandler createNewUser:parseUser complete:^(BOOL complete) {
//                    NSLog(@"complete : %d", complete);
//                }];                
//            }
//                        
//            [self performSegueWithIdentifier:@"LoginToDailyBase" sender:self];
//        }
//        else
//            [CommonErrorType showErrorWithType:ErrorTypeAccountNotMatched];
//        
//        [SVProgressHUD dismiss];
//    }];
    
//    [SignHandler signUpWithEmail:@{@"id": _tfEmail.text, @"password": _tfPassword.text} complete:^(bool success) {
//        [self performSegueWithIdentifier:@"LoginToRegister" sender:self];
//    }];
//    // parse 연동
//    PFUser *user = [PFUser user];
//   
//    // new sign up
//    user.username = _tfEmail.text;
//    user.password = _tfPassword.text;
//    
////    PFACL *defaultACL = [PFACL ACL];
////    [defaultACL setPublicReadAccess:YES];
////    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
//
//    // 당사자만 접근 허용
////    [PFACL setDefaultACL:[PFACL ACL] withAccessForCurrentUser:YES];
//    
//    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        if (!error) {
//            // Hooray! Let them use the app now.
//            NSLog(@"sign succeed");
//        } else {
//            NSString *errorString = [error userInfo][@"error"];
//            // Show the errorString somewhere and let the user try again.
//            NSLog(@"error : %@", errorString);
//        }
//    }];
}

- (void)logOut{
    
    [PFUser logOut];
}

- (void)query{
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:@"tulurira3@naver.com"]; // find all the women
    NSArray *results = [query findObjects];
    
    NSLog(@"results : %@", results);
}

- (BOOL)checkEmptyAccount{
    
    BOOL emptyAccount = NO;
    
//    if([_tfEmail.text isEqual:@""]){
//        [CommonErrorType showErrorWithType:ErrorTypeMissingEmail];
//        emptyAccount = YES;
//    }
//    else if([_tfPassword.text isEqual:@""]){
//        [CommonErrorType showErrorWithType:ErrorTypeMissingPassword];
//        emptyAccount = YES;
//    }
    
    return emptyAccount;
}


#pragma mark - Navi

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
}

#pragma mark - TextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
