//
//  LoginViewController.m
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 9..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "MainViewController.h"
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
#import "SCFacebook.h"
#import "UserDataModel.h"
#import "UINavigationBar+Addition.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <Parse/Parse.h>
#import "UserData.h"
#import "SignHandler.h"
#import "UserHandler.h"
#import "UIImage+ResizeNCrop.h"
#import <Realm/Realm/Realm.h>

typedef NS_ENUM(NSUInteger, LoginType) {
    LoginTypeSign,
    LoginTypeFacebook,
    LoginTypeCreateAccount,
};

@interface MainViewController ()<UIGestureRecognizerDelegate>
@property (assign)BOOL isSigned;
@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;

- (IBAction)ButtonClick:(UIButton *)sender;
@end

@implementation MainViewController
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
//    [self initforRetina];
    
    [self localizeString];
    // 가입 한 유저 구분
//    if([UserDataModel getUserData])
//    [self performSegueWithIdentifier:@"LoginToDailyBase" sender:self];

}

//- (void)viewWillAppear:(BOOL)animated{
//    self.navigationController.navigationBarHidden = YES;
//
////    [self setBackButtonWithoutTitle];
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    self.navigationController.swipeBackEnabled = YES;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    float deviceHeight = [GeneralUtil getCurrentDeviceHeight]; // 6: 647 , 6+ :  716.000000
    
    if(deviceHeight < iPhone5){
       
        [_landingView.image resizeImageAtSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}


#pragma mark - Private

- (void)localizeString{

    [_btnCreate setTitle:NSLocalizedString(@"create_account", @"Create your account") forState:UIControlStateNormal];
    [_btnEmail setTitle:NSLocalizedString(@"sign_email", @"Sign in with Email") forState:UIControlStateNormal];
    [_btnFacebook setTitle:NSLocalizedString(@"sign_facebook", @"   Sign in with facebook") forState:UIControlStateNormal];
}

- (void)share{

}

- (void)initforRetina{
 
//    UIImage *image = [UIImage imageNamed:@"app_landing"];
    float deviceHeight = [GeneralUtil getCurrentDeviceHeight]; // 6: 647 , 6+ :  716.000000
    
    if(deviceHeight < iPhone5){
//        _landingView.image = [self scaleImage:_landingView.image toSize:CGSizeMake(self.view.frame.size.width, iPhone5)];
        
        [_landingView.image resizeImageAtSize:CGSizeMake(self.view.frame.size.width, iPhone5)];
    }

//    _landingView.center = CGPointMake(self.view.center.x, self.view.center.y);
}


- (void)setNavigationBarLineColor{
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar hideBottomHairline];
}

- (IBAction)ButtonClick:(UIButton *)sender {
    
    switch (sender.tag) {
        case LoginTypeSign:
            break;
        case LoginTypeFacebook:
            [self facebookLogin];
            break;
        case LoginTypeCreateAccount:
            break;
    }
}

- (void)facebookLogin
{
    [SignHandler signUpWithFacebook:^(int successType) {
        if(successType == FaceBookLoginTypeSignUp){
            [self performSegueWithIdentifier:@"MainToTerm" sender:self];
        }
        else if(successType == FaceBookLoginTypeSignIn){
          
            NSLog(@"페이스북 로긴");
            
            PFUser *pfUser = [PFUser currentUser];
            NSLog(@"pfuser : %@", pfUser);
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"uuid = %@", pfUser.objectId];
            RLMResults *result = [UserData objectsWithPredicate:pred];
            
            NSLog(@"result : %ld",result.count);
            
            if(result.count > 0){
                [UserHandler updateUser:pfUser];
            }
            else
                [UserHandler createNewUser:pfUser complete:^(BOOL complete) {
                    if(complete)
                        NSLog(@"신규 유저 생성 success");
                }];
            
            [self performSegueWithIdentifier:@"LoginToBodyBase" sender:self];
        }
        [SVProgressHUD dismiss];
    }];
}

- (void)linkFacebookUserToParse:(PFUser *)user complete:(void (^)(bool complete))complete{
    
    if (![PFFacebookUtils isLinkedWithUser:user]) {
        [PFFacebookUtils linkUser:user permissions:nil block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Woohoo, user logged in with Facebook!");
            }
            else{
                NSLog(@"failed linking");
            }
        }];
    }
    else{
        NSLog(@"already linked user");
    }
    complete(YES);
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"base"]){
        
    }

}

#pragma mark - SlideNavigationController Methods -

@end
