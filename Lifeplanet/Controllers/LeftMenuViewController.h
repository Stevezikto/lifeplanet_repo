//
//  LeftMenuViewController.h
//  Arki
//
//  Created by tulurira on 2015. 1. 2..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
