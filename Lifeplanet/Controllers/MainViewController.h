//
//  LoginViewController.h
//  Arki
//
//  Created by Eizer-D93 on 2015. 1. 9..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "BaseViewController.h"

@interface MainViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *landingView;

@end
