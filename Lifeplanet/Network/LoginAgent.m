//
//  LoginAgent.m
//  Arki
//
//  Created by ken on 2015. 3. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import "LoginAgent.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPSessionManager.h"

@implementation LoginAgent

+ (void)sendAnsynchronousRequestForMethodPost:(NSString *)urlString
                                   parameters:(id) parameters
                                      success:(void (^)(id responseObject))success
                                      failure:(void (^) (NSError *error) )failure{
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
////    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
////    manager.responseSerializer = [AFXMLParserResponseSerializer new];
////    // Or data
////    manager.responseSerializer = [AFHTTPResponseSerializer new];


    
//    [manager POST:urlString parameters:@ {@"user" :@"ken", @"pwd" :@"" } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                NSLog(@"responseObject: %@", responseObject);
//        success(responseObject);
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"Error: %@", error);
//        failure(error);
//    }];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //application/octet-stream / text/html
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/octet-stream"];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:urlString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"responseObject: %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
}

+ (void)sendAnsynchronousRequestForMethodGet:(NSString *)urlString
                                   parameters:(id) parameters
                                      success:(void (^)(id responseObject))success
                                      failure:(void (^) (NSError *error) )failure{
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//
//    [manager GET:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSLog(@"responseObject: %@", responseObject);
//        success(responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        NSLog(@"Error: %@", error);
//        failure(error);
//    }];
    
//    NSURL *baseURL = [NSURL URLWithString:HOST];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/octet-stream"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSLog(@"url : %@", urlString);
    
    [manager GET:urlString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        success(responseObject);

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error: %@", error.description);
        failure(error);
    }];
}


@end
