//
//  LoginAgent.h
//  Arki
//
//  Created by ken on 2015. 3. 27..
//  Copyright (c) 2015년 Zikto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginAgent : NSObject

//+ (LoginAgent *)sharedInstance;

+ (void)sendAnsynchronousRequestForMethodPost:(NSString *)urlString
                                   parameters:(id) parameters
                                      success:(void (^)(id responseObject))success
                                      failure:(void (^) (NSError *error) )failure;

//+ (void)sendAnsynchronousRequestForMethodMultiPartPost:(NSString *)urlString
//                                            parameters:(id) parameters
//                                                 image:(NSData *)imageData
//                                               success:(void (^)(id responseObject))success
//                                               failure:(void (^) (NSError *error) )failure;


+ (void)sendAnsynchronousRequestForMethodGet:(NSString *)urlString
                                  parameters:(id) parameters
                                     success:(void (^)(id responseObject))success
                                     failure:(void (^) (NSError *error) )failure;

@end
